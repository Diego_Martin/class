function calcula(texto) { /* Aqui le llega un String, que puede tener 0's a la izquierda */
	var int = parseInt(texto) /* Aqui los paso a entero, por lo que se le quitan los 0's */
	document.getElementById('res').innerHTML = "Dígitos: " + int.toString().length /* Aqui lo convierto a String y le calculo el número de digitos */
}