function calcula () {
	/* Cojo datos */
	var dia = parseInt(document.getElementById('dia').value)
	var mes = parseInt(document.getElementById('mes').value)
	var año = parseInt(document.getElementById('año').value)
	/* Sumo 1 al día para calcular el siguiente*/
	dia++

	/* Si el día o el mes es negativo le hecho del programa */
	if (dia < 1 || mes < 1) {
		alert("Los dias y meses son positivos")
		return 0
	}
	/* Si el mes es Enero, Marzo, Mayo, Julio, Agosto, Octubre o Diciembre, el mes tiene 31 días*/
	if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
		if (dia > 31) {
			/* Para saber cuántos me paso, le calculo el resto */
			dia = dia % 31
			/* Paso al siguiente mes */
			mes++
			alert ("Te has pasado, te lo sumo en el siguiente mes")
		}
	}
	/* Si el mes es Abril, Junio, Septiembre o Noviembre, el mes tiene 30 días*/
	if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
		if (dia > 30) {
			/* Para saber cuántos me paso, le calculo el resto */
			dia = dia % 30
			/* Paso al siguiente mes */
			mes++
			alert ("Te has pasado, te lo sumo en el siguiente mes")
		}
	}
	/* Si el mes es Febrero: */
	if (mes == 2) {
		/* Si el año es divisible entre 4 y no es múltiplo de 400 (según el calendario gregoriano) */
		if (año % 4 == 0 && año % 400 != 0) {
			/* Entonces Febrero tendria 29 días */
			if (dia > 29) {
				/* Para saber cuántos me paso, le calculo el resto */
				dia = dia % 29
				/* Paso al siguiente mes */
				mes++
				alert ("Te has pasado, te lo sumo en el siguiente mes")
			}
		/* Si no es especial */
		} else {
			/* Entonces Febrero tendria 28 días */
			if (dia > 28) {
				/* Para saber cuántos me paso, le calculo el resto */
				dia = dia % 28
				/* Paso al siguiente mes */
				mes++
				alert ("Te has pasado, te lo sumo en el siguiente mes")
			}
		}
	}
	/* Si pone más de 12 meses */
	if (mes > 12) {
		/* Le sumo al año (mes / 12) y lo redondeo */
		año = año + Math.floor(mes / 12)
		/* Para saber cuántos me paso, le calculo el resto */
		mes = mes % 12
	}
	/* Imprimo el resultado */
	document.getElementById('res').innerHTML = dia + "-" + mes + "-" + año
}