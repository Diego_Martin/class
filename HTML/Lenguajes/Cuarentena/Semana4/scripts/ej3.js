function calcularFecha () {
	var dia1 = parseInt (document.getElementById('dia1').value)
	var dia2 = parseInt (document.getElementById('dia2').value)
	var mes1 = parseInt (document.getElementById('mes1').value)
	var mes2 = parseInt (document.getElementById('mes2').value)
	var año1 = parseInt (document.getElementById('año1').value)
	var año2 = parseInt (document.getElementById('año2').value)
	var parrafo = document.getElementById("resfecha")


	// ---- Compruebo que es un afecha válida-------------
	// Fecha 1
	if (dia1 < 1 || mes1 < 1) {
		alert("Los dias y meses son positivos")
		return 0
	}
	if (mes1 == 1 || mes1 == 3 || mes1 == 5 || mes1 == 7 || mes1 == 8 || mes1 == 10 || mes1 == 12) {
		if (dia1 > 31) {
			dia1 = dia1 % 31
			mes1++
			alert ("Te has pasado, te lo sumo en el siguiente mes")
		}
	}
	if (mes1 == 4 || mes1 == 6 || mes1 == 9 || mes1 == 11) {
		if (dia1 > 30) {
			dia1 = dia1 % 30
			mes1++
			alert ("Te has pasado, te lo sumo en el siguiente mes")
		}
	}
	if (mes1 == 2) {
		if (año1 % 4 == 0 || año1 % 400 != 0) {
			if (dia1 > 28) {
				dia1 = dia1 % 28
				mes1++
				alert ("Te has pasado, te lo sumo en el siguiente mes")
			}
		} else {
			if (dia1 > 29) {
				dia1 = dia1 % 29
				mes1++
				alert ("Te has pasado, te lo sumo en el siguiente mes")
			}
		}
	}
	if (mes1 > 12) {
		mes1 = mes1 % 12
		año1++
	}
	/// Fecha 2
	if (dia2 < 1 || mes2 < 1) {
		alert("Los dias y meses son positivos")
		return 0
	}
	if (mes2 == 1 || mes2 == 3 || mes2 == 5 || mes2 == 7 || mes2 == 8 || mes2 == 10 || mes2 == 12) {
		if (dia2 > 31) {
			dia2 = dia2 % 31
			mes2++
			alert ("Te has pasado, te lo sumo en el siguiente mes")
		}
	}
	if (mes2 == 4 || mes2 == 6 || mes2 == 9 || mes2 == 11) {
		if (dia2 > 30) {
			dia2 = dia2 % 30
			mes2++
			alert ("Te has pasado, te lo sumo en el siguiente mes")
		}
	}
	if (mes2 == 2) {
		if (año2 % 4 == 0 || año2 % 400 != 0) {
			if (dia2 > 28) {
				dia2 = dia2 % 28
				mes2++
				alert ("Te has pasado, te lo sumo en el siguiente mes")
			}
		} else {
			if (dia2 > 29) {
				dia2 = dia2 % 29
				mes2++
				alert ("Te has pasado, te lo sumo en el siguiente mes")
			}
		}
	}
	if (mes2 > 12) {
		mes2 = mes2 % 12
		año2++
	}
	// ----------- Termino la comproación-----------


	// Comparo
	if (año1 == año2) {
		if (mes1 == mes2) {
			if (dia1 == dia2) {
				parrafo.innerHTML = "Las fechas son iguales"
			} else if (dia1 > dia2) {
				parrafo.innerHTML = "La fecha 1 es mayor: " + dia1 + "-" + mes1 + "-" + año1
			} else {
				parrafo.innerHTML = "La fecha 2 es mayor: " + dia2 + "-" + mes2 + "-" + año2
			}
		} else if (mes1 > mes2) {
			parrafo.innerHTML = "La fecha 1 es mayor: " + dia1 + "-" + mes1 + "-" + año1
		} else {
			parrafo.innerHTML = "La fecha 2 es mayor: " + dia2 + "-" + mes2 + "-" + año2
		}
	} else if (año1 > año2) {
		parrafo.innerHTML = "La fecha 1 es mayor: " + dia1 + "-" + mes1 + "-" + año1
	} else {
		parrafo.innerHTML = "La fecha 2 es mayor: " + dia2 + "-" + mes2 + "-" + año2
	}
}