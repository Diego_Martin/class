function calcularFecha () {
	var diaA = parseInt(document.getElementById("diaa").value)
	var mesA = parseInt(document.getElementById("mesa").value)
	var añoA = parseInt(document.getElementById("añoa").value)
	var diaC = parseInt(document.getElementById("diac").value)
	var mesC = parseInt(document.getElementById("mesc").value)
	var añoC = parseInt(document.getElementById("añoc").value)
	// A: Actual
	// C: Cumpleaños
	var parrafo = document.getElementById("resFecha")

	// ---- Compruebo que es un afecha válida-------------
	// Fecha 1
	if (diaA < 1 || mesA < 1) {
		alert("Los dias y meses son positivos")
		return 0
	}
	if (mesA == 1 || mesA == 3 || mesA == 5 || mesA == 7 || mesA == 8 || mesA == 10 || mesA == 12) {
		if (diaA > 31) {
			diaA = diaA % 31
			mesA++
			alert ("Te has pasado, te lo sumo en el siguiente mes")
		}
	}
	if (mesA == 4 || mesA == 6 || mesA == 9 || mesA == 11) {
		if (diaA > 30) {
			diaA = diaA % 30
			mesA++
			alert ("Te has pasado, te lo sumo en el siguiente mes")
		}
	}
	if (mesA == 2) {
		if (añoA % 4 == 0 || añoA % 400 != 0) {
			if (diaA > 28) {
				diaA = diaA % 28
				mesA++
				alert ("Te has pasado, te lo sumo en el siguiente mes")
			}
		} else {
			if (diaA > 29) {
				diaA = diaA % 29
				mesA++
				alert ("Te has pasado, te lo sumo en el siguiente mes")
			}
		}
	}
	if (mesA > 12) {
		mesA = mesA % 12
		añoA++
	}
	/// Fecha 2
	if (diaC < 1 || mesC < 1) {
		alert("Los dias y meses son positivos")
		return 0
	}
	if (mesC == 1 || mesC == 3 || mesC == 5 || mesC == 7 || mesC == 8 || mesC == 10 || mesC == 12) {
		if (diaC > 31) {
			diaC = diaC % 31
			mesC++
			alert ("Te has pasado, te lo sumo en el siguiente mes")
		}
	}
	if (mesC == 4 || mesC == 6 || mesC == 9 || mesC == 11) {
		if (diaC > 30) {
			diaC = diaC % 30
			mesC++
			alert ("Te has pasado, te lo sumo en el siguiente mes")
		}
	}
	if (mesC == 2) {
		if (añoC % 4 == 0 || añoC % 400 != 0) {
			if (diaC > 28) {
				diaC = diaC % 28
				mesC++
				alert ("Te has pasado, te lo sumo en el siguiente mes")
			}
		} else {
			if (diaC > 29) {
				diaC = diaC % 29
				mesC++
				alert ("Te has pasado, te lo sumo en el siguiente mes")
			}
		}
	}
	if (mesC > 12) {
		mesC = mesC % 12
		añoC++
	}
	// ----------- Termino la comproación-----------


	// Te digo tu edad exacta
	if (añoA < añoC) {
		parrafo.innerHTML = "No puedes calcular tu cumpleaños sin haber nacido1"
		return 0
	}
	if (añoA == añoC) {
		if (mesA < mesC) {
			parrafo.innerHTML = "No puedes calcular tu cumpleaños sin haber nacido2"
			return 0
		} else if (mesA > mesC) {
			parrafo.innerHTML = "Tienes " + (mesA - mesC) + ((mesA - mesC) > 1 ? " meses" : " mes")
			if (diaA > diaC) {
				parrafo.innerHTML += " y " + (diaA - diaC) + ((diaA - diaC) > 1 ? " dias" : " dia")
			} else {
				parrafo.innerHTML += " menos " + Math.abs((diaC - diaA)) + ((diaA - diaC) > 1 ? " dias" : " dia")
			}
		} else {
			parrafo.innerHTML = "Tienes " + (diaA - diaC) + ((diaA - diaC) > 1 ? " dias" : " dia")
		}
	} else {
		parrafo.innerHTML = "Tienes " + (añoA - añoC) + ((añoA - añoC) > 1 ? " años" : " año")
		if (mesA < mesC) {
			parrafo.innerHTML += " menos " + Math.abs((mesA - mesC)) + ((mesA - mesC) > 1 ? " meses" : " mes")
			if (diaA > diaC) {
				parrafo.innerHTML += " y " + (diaA - diaC) + ((diaA - diaC) > 1 ? " dias" : " dia")
			} else {
				parrafo.innerHTML += " menos " + Math.abs((diaC - diaA)) + ((diaA - diaC) > 1 ? " dias" : " dia")
			}
		} else if (mesA > mesC) {
			parrafo.innerHTML += ", " + (mesA - mesC) + ((mesA - mesC) > 1 ? " meses" : " mes")
			if (diaA > diaC) {
				parrafo.innerHTML += " y " + (diaA - diaC) + ((diaA - diaC) > 1 ? " dias" : " dia")
			} else {
				parrafo.innerHTML += " menos " + Math.abs((diaC - diaA)) + ((diaA - diaC) > 1 ? " dias" : " dia")
			}
		} else {
			parrafo.innerHTML += " y " + (diaA - diaC) + ((diaA - diaC) > 1 ? " dias" : " dia")
		}
	}
	parrafo.innerHTML += "."
}