// Variables globales
// Variable que contiene la puntuacion total
var puntuacionTotal = 0
// Variable que contiene la puntuacion actual
var puntuacionActual = 0
// Contiene las id's de los botones para desactivarlos
var botones = ["btn1", "btn2", "btn3"]
// Contiene las rutas de las imágenes, si añades más imagenes, añade las rutas aqui
var imgs = ["./img/fresa.png", "./img/limon.png", "./img/sandia.png", "./img/manzana.png", "./img/naranja.png", "img/pera.png"]

// Muestra las puntuaciones en los marcadores
function actualizarMarcadores (total, actual) {
	document.getElementById('puntTotal').innerHTML = total
	document.getElementById('puntActual').innerHTML = actual
}

// Cambia el estilo de las cajas 'inicio', 'juego', 'info' para que se oculten
function cerrar() {
	var divs = ['inicio', 'juego', 'info']
	for (var i = 0; i < divs.length; i++)
		document.getElementById(divs[i]).style.display = 'none' // Cambiando la propiedad 'display'

}

// Muestra una de las 3 cajas principales
function show(div) {
	cerrar ()                                            // Primero cierra todos
	document.getElementById(div).style.display = 'block' // Y muestra el div que se le pasa por parámetro
}


function comprobarPuntuacion() {
	// Meto la ruta de las imagenes en variables
	var valor1 = document.getElementById("slot1").src
	var valor2 = document.getElementById("slot2").src
	var valor3 = document.getElementById("slot3").src
	// Si ninguna es './img/nada.png'
	if (valor1 !== './img/nada.png' && valor2 !== './img/nada.png' && valor3 !== './img/nada.png') {
		// Si los 3 son iguales
		if (valor1 == valor2 && valor2 == valor3)
			puntuacionActual = 1000
		// Si hay 2 iguales
		else if (valor1 == valor2 || valor1 == valor3 || valor2 == valor3)
			puntuacionActual = 500
		// Si ninguno es igual
		else
			puntuacionActual = 0
		// Actualiza los maicadores
		actualizarMarcadores (puntuacionTotal, puntuacionActual)
	} else {
		actualizarMarcadores (puntuacionTotal, 0)
	}
}


function reiniciar() {
	// Muestra la imagen 'nada'
	document.getElementById("slot1").src = "./img/nada.png"
	document.getElementById("slot2").src = "./img/nada.png"
	document.getElementById("slot3").src = "./img/nada.png"
	// Resetea la puntuación
	puntuacionActual = 0
	puntuacionTotal = 0
	// Desactiva lo botones "btn1", "btn2", "btn3" para que no se pulsen
	for (var i = 0; i < botones.length; i++)
		document.getElementById(botones[i]).disabled = true
	// Actualiza los marcadores
	actualizarMarcadores (puntuacionTotal, puntuacionActual)
}

// Guarda la puntuación
function guardar() {
	// Suma la puntuación actual y la total y la mete en la total
	puntuacionTotal += puntuacionActual
	// Resetea la puntuación actual
	puntuacionActual = 0
	// Actualiza los marcadores
	actualizarMarcadores (puntuacionTotal, puntuacionActual)
	// Juega con todos
	jugarTodos ()
	// Desactiva los botones de abajo (Jugar 1, 2 y 3)
	for (var i = 0; i < botones.length; i++)
		document.getElementById(botones[i]).disabled = true
	// Desactiva el boton de guardar
	document.getElementById("btnguardar").disabled = true
}


function jugarTodos() {
	// Activa el boton de guardar
	document.getElementById("btnguardar").disabled = false
	// Activa los botones de jugar 1, 2 y 3
	for (var i = 0; i < botones.length; i++)
		document.getElementById(botones[i]).disabled = false
	// Juega los 3 slots
	for (var i = 1; i <= 3; i++)
		jugar (i)
}

// Juega un solo slot
function jugar (slot) {
	// Consigue un número entre el 0 y (el número de imagenes - 1)
	var num = Math.floor (Math.random () * imgs.length)
	// Muestra la imagen con el recurso en la posición 'num' atendiendo a la aleatoriedad de la linea anterior
	document.getElementById("slot" + slot).src = imgs[num]
	// Comprueba la puntuación
	comprobarPuntuacion ()
}

// Al cargarse la página
window.onload = function () {
	// Muestra la pantalla de inicio
	show ('inicio')
	// Desactiva los botones
	for (var i = 0; i < botones.length; i++)
		document.getElementById(botones[i]).disabled = true
	// Actualiza los marcadores
	actualizarMarcadores (puntuacionTotal, puntuacionActual)
	// Instancio un objeto Date
	var fecha = new Date()
	// Muestra la fecha en el span con la id fecha
	document.getElementById('fecha').innerHTML = fecha.getDate() + " / " + (fecha.getMonth() + 1) + " / " + fecha.getFullYear()
}