DROP USER 'admin'@'localhost';
DROP USER 'cliente'@'localhost';
DROP USER 'contable'@'localhost';

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'contra';
CREATE USER 'cliente'@'localhost' IDENTIFIED BY 'contra';
CREATE USER 'contable'@'localhost' IDENTIFIED BY 'contra';

GRANT ALL PRIVILEGES ON Jardineria.clientes TO 'admin'@'localhost';
GRANT SELECT, INSERT PRIVILEGES ON Jardineria.pedidos TO 'cliente'@'localhost';
-- GRANT SELECT, UPDATE, INSERT, DELETE PRIVILEGES ON Jardineria.clientes TO 'contable'@'localhost'; -- A detallepedidos tambien