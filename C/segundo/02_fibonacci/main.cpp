#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
	int num1, num2, limite, temp;
	char *miargv[3];
	if (argc < 2) {
		fprintf (stderr, "\033[91;40mEl programa debe tener un parámtero que sea el final de la serie de fibonacci.\033[97;40m\n\n");
		exit (EXIT_FAILURE);
	} else if (argc == 2) {
		num1 = 1;
		num2 = 1;
		limite = atoi (argv [1]);
		miargv[0] = (char *) malloc (sizeof (char) * strlen (argv [0]));
		miargv[1] = (char *) malloc (sizeof (char) * 2);
		miargv[2] = (char *) malloc (sizeof (char) * 2);
		strcpy (miargv[0], argv[1]);
		strcpy (miargv[1], "1");
		strcpy (miargv[2], "1");
	} else if (argc == 4) {
		miargv[0] = (char *) malloc (sizeof (char) * strlen (argv [0]));
		miargv[1] = (char *) malloc (sizeof (char) * strlen (argv [1]));
		miargv[2] = (char *) malloc (sizeof (char) * strlen (argv [2]));
		strcpy (miargv[0], argv[1]);
		strcpy (miargv[1], argv [2]);
		strcpy (miargv[2], argv [3]);
		limite = atoi (argv [1]);
		num1 = atoi (argv [2]);
		num2 = atoi (argv [3]);
	} else {
		fprintf (stderr, "\033[91;40mEl programa solo debe tener un parámtero.\033[97;40m\n\n");
		exit (EXIT_FAILURE);
	}

	// printf ("Numero: %d", num2);
	temp = num1 + num2;
	num1 = num2;
	num2 = temp;

	printf ("num1: %d\n", num1);
	printf ("num2: %d\n", num2);
	printf ("limite: %d\n", limite);
	int longlista = sizeof (miargv) / sizeof (char *);
	printf ("[");
	for (int i = 0; i < longlista; i++)
		if (i < longlista - 1)
			printf ("%s, ", miargv [i]);
		else
			printf ("%s", miargv [i]);

	printf ("]\n\n");
	execvp (argv[0], miargv);

	return EXIT_SUCCESS;
}