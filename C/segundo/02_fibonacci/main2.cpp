#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
	int num1 = 0, num2 = 1, temp, max = 5;

	if (num2 <= max) {
		fork ();
		temp = num1 + num2;
		num1 = num2;
		num2 = temp;
		printf ("%d - %d\n", num1, num2);
	}
	
	// execvp (argv[0], miargv);

	return EXIT_SUCCESS;
}