#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void sigint_handler (int sig) {
	printf("No se puede usar (Ctrl + C).\n");
}

void sigusr1_handler (int sig) {
	printf("Se ha mandado una señal SIGUSR1.\n");
}

int main(int argc, char const *argv[])
{
	struct sigaction sa;
	struct sigaction sa2;

	sa.sa_handler = &sigint_handler;
	sa2.sa_handler = &sigusr1_handler;

	sigaction (SIGINT, &sa, NULL);
	sigaction (SIGUSR1, &sa2, NULL);

	for (int i = 0; i < 100; ++i) {
		printf ("Mensaje.\n\n");
		sleep (2);
	}
	return 0;
}