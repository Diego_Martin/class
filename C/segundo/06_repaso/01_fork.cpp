#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
	pid_t child_pid;

	child_pid = fork ();

	if (child_pid == 0) {
		// Aqui el hijo
		printf("[PID: %i]: Hijo.\n", getpid ());
	} else {
		// Aqui el padre
		printf("[PID: %i]: Padre.\n", getpid ());
	}
	return 0;
}