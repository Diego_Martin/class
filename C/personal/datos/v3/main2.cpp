#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class Familia
{
public:
	// Familia();
	// ~Familia();
	virtual void hablar () = 0;
};

class Madre : public Familia
{
public:
	// Madre();
	// ~Madre();
	void hablar () {
		printf("Soy la Madre\n");
	}
};

class Hija : public Familia
{
public:
	// Hija();
	// ~Hija();
	void hablar () {
		printf("Soy la Hija\n");
	}
};

class Hijo : public Familia
{
public:
	// Hijo();
	// ~Hijo();
	void hablar () {
		printf("Soy el Hijo\n");
	}
};


int main(int argc, char const *argv[])
{
	Hija hija;
	Hijo hijo;
	Madre madre;
	Familia *lista[3] = {&madre, &hijo, &hija};
	for (int i = 0; i < 3; ++i)
		lista[i]->hablar();

	Familia *Diego = new Hijo ();
	Diego->hablar ();
	return 0;
}