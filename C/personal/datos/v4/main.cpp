/*
=========================================
==           Datos version 4           ==
=========================================
== Se trata de hacer un programa que   ==
== rechace los tipos de datos, creando ==
== una estructura de datos de manera   ==
== que exista uno padre, una interfaz, ==
== no instanciable, del que cuelgan    ==
== todos los demas.                    ==
==                                     ==
== Aun no me libro de los punteros, he ==
== pensado que si quisiera hacerlo,    ==
== deberia hacer un intérprete, pero   ==
== eso significaría la creación de un  ==
== lenguaje de programación, y me da   ==
== pereza.                             ==
=========================================
*/
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <typeinfo>

/*
=========================================
==            Clase Object             ==
=========================================
== Es la clase padre de todos los ti-  ==
== pos de datos que voy a crear.       ==
==                                     ==
== Realmente se podría decir que no es ==
== una clase, sino una interfaz (clase ==
== 100% abstracta)                     ==
==                                     ==
== Todos los metodos de esta clase     ==
== deben ser sobreescritos pero es ne- ==
== cesaria.                            ==
=========================================
*/
class Object
{
	public:
		// Object();
		// ~Object();
		virtual const char *toString () = 0;
		virtual const char *type () = 0;
		virtual void setValor (const char *frase) = 0;
};
/*
=========================================
==            Clase String             ==
=========================================
== Representa el tipo de dato 'String' ==
== adaptado a mi propio programa.      ==
==                                     ==
== Es una clase hija (derivada) de la  ==
== clase 'Object'                      ==
=========================================
*/
class String : public Object
{
	public:
		String () {}
		String (const char *texto) {
			this->setValor (texto);
		}
		String (char *texto) {
			this->setValor ((const char *) texto);
		}
		String (char texto) {
			char *res = (char *) malloc (sizeof (char) * 2);
			sprintf (res, "%c", texto);
			this->setValor ((const char *) res);
		}
		/*
		=========================================
		==           Metodo toString           ==
		=========================================
		== Se utiliza en casi todos lados      ==
		=========================================
		*/
		const char *toString () {
			return (const char *) this->valor;
		}
		/*
		=========================================
		==             Metodo type             ==
		=========================================
		== Lo creé para poder comparar tipos   ==
		== independientemente de la función    ==
		== type ()                             ==
		=========================================
		*/
		const char *type () {
			return "String";
		}

		/*
		=========================================
		==           Sobrecarga de []          ==
		=========================================
		== Se usa para acceder a un caracter   ==
		== en concreto del String.             ==
		==                                     ==
		== Admite valores negativos (Empezando ==
		== a contar desde el final) pero no    ==
		== indices fuera de rango.             ==
		=========================================
		*/
		char operator [] (int index) {
			if (index > -1) {
				if (index < this->length()) {
					char res = *(this->valor + index);
					return res;
				} else {
					printf("\033[31;40mEl indice debe ser menor que la longitud de la palabra.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			} else {
				if (index > (-1 * this->length())) {
					char res = *(this->valor + this->length() + index);
					return res;
				} else {
					printf("\033[31;40mEl indice debe ser mayor que la longitud de la palabra en negativo.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			}
		}
		/*
		=========================================
		==         Sobreescritura de []        ==
		=========================================
		==           Sobrecarga de []          ==
		=========================================
		== Se usa para acceder a un caracter   ==
		== en concreto del String.             ==
		==                                     ==
		== Admite valores negativos (Empezando ==
		== a contar desde el final) pero no    ==
		== indices fuera de rango.             ==
		=========================================
		== No funciona porque en este punto    ==
		== del programa 'Integer' no esta      ==
		== creado, cuando se divida el         ==
		== programa en varios ficheros, ésta   ==
		== funcion se habilitará.              ==
		=========================================
		*/
		// char operator [] (Integer index) {
		// 	if (index.getValor() > -1) {
		// 		if (index.getValor() < this->length()) {
		// 			char res = *(this->valor + index.getValor());
		// 			return res;
		// 		} else {
		// 			printf("\033[31;40mEl indice debe ser menor que la longitud de la palabra.\n\033[97;40m");
		// 			exit (EXIT_FAILURE);
		// 		}
		// 	} else {
		// 		if (index.getValor() > (-1 * this->length())) {
		// 			char res = *(this->valor + this->length() + index.getValor());
		// 			return res;
		// 		} else {
		// 			printf("\033[31;40mEl indice debe ser mayor que la longitud de la palabra en negativo.\n\033[97;40m");
		// 			exit (EXIT_FAILURE);
		// 		}
		// 	}
		// }
		/*
		=========================================
		==           Sobrecarga de =           ==
		=========================================
		== Fija el valor de la variable al     ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (const char *)            ==
		=========================================
		*/
		void operator = (const char *frase) {
			this->setValor (frase);
		}
		/*
		=========================================
		==         Sobreescritura de =         ==
		=========================================
		==           Sobrecarga de =           ==
		=========================================
		== Fija el valor de la variable al     ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite otro (String)                ==
		=========================================
		*/
		void operator = (String frase) {
			this->setValor (frase.toString());
		}
		/*
		=========================================
		==            Metodo length            ==
		=========================================
		== Retorna la longitud del String      ==
		=========================================
		*/
		int length () {
			return strlen (this->valor);
		}
		/*
		=========================================
		==           Metodo setValor           ==
		=========================================
		== Fija el valor de la variable al     ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (const char *)            ==
		=========================================
		*/
		void setValor (const char *frase) {
			this->valor = (char *) malloc (sizeof (char) * strlen (frase));
			strcpy (this->valor, frase);
			// free (this->valor);
		}
		/*
		=========================================
		==           Sobrecarga de +           ==
		=========================================
		== Concatena Strings                   ==
		==                                     ==
		== Admite un (const char *)            ==
		=========================================
		*/
		String operator + (const char *frase) {
			String res;
			char *resultado = (char *) malloc (sizeof (char) * (strlen (this->valor) + strlen (frase)));
			strcpy (resultado, this->valor);
			strcat (resultado, frase);
			res = (const char *) resultado;
			return res;
		}
		/*
		=========================================
		==         Sobreescitura de +          ==
		=========================================
		==           Sobrecarga de +           ==
		=========================================
		== Concatena Strings                   ==
		==                                     ==
		== Admite un (String)                  ==
		=========================================
		*/
		String operator + (String frase) {
			String res;
			char *resultado = (char *) malloc (sizeof (char) * (strlen (this->valor) + strlen (frase.toString ())));
			strcpy (resultado, this->valor);
			strcat (resultado, frase.toString ());
			res = (const char *) resultado;
			return res;
		}
		/*
		=========================================
		==           Sobrecarga de ==          ==
		=========================================
		== Compara Strings                     ==
		==                                     ==
		== Admite un (const char *)            ==
		=========================================
		*/
		bool operator == (const char *frase) {
			return !((bool) strcmp ((const char *) this->valor, frase));
		}
		/*
		=========================================
		==         Sobreescitura de ==         ==
		=========================================
		==           Sobrecarga de ==          ==
		=========================================
		== Compara Strings                     ==
		==                                     ==
		== Admite un (String)                  ==
		=========================================
		*/
		bool operator == (String frase) {
			return !((bool) strcmp ((const char *) this->valor, frase.toString ()));
		}

		~String () {
			delete this->valor;
		}
	private:
		char *valor;
};

class Boolean : public Object
{
	public:
		Boolean () {}
		Boolean (bool valor) {
			this->valor = valor;
		}
		Boolean (int valor) {
			this->valor = (bool) valor;
		}
		/*
		=========================================
		==           Metodo toString           ==
		=========================================
		== Se utiliza en casi todos lados      ==
		=========================================
		*/
		const char *toString () {
			return ((this->valor) ? "True" : "False");
		}
		/*
		=========================================
		==             Metodo type             ==
		=========================================
		== Lo creé para poder comparar tipos   ==
		== independientemente de la función    ==
		== type ()                             ==
		=========================================
		*/
		const char *type () {
			return "Boolean";
		}
		void setValor (const char *frase) {return;}
		/**/
		bool getValor () {
			return this->valor;
		}
		/**/
		void operator = (bool valor) {
			this->valor = valor;
		}
		/**/
		void operator = (Boolean valor) {
			this->valor = valor.getValor ();
		}
		/**/
		void operator = (String valor) {
			if (valor == "")
				this->valor = false;
			else
				this->valor = true;
		}
		/**/
		void operator = (int valor) {
			if (valor == 0)
				this->valor = false;
			else
				this->valor = true;
		}
		/**/
		bool operator ! () {
			return !this->valor;
		}
	private:
		bool valor;
};

class Integer : public Object
{
	public:
		Integer () {}
		Integer (int num) {
			this->valor = num;
		}
		Integer (float num) {
			this->valor = (int) num;
		}
		Integer (double num) {
			this->valor = (int) num;
		}
		/*
		=========================================
		==           Metodo toString           ==
		=========================================
		== Se utiliza en casi todos lados      ==
		=========================================
		*/
		const char *toString () {
			int contador = 1;
			long long int aux = this->valor;
			
			if (aux >= 0) {
				while(aux / 10 > 0) {
					aux /= 10;
					contador++;
				}
				this->res = (char *) malloc (sizeof (char) * contador);
			} else {
				while(aux * 10 > 0) {
					aux *= 10;
					contador++;
				}
				this->res = (char *) malloc (sizeof (char) * (contador + 1));
			}
			sprintf (this->res, "%lli", this->valor);
			// free (res); // Da error
			return (const char *) res;
		}
		/*
		=========================================
		==             Metodo type             ==
		=========================================
		== Lo creé para poder comparar tipos   ==
		== independientemente de la función    ==
		== type ()                             ==
		=========================================
		*/
		const char *type () {
			return "Integer";
		}
		void setValor (const char *frase) {return;}
		/*
		=========================================
		==           Sobrecarga de =           ==
		=========================================
		== Fija el valor de la variable al     ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		void operator = (int valor) {
			this->valor = valor;
		}
		/*
		=========================================
		==         Sobreescritura de =         ==
		=========================================
		==           Sobrecarga de =           ==
		=========================================
		== Fija el valor de la variable al     ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		void operator = (Integer valor) {
			this->valor = valor.getValor ();
		}
		/*
		=========================================
		==           Sobrecarga de +           ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		Integer operator + (int valor) {
			Integer res;
			valor += this->valor;
			res = valor;
			return res;
		}
		/*
		=========================================
		==         Sobreescritura de +         ==
		=========================================
		==           Sobrecarga de +           ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		Integer operator + (Integer valor) {
			Integer res;
			int acumulador = this->valor + valor.getValor();
			res = acumulador;
			return res;
		}
		/*
		=========================================
		==           Sobrecarga de +=          ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		void operator += (int valor) {
			this->valor += valor;
		}
		/*
		=========================================
		==         Sobreescritura de +=        ==
		=========================================
		==           Sobrecarga de +=          ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		void operator += (Integer valor) {
			this->valor += valor.getValor();
		}
		/*
		=========================================
		==           Sobrecarga de -           ==
		=========================================
		== Resta el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		Integer operator - (int valor) {
			Integer res;
			valor -= this->valor;
			res = valor;
			return res;
		}
		/*
		=========================================
		==         Sobreescritura de -         ==
		=========================================
		==           Sobrecarga de -           ==
		=========================================
		== Resta el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		Integer operator - (Integer valor) {
			Integer res;
			int acumulador = this->valor - valor.getValor();
			res = acumulador;
			return res;
		}
		/*
		=========================================
		==           Sobrecarga de -=          ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		void operator -= (int valor) {
			this->valor -= valor;
		}
		/*
		=========================================
		==         Sobreescritura de -=        ==
		=========================================
		==           Sobrecarga de -=          ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		void operator -= (Integer valor) {
			this->valor -= valor.getValor();
		}
		/*
		=========================================
		==           Sobrecarga de *           ==
		=========================================
		== Multiplica el valor de la variable  ==
		== al valor que se ha igualado.        ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		Integer operator * (int valor) {
			Integer res;
			valor *= this->valor;
			res = valor;
			return res;
		}
		/*
		=========================================
		==         Sobreescritura de *         ==
		=========================================
		==           Sobrecarga de *           ==
		=========================================
		== Multiplica el valor de la variable  ==
		== al valor que se ha igualado.        ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		Integer operator * (Integer valor) {
			Integer res;
			int acumulador = this->valor * valor.getValor();
			res = acumulador;
			return res;
		}
		/*
		=========================================
		==           Sobrecarga de *=          ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		void operator *= (int valor) {
			this->valor *= valor;
		}
		/*
		=========================================
		==         Sobreescritura de *=        ==
		=========================================
		==           Sobrecarga de *=          ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		void operator *= (Integer valor) {
			this->valor *= valor.getValor();
		}
		/*
		=========================================
		==           Sobrecarga de /           ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		Integer operator / (int valor) {
			Integer res;
			valor /= this->valor;
			res = valor;
			return res;
		}
		/*
		=========================================
		==         Sobreescritura de /         ==
		=========================================
		==           Sobrecarga de /           ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		Integer operator / (Integer valor) {
			Integer res;
			int acumulador = this->valor / valor.getValor();
			res = acumulador;
			return res;
		}
		/*
		=========================================
		==           Sobrecarga de /=          ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		void operator /= (int valor) {
			this->valor /= valor;
		}
		/*
		=========================================
		==         Sobreescritura de /=        ==
		=========================================
		==           Sobrecarga de /=          ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		void operator /= (Integer valor) {
			this->valor /= valor.getValor();
		}
		/*
		=========================================
		==           Sobrecarga de %           ==
		=========================================
		== Obtiene el resto del Integer entre  ==
		== el valor pasado por parametro.      ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		Integer operator % (int valor) {
			Integer res;
			valor %= this->valor;
			res = valor;
			return res;
		}
		/*
		=========================================
		==         Sobreescritura de %         ==
		=========================================
		==           Sobrecarga de %           ==
		=========================================
		== Obtiene el resto del Integer entre  ==
		== el valor pasado por parametro.      ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		Integer operator % (Integer valor) {
			Integer res;
			int acumulador = this->valor % valor.getValor();
			res = acumulador;
			return res;
		}
		/*
		=========================================
		==           Sobrecarga de %=          ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		void operator %= (int valor) {
			this->valor %= valor;
		}
		/*
		=========================================
		==         Sobreescritura de %=        ==
		=========================================
		==           Sobrecarga de %=          ==
		=========================================
		== Añade el valor de la variable al    ==
		== valor que se ha igualado.           ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		void operator %= (Integer valor) {
			this->valor %= valor.getValor();
		}
		/*
		=========================================
		==           Sobrecarga de ==          ==
		=========================================
		== Compara                             ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		bool operator == (int valor) {
			return (this->valor == valor);
		}
		/*
		=========================================
		==         Sobreescritura de ==        ==
		=========================================
		==           Sobrecarga de ==          ==
		=========================================
		== Compara                             ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		bool operator == (Integer valor) {
			return (this->valor == valor.getValor());
		}
		/*
		=========================================
		==           Sobrecarga de ++          ==
		=========================================
		== Incrementa 1                        ==
		=========================================
		*/
		void operator ++ () {
			this->valor++;
		}
		/*
		=========================================
		==           Sobrecarga de --          ==
		=========================================
		== Decrementa 1                        ==
		=========================================
		*/
		void operator -- () {
			this->valor--;
		}
		/*
		=========================================
		==           Sobrecarga de <           ==
		=========================================
		== Compara (menor que)                 ==
		=========================================
		*/
		bool operator < (int valor) {
			return (this->valor < valor);
		}
		/*
		=========================================
		==         Sobreescritura de <         ==
		=========================================
		==           Sobrecarga de <           ==
		=========================================
		== Compara (menor que)                 ==
		=========================================
		*/
		bool operator < (Integer valor) {
			return (this->valor < valor.getValor ());
		}
		/*
		=========================================
		==           Sobrecarga de >           ==
		=========================================
		== Compara (mayor que)                 ==
		=========================================
		*/
		bool operator > (int valor) {
			return (this->valor > valor);
		}
		/*
		=========================================
		==         Sobreescritura de >         ==
		=========================================
		==           Sobrecarga de >           ==
		=========================================
		== Compara (mayor que)                 ==
		=========================================
		*/
		bool operator > (Integer valor) {
			return (this->valor > valor.getValor());
		}
		/*
		=========================================
		==           Sobrecarga de <=          ==
		=========================================
		== Compara (menor o igual que)         ==
		=========================================
		*/
		bool operator <= (int valor) {
			return (this->valor <= valor);
		}
		/*
		=========================================
		==         Sobreescritura de <=        ==
		=========================================
		==           Sobrecarga de <=          ==
		=========================================
		== Compara (menor o igual que)         ==
		=========================================
		*/
		bool operator <= (Integer valor) {
			return (this->valor <= valor.getValor ());
		}
		/*
		=========================================
		==           Sobrecarga de >=          ==
		=========================================
		== Compara (mayor o igual que)         ==
		=========================================
		*/
		bool operator >= (int valor) {
			return (this->valor >= valor);
		}
		/*
		=========================================
		==         Sobreescritura de >=        ==
		=========================================
		==           Sobrecarga de >=          ==
		=========================================
		== Compara (mayor o igual que)         ==
		=========================================
		*/
		bool operator >= (Integer valor) {
			return (this->valor >= valor.getValor());
		}
		/*
		=========================================
		==              Metodo pow             ==
		=========================================
		== Operación potencia, si hiciera un   ==
		== intérprete, sería una 'sobrecarga'  ==
		== de '**'.                            ==
		==                                     ==
		== Admite un (int)                     ==
		=========================================
		*/
		Integer pow (int exp) {
			Integer res;
			res = 1;
			if (exp == 0 && this->valor == 0) {
				printf("\033[31;40mTanto la base como el exponente deben ser distintos de 0.\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
			if (!exp)
				return res;

			for (int i = 0; i < exp; i++)
				res = res * this->valor;

			return res;
		}
		/*
		=========================================
		==              Metodo pow             ==
		=========================================
		== Operación potencia, si hiciera un   ==
		== intérprete, sería una 'sobrecarga'  ==
		== de '**'.                            ==
		==                                     ==
		== Admite un (Integer)                 ==
		=========================================
		*/
		Integer pow (Integer exp) {
			return this->pow(exp.getValor());
		}
		/*
		=========================================
		==              Metodo sqrt            ==
		=========================================
		== Operación raiz cuadrada.            ==
		=========================================
		*/
		Integer sqrtd () {
			Integer res;
			res = sqrt (this->valor);
			return res;
		}
		/*
		=========================================
		==              Metodo max             ==
		=========================================
		== Retorna el valor máximo que puede   ==
		== albergar este tipo de dato.         ==
		=========================================
		*/
		static long long int max () {
			return LLONG_MAX;
		}
		/*
		=========================================
		==              Metodo min             ==
		=========================================
		== Retorna el valor minimo que puede   ==
		== albergar este tipo de dato.         ==
		=========================================
		*/
		static long long int min () {
			return LLONG_MIN;
		}
		/*
		=========================================
		==           Metodo getValor           ==
		=========================================
		== Es un getter de la propiedad 'valor'==
		=========================================
		*/
		long long int getValor () {
			return this->valor;
		}
		/*
		=========================================
		==           Metodo parseInt           ==
		=========================================
		== Convierte a Integer un const char * ==
		=========================================
		*/
		static Integer parseInt (const char *frase) {
			bool esEntero = true;
			for (int i = 0; i < strlen (frase); i++)
				if (!(*(frase + i) < ':' && *(frase + i) > '/'))
					esEntero = false;

			if (esEntero) {
				Integer res;
				res = atoi (frase);
				return res;
			} else {
				printf ("\033[31;40mEl parámetro pasado a Integer::parseInt() contiene caracteres no numericos.\n\033[97;40m]]");
				exit (EXIT_FAILURE);
			}
		}
		/*
		=========================================
		==        Sobrecarga de parseInt       ==
		=========================================
		==           Metodo parseInt           ==
		=========================================
		== Convierte a Integer un String       ==
		=========================================
		*/
		static Integer parseInt (String frase) {
			return Integer::parseInt(frase.toString ());
		}
		// ME DAN ERROR TODAS ESTAS SENTENCIAS
		// ~Integer() {
		// 	free (this->res);
		// 	delete &this->valor;
		// 	delete this->res;
		// }
	private:
		char *res;
		long long int valor;
};

class Float : public Object
{
	public:
		Float () {}
		Float (int num) {
			this->valor = (double) num;
		}
		Float (float num) {
			this->valor = (double) num;
		}
		Float (double num) {
			this->valor = num;
		}
		/*
		=========================================
		==           Metodo toString           ==
		=========================================
		== Se utiliza en casi todos lados      ==
		=========================================
		*/
		const char *toString () {
			char *res = (char *) malloc (sizeof (char) * 35);
			sprintf (res, "%lf", this->valor);
			return (const char *) res;
		}
		/*
		=========================================
		==             Metodo type             ==
		=========================================
		== Lo creé para poder comparar tipos   ==
		== independientemente de la función    ==
		== type ()                             ==
		=========================================
		*/
		const char *type () {
			return "Float";
		}
		void setValor (const char *frase) {return;}
	private:
		double valor;
};

class Array : public Object
{
	public:
		Array () {
			this->len = 0;
			this->array = (Object **) malloc (sizeof (Object *) * this->len);
		}
		/*
		=========================================
		==           Metodo toString           ==
		=========================================
		== Se utiliza en casi todos lados      ==
		=========================================
		*/
		// const char *toString () {
		// 	int tamano = 2;
		// 	char *res = (char *) malloc (sizeof (char) * tamano);
		// 	strcpy (res, "[");
		// 	for (int i = 0; i < this->len; i++) {

		// 		tamano += (sizeof (char) * strlen ((*this->array + i)->toString()));
		// 		res = (char *) realloc (res, tamano);
		// 		strcat (res, (*this->array + i)->toString ());

		// 		if (i < this->len - 1) {

		// 			tamano += sizeof (char) * 2;
		// 			res = (char *) realloc (res, tamano);
		// 			strcat (res, ", ");
		// 		}
		// 	}
		// 	strcat (res, "]");
		// 	return (const char *) res;
		// }
		// const char *toString () {
		// 	int longitud = 2;
		// 	char *res = NULL;
		// 	for (int i = 0; i < this->len; i++)
		// 		longitud += strlen ((*this->array + i)->toString ());

		// 	printf ("Longitud: %i\n", this->len);
		// 	res = (char *) malloc (sizeof (char) * longitud);
		// 	strcpy (res, "[");
		// 	for (int i = 0; i < this->len; i++)
		// 		strcat (res, (*this->array + i)->toString ());

		// 	strcat (res, "]");
		// 	return "Hola";
		// }
		const char *toString () {
			String resultado;
			resultado = "[";
			printf ("%s", resultado.toString ());
			for (int i = 0; i < this->len; i++) {
				resultado = resultado + (*this->array + i)->toString ();
				printf ("%s", resultado.toString ());
			}
			resultado = resultado + "]";
			printf ("%s", resultado.toString ());
			return resultado.toString ();
		}
		/*
		=========================================
		==             Metodo type             ==
		=========================================
		== Lo creé para poder comparar tipos   ==
		== independientemente de la función    ==
		== type ()                             ==
		=========================================
		*/
		const char *type () {
			return "Array";
		}
		void setValor (const char *frase) {return;}
		/*
		=========================================
		==            Metodo append            ==
		=========================================
		== Añade items al array.               ==
		=========================================
		*/
		void append (Object *item) {
			this->len++;
			array = (Object **) realloc (array, sizeof (Object *) * this->len);
			*(this->array + this->len - 1) = item;
		}
		/*
		=========================================
		==            Metodo length            ==
		=========================================
		== Indica la longitud del array.       ==
		=========================================
		*/
		int length () {
			return this->len;
		}
		/*
		=========================================
		==           Sobrecarga de []          ==
		=========================================
		== Extrae un elemento del array.       ==
		=========================================
		*/
		Object *operator [] (int index) {
			if (index < this->len && index > -1)
				return *(array + index);
			else if (index >= (-1 * this->len) && index < 0)
				return *(array + (-1 * index));
			else {
				printf ("\033[31;40mEl indice debe ser menor que la longitud de la lista y mayor que la longitud de la lista en negativo.\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
		}
	private:
		Object **array;
		int len;

		// int getTamano () {
		// 	return this->len * sizeof (Object *);
		// }
};

/*
=======================================================
== Se usa para concatenar 2 variables                ==
== Se pueden crear árboles binarios con esta funcion ==
=======================================================
*/
Object *Concat (Object *cosa1, Object *cosa2) {
	Object *temp(NULL);
	temp = new String;
	char *resultado = (char *) malloc (sizeof (char) * (strlen (cosa1->toString ()) + strlen (cosa2->toString ())));
	strcpy (resultado, cosa1->toString());
	strcat (resultado, cosa2->toString());
	temp->setValor((const char *) resultado);
	return temp;
}
/*
=============================================================
== Se usa para concatenar 2 o mas variables (No operativa) ==
=============================================================
*/
// Object *Concat (Object *cosa1, Object *cosa2, ...) {
// 	va_list argumentos;
// 	int i;
// 	int tamano = sizeof (char) * (strlen (cosa1->toString ()) + strlen (cosa2->toString ()));

// 	char *resultado = (char *) malloc (tamano);
// 	strcpy (resultado, cosa1->toString());
// 	strcat (resultado, cosa2->toString());

// 	for (i = cosa2; i == NULL; i = va_arg (argumentos, Object*)) {
// 		tamano += (sizeof (char) * strlen (i->toString()));
// 		resultado = (char *) realloc (resultado, tamano);
// 		strcat (resultado, i->toString());
// 	}
// }

// ------------------- print ---------------------
void print (Object *texto, const char *fin = "\n") {
	printf("%s%s", texto->toString(), fin);
}

void print (const char *texto, const char *fin = "\n") {
	printf("%s%s", texto, fin);
}

void print (char *texto, const char *fin = "\n") {
	printf("%s%s", texto, fin);
}

void print (char texto, const char *fin = "\n") {
	printf("%c%s", texto, fin);
}

void print (int texto, const char *fin = "\n") {
	printf("%i%s", texto, fin);
}

void print (short int texto, const char *fin = "\n") {
	printf("%hi%s", texto, fin);
}

void print (long int texto, const char *fin = "\n") {
	printf("%li%s", texto, fin);
}

void print (long long int texto, const char *fin = "\n") {
	printf("%lli%s", texto, fin);
}

void print (unsigned texto, const char *fin = "\n") {
	printf("%u%s", texto, fin);
}

void print (unsigned short texto, const char *fin = "\n") {
	printf("%hu%s", texto, fin);
}

void print (long unsigned texto, const char *fin = "\n") {
	printf("%lu%s", texto, fin);
}

void print (long long unsigned texto, const char *fin = "\n") {
	printf("%llu%s", texto, fin);
}

void print (float texto, const char *fin = "\n") {
	printf("%f%s", texto, fin);
}

void print (double texto, const char *fin = "\n") {
	printf("%lf%s", texto, fin);
}

void print (bool texto, const char *fin = "\n") {
	printf("%s%s", (texto) ? "True" : "False", fin);
}

// ------------------- type (Object) ----------------------
Object *type (Object *var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * (strlen (var->type()) + 10));
	sprintf (res, "<class '%s'>", var->type());
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (const char *) ----------------------
Object *type (const char *var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 22);
	sprintf (res, "<class 'const char *'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (char *) ----------------------
Object *type (char *var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 16);
	sprintf (res, "<class 'char *'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (char) ----------------------
Object *type (char var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 14);
	sprintf (res, "<class 'char'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (int) ----------------------
Object *type (int var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 13);
	sprintf (res, "<class 'int'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (short) ----------------------
Object *type (short var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 15);
	sprintf (res, "<class 'short'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (long int) ----------------------
Object *type (long int var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 18);
	sprintf (res, "<class 'long int'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (long long int) ----------------------
Object *type (long long int var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 23);
	sprintf (res, "<class 'long long int'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (unsigned) ----------------------
Object *type (unsigned var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 18);
	sprintf (res, "<class 'unsigned'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (long unsigned) ----------------------
Object *type (long unsigned var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 23);
	sprintf (res, "<class 'long unsigned'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (long long unsigned) ----------------------
Object *type (long long unsigned var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 28);
	sprintf (res, "<class 'long long unsigned'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (float) ----------------------
Object *type (float var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 15);
	sprintf (res, "<class 'float'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (double) ----------------------
Object *type (double var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 16);
	sprintf (res, "<class 'double'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (bool) ----------------------
Object *type (bool var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 14);
	sprintf (res, "<class 'bool'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}


int main(int argc, char const *argv[])
{
	String prueba, prueba2, compara1, compara2;
	Integer entero, entero2, max, parse, potencia; // int
	Boolean booleano; // bool
	Array miArray;
	// String res;
	compara1 = "Diego";
	compara2 = "Diego";
	entero = 2;
	potencia = entero.pow (10);
	entero2 = 73;
	// max = Integer::max ();
	prueba = "Hola";
	prueba2 = "Ke Pacha";
	prueba = prueba2;
	entero = entero2;
	prueba = prueba + " cruc";
	// res = prueba * 3;
	print (&prueba);

	print (Concat (new String ("La potencia de 2 elevado a 10 es "), &potencia));

	print (prueba[-7]);
	// print (&res);
	print ("Adios");

	print (&entero);

	print (Integer::min ());
	// print (&max);

	print (type (&prueba));

	print (type (&entero));

	print (23);

	print (Concat (&entero, Concat(&prueba, &entero2)));

	print ('D', "");

	print (" de Diego");

	print (3.1415);

	print (type (3.4));

	printf ("Longitud de prueba: %i\n", prueba.length());

	parse = Integer::parseInt ("123456");

	print (&parse);

	print ((compara1 == compara2));

	miArray.append (&prueba);
	miArray.append (new Integer(73));
	miArray.append (&booleano);
	// miArray.append (&entero);
	print ("Longitud de miArray: ", "");
	print (miArray.length());
	print (miArray[1]);
	print ("[", "");
	for (int i = 0; i < miArray.length(); i++) {
		print (miArray[i], "");
		print (", ", "");
	}
	printf ("\b\b");
	print ("]");
	print (&miArray);
	return 0;
}
