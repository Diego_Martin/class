#ifndef __BOOLEAN_H__
#define __BOOLEAN_H__

#include "Object.h"
#include "String.h"
#include "Integer.h"

class Boolean : public Object
{
	public:
		Boolean () {}
		Boolean (bool valor) {
			this->valor = valor;
		}
		Boolean (int valor) {
			this->valor = (bool) valor;
		}

		const char *toString () {
			return (this->valor) ? "True" : "False";
		}

		const char *type () {
			return "Boolean";
		}
		void setValor (const char *frase) {return;}
		
		bool getValor () {
			return this->valor;
		}
		
		void operator = (bool valor) {
			this->valor = valor;
		}
		
		void operator = (Boolean valor) {
			this->valor = valor.getValor ();
		}
		
		void operator = (String valor) {
			if (valor == "")
				this->valor = false;
			else
				this->valor = true;
		}
		
		void operator = (int valor) {
			if (valor == 0)
				this->valor = false;
			else
				this->valor = true;
		}
		
		bool operator ! () {
			return !this->valor;
		}
	private:
		bool valor;
};

#endif