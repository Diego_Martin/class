#include "datos.h"

int main(int argc, char const *argv[])
{
	Integer miInt;
	String miString;
	Array miArray;

	miInt = 5;
	miString = "Hola mundo";

	miArray.append (&miInt);
	miArray.append (&miString);

	print (&miInt);
	print (&miString);

	printf ("[");
	for (int i = 0; i < miArray.length (); i++) {
		printf("%s", miArray [i]->toString ());
		if (i < miArray.length () - 1)
			printf (", ");
	}
	printf ("]\n");

	print (&miArray);
	return 0;
}