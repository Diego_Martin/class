#include "CPL.h"
#include <stdio.h>

// Prueba
class Prueba : public Object {
	public:
		void print () {
			printf ("Esto es prueba.\n");
		}
		const char *__name__ () {
			return "Prueba";
		}
};

int main (int argc, char *argv []) {
	Object *var (NULL); // Ignora esto

	var = new Integer (-3);
	print (var);

	var = new Boolean (true);
	print (var);

	var = new String ("Hola mundo.");
	print (var);
	print (new String ("Longitud: "), new String (""));
	print (len (var));

	var = new Float (3.1415);
	print (var);

	var = new None ();
	print (var);

	File *vari = new File ("./prueba.txt");
	print (new String ("Hola que tal"), NULL, vari);
	print (vari);
	print (len (vari));
	print (new String ("Palabras: "), new String (""));
	print (vari->words ());
	print (new String ("El fichero 'vari' ocupa:"), Sys::Sizeof (vari));
	print (new String ("El fichero 'vari' ocupa:"), Sys::Sizeof (vari));

	print (new String ("String de prueba en Stdvbs."), NULL, Sys::Stdvbs);
	print (new String ("String de prueba en Stderr."), NULL, Sys::Stderr);

	Array *miArray = new Array ();
	print (new String ("El array 'miArray' ocupa:"), Sys::Sizeof (miArray));
	miArray->append (var);
	print (new String ("El array 'miArray' ocupa:"), Sys::Sizeof (miArray));
	miArray->append (new Float (73.73));
	print (new String ("El array 'miArray' ocupa:"), Sys::Sizeof (miArray));
	miArray->append (new Boolean (true));
	print (new String ("El array 'miArray' ocupa:"), Sys::Sizeof (miArray));
	miArray->append (new String ("Hola Jose Luis"));
	print (new String ("El array 'miArray' ocupa:"), Sys::Sizeof (miArray));
	miArray->append (new Integer (729));
	print (new String ("Eleccion aleatoria: "), Random::Choice (miArray));

	Prueba *miPrueba = new Prueba ();
	miPrueba->print ();
	
	print (miPrueba);
	String *substr = new String ("Diego");
	print (new String ("Terminado"), NULL, Sys::Stdvbs);
	print (new String ("Tamaño de un Integer: "), Sys::Sizeof (new Integer (73)));
	print (new String ("Substring[0]:"), substr->operator[] (0), new String (" "), new String ("\n"));
	// print (new String ("Por Sys::Stdin: "), NULL, Sys::Stdin); 
	print (new String ("Numero aleatorio entre 0 y 5: "), Random::RandInt (new Integer (5)));
	print (new String ("Numero aleatorio entre 10 y 15: "), Random::RandInt (new Integer (10), new Integer (15)));
	print (input (new String ("Prueba de input: "))); // Arreglar esto de

	print (new String ("Por cierto este es el Array: "), new String (""));

	print (miArray);
	Array *miArray2 = new Array ();

	miArray2->append (new String ("Pinilla"));
	miArray2->append (new String ("Diego"));
	miArray2->append (new Integer (73));

	miArray->append (miArray2);
	
	Os::System (new String ("toilet -fpagga --gay Funciona!"));
	print (miArray);
	print (new String ("------------------ Acabo con los Arrays ----------------------"), NULL, Sys::Stdvbs);
	// String concat ("Diego");
	String *concat = new String ("Diego");

	*concat = *concat + " es";
	*concat = *concat + new String (" guapo");

	print (concat);
	// print (concat);
	print (*concat * 5);

	Dict *diccionario = new Dict ();
	print (new String ("Lo crea"), NULL, Sys::Stdvbs);
	diccionario->update (new String ("Indice 1"), new Boolean (true));
	diccionario->update (new Integer (73), new Float (73.73));

	print (new String ("Tamaño del diccionario: "), new String (""));
	print (Sys::Sizeof (diccionario));
	print (diccionario);
	print (new String ("diccionario [73]: "), new String (""));
	print (diccionario->operator [] (new Integer (73)));
	print (new String ("diccionario ['Indice 1']: "), new String (""));
	print (diccionario->operator [] (new String ("Indice 1")));
	
	Array *coso = new Array ();
	coso->append (new Integer (53));
	coso->append (vari);
	diccionario->update (new Boolean (true), coso);
	print (diccionario);
	print (new String ("diccionario [True]: "), new String (""));
	print ((*diccionario) [new Boolean (true)]);
	// print (diccionario->operator [] (new Boolean (true))->operator [] (new Integer (0)))
	miArray->append (diccionario);
	print (miArray);
	return EXIT_SUCCESS;
}