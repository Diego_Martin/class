#ifndef __OS_H__
#define __OS_H__

#include "__datos__.h"
#include <stdlib.h>

namespace Os
{

	None *System (String *comando = NULL) {
		if (comando == NULL) {
			fprintf (stderr, "\033[31;40mERROR: No se pudo ejecutar el comando porque el valor del parámetro 'comando' es NULL.\033[97;40m");
			Sys::Exit (new Integer (1));
		}

		system (comando->__repr__ ());
		return new None ();
	}

	// String *Getcwd () {
	// }
}

#endif