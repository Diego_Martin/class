#include "CPL.h"
#include <stdio.h>

// Prueba
class Prueba : public Object {
	public:
		void print () {
			printf ("Esto es prueba.\n");
		}
};

int main (int argc, char *argv []) {
	Object *var (NULL); // Ignora esto

	var = new Integer (-3);
	print (var);

	var = new Boolean (true);
	print (var);

	var = new String ("Hola mundo.");
	print (var);
	print (new String ("Longitud: "), new String (""));
	print (len (var));

	var = new Float (3.1415);
	print (var);

	var = new None ();
	print (var);

	File *vari = new File ("./prueba.txt");
	print (new String ("Hola que tal"), new String ("\n"), vari);
	print (vari);
	print (len (vari));
	print (new String ("Palabras: "), new String (""));
	print (vari->words ());

	print (new String ("String de prueba en Stdvbs."), new String ("\n"), Sys::Stdvbs);
	print (new String ("String de prueba en Stderr."), new String ("\n"), Sys::Stderr);

	Array *miArray = new Array ();
	miArray->append (var);
	miArray->append (new Float (73.73));
	// printf ("miArray->__repr__(): %s\n", miArray->__repr__ ());

	Prueba *miPrueba = new Prueba ();
	miPrueba->print ();
	print (miPrueba);
	// en
	return EXIT_SUCCESS;
}