#ifndef __DICT_H__
#define __DICT_H__

#include "Object.h"

class Dict : public Object
{
	public:
		Dict();
		~Dict();

		const char *__repr__ () {
			// TODO
			return "";
		}

		const char *__name__ () {
			return "Dict";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return this->len;
		}

	private:
		Array valor;
		Array clave;
		int len;
};

#endif