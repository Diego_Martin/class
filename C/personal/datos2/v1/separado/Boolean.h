#ifndef __BOOLEAN_H__
#define __BOOLEAN_H__

#include "Object.h"
#include <string.h>

class Boolean : public Object
{
	public:
		Boolean () {
			this->valor = false;
		}

		Boolean (bool valor) {
			this->valor = valor;
		}

		Boolean (int valor) {
			this->valor = (bool) valor;
		}

		~Boolean();

		const char *__repr__ () {
			return (this->valor) ? "True" : "False";
		}

		const char *__name__ () {
			return "Boolean";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return strlen (this->__repr__ ());
		}

		bool __cvalue__ () {
			return this->valor;
		}

	private:
		bool valor;
};

#endif