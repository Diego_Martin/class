#ifndef __INTEGER_H__
#define __INTEGER_H__

#include "Object.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class Integer : public Object
{
	public:
		Integer () {
			this->valor = 0;
		}

		Integer (int num) {
			this->valor = (long long int) num;
		}

		Integer (float num) {
			this->valor = (long long int) num;
		}

		Integer (double num) {
			this->valor = (long long int) num;
		}

		Integer (long int num) {
			this->valor = (long long int) num;
		}

		Integer (long long int num) {
			this->valor = num;
		}

		Integer (Integer *obj)  {
			this->valor = obj->__getcvalue__ ();
		}

		~Integer();

		const char *__repr__ () {
			int contador = 1;
			long long int aux = this->valor;
			char *res = NULL;
			
			if (aux >= 0) {
				while(aux / 10 > 0) {
					aux /= 10;
					contador++;
				}
				res = (char *) malloc (sizeof (char) * contador);
			} else {
				while(aux * 10 > 0) {
					aux *= 10;
					contador++;
				}
				res = (char *) malloc (sizeof (char) * (contador + 1));
			}
			sprintf (res, "%lli", this->valor);
			return (const char *) res;
		}

		const char *__name__ () {
			return "Integer";
		}

		void operator = (int valor) {
			this->valor = valor;
		}

		void operator = (Integer valor) {
			this->valor = valor.__getcvalue__ ();
		}

		Integer operator + (int valor) {
			Integer res;
			valor += this->valor;
			res = valor;
			return res;
		}

		Integer operator + (Integer valor) {
			return this->operator+(valor.__getcvalue__ ());
		}

		long long int __getcvalue__ () {
			return this->valor;
		}

		static Integer parseInt (const char *frase) {
			bool esEntero = true;
			for (int i = 0; i < strlen (frase); i++)
				if (!(*(frase + i) < ':' && *(frase + i) > '/'))
					esEntero = false;

			if (esEntero) {
				Integer res;
				res = atoi (frase);
				return res;
			} else {
				printf ("\033[31;40mEl parámetro pasado a Integer::parseInt() contiene caracteres no numericos.\n\033[97;40m]]");
				exit (EXIT_FAILURE);
			}
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return strlen (this->__repr__ ());
		}

	private:
		long long int valor;
};

#endif