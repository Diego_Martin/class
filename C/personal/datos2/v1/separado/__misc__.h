#ifndef __MISC_H__
#define __MISC_H__

#include "__datos__.h"
#include "__sys__.h"
#include <stdio.h>

None *print (Object *variable, String *end = new String ("\n"), File *file = Sys::Stdout, Boolean *flush = new Boolean (true)) {
	if (file == Sys::Stdin) {
		fprintf (stderr, "\033[31;40mERROR: No se puede imprimir '%s' en el canal 'Sys::Stdin' (C: stdin) porque 'Sys::Stdin' es un canal de entrada de datos, no de salida.\033[97;40m", variable->__name__ ());
		Sys::_exit (new Integer (1));
	}

	else if (file == Sys::Stdout)
		fprintf (file->__getcfile__ (), "\033[97;40m%s%s\033[97;40m", variable->__repr__ (), end->__repr__ ());

	else if (file == Sys::Stderr)
		fprintf (file->__getcfile__ (), "\033[31;40m%s%s\033[97;40m", variable->__repr__ (), end->__repr__ ());

	else if (file == Sys::Stdvbs)
		fprintf (file->__getcfile__ (), "\033[32;40m%s%s\033[97;40m", variable->__repr__ (), end->__repr__ ());
	
	else {
		char *cosa = (char *) malloc (sizeof(char) * (strlen (variable->__repr__ ()) + strlen (end->__repr__ ())));
		strcpy (cosa, variable->__repr__ ());
		strcat (cosa, end->__repr__ ());
		file->append (new String (cosa));
		free (cosa);
	}

	if (flush->__cvalue__ ())
		fflush (file->__getcfile__ ());
}

None *print (Object *variable, Object *variable2, String *sep = new String (" "), String *end = new String ("\n"), File *file = Sys::Stdout, Boolean *flush = new Boolean (true)) {
	if (file == Sys::Stdin) {
		fprintf (stderr, "\033[31;40mERROR: No se puede imprimir '%s' en el canal 'Sys::Stdin' (C: stdin) porque 'Sys::Stdin' es un canal de entrada de datos, no de salida.\033[97;40m", variable->__name__ ());
		Sys::_exit (new Integer (1));
	}

	else if (file == Sys::Stdout)
		fprintf (file->__getcfile__ (), "\033[97;40m%s%s%s%s\033[97;40m", variable->__repr__ (), sep->__repr__ (), variable2->__repr__ (), end->__repr__ ());

	else if (file == Sys::Stderr)
		fprintf (file->__getcfile__ (), "\033[31;40m%s%s%s%s\033[97;40m", variable->__repr__ (), sep->__repr__ (), variable2->__repr__ (), end->__repr__ ());

	else if (file == Sys::Stdvbs)
		fprintf (file->__getcfile__ (), "\033[32;40m%s%s%s%s\033[97;40m", variable->__repr__ (), sep->__repr__ (), variable2->__repr__ (), end->__repr__ ());
	
	else {
		char *cosa = (char *) malloc (sizeof (char) * (strlen (variable->__repr__ ()) + strlen (variable2->__repr__ ()) + strlen (sep->__repr__ ()) + strlen (end->__repr__ ())));
		strcpy (cosa, variable->__repr__ ());
		strcat (cosa, sep->__repr__ ());
		strcat (cosa, variable2->__repr__ ());
		strcat (cosa, end->__repr__ ());
		file->append (new String (cosa));
		free (cosa);
	}

	if (flush->__cvalue__ ())
		fflush (file->__getcfile__ ());
}

Integer *len (Object *variable) {
	return new Integer (variable->__len__ ());
}

String *type (Object *variable) {
	return new String (variable->__name__ ());
}

#endif