#ifndef __CPL__
#define __CPL__

#ifdef __cplusplus
extern "C" {
#endif

#include "__datos__.h"
#include "__misc__.h"
#include "__sys__.h"
#include "__random__.h"

#ifdef __cplusplus
}
#endif

#endif