#include "CPL.h"
#include <stdio.h>

// Prueba
class Prueba : public Object {
	public:
		void print () {
			printf ("Esto es prueba.\n");
		}
};

int main (int argc, char *argv []) {
	Object *var (NULL); // Ignora esto

	var = new Integer (-3);
	print (var);

	var = new Boolean (true);
	print (var);

	var = new String ("Hola mundo.");
	print (var);
	print (new String ("Longitud: "), new String (""));
	print (len (var));

	var = new Float (3.1415);
	print (var);

	var = new None ();
	print (var);

	File *vari = new File ("./prueba.txt");
	print (new String ("Hola que tal"), NULL, vari);
	print (vari);
	print (len (vari));
	print (new String ("Palabras: "), new String (""));
	print (vari->words ());
	print (new String ("El fichero 'vari' ocupa:"), Sys::Sizeof (vari));
	print (new String ("El fichero 'vari' ocupa:"), Sys::Sizeof (vari));

	print (new String ("String de prueba en Stdvbs."), NULL, Sys::Stdvbs);
	print (new String ("String de prueba en Stderr."), NULL, Sys::Stderr);

	Array *miArray = new Array ();
	print (new String ("El array 'miArray' ocupa:"), Sys::Sizeof (miArray));
	miArray->append (var);
	print (new String ("El array 'miArray' ocupa:"), Sys::Sizeof (miArray));
	miArray->append (new Float (73.73));
	print (new String ("El array 'miArray' ocupa:"), Sys::Sizeof (miArray));
	miArray->append (new Boolean (true));
	print (new String ("El array 'miArray' ocupa:"), Sys::Sizeof (miArray));
	print (new String ("ELeccion aleatoria: "), Random::Choice (miArray));
	// printf ("miArray->__repr__(): %s\n", miArray->__repr__ ())

	Prueba *miPrueba = new Prueba ();
	miPrueba->print ();
	print (miPrueba);
	String *substr = new String ("Diego");
	print (new String ("Terminado"), NULL, Sys::Stdvbs);
	print (new String ("Tamaño de un Integer: "), Sys::Sizeof (new Integer (73)));
	print (new String ("Substring[0]:"), substr->operator[] (0), new String (" "), new String ("\n"));
	// print (new String ("Por Sys::Stdin: "), NULL, Sys::Stdin);
	print (new String ("Numero aleatorio entre 0 y 5: "), Random::RandInt (new Integer (5)));
	print (new String ("Numero aleatorio entre 10 y 15: "), Random::RandInt (new Integer (10), new Integer (15)));
	print (input (new String ("Prueba de input: "))); //
	return EXIT_SUCCESS;
}