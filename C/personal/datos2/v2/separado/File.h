#ifndef __FILE_H__
#define __FILE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "Object.h"
#include "Integer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class File : public Object
{
	public:
		File (const char *ruta = NULL) {
			if (ruta != NULL) {
				this->nombre = (char *) malloc (sizeof (char) * (strlen (ruta) + 1));
				strcpy (this->nombre, ruta);
				printf ("%s\n", this->nombre);
			} else {
				this->valor = stdout;
				this->nombre = NULL;
			}
		}
		
		File (FILE *param) {
			this->valor = param;
			this->nombre = NULL;
		}
		// ~File();
		const char *__repr__ () {
			char *res = (char *) malloc (sizeof (char) * (13 + strlen (this->__name__ ()) + 14));
			sprintf (res, "<%s class at %p>", this->__name__ (), (void *) this);
			return (const char *) res;
		}

		const char *__name__ () {
			return "File";
		}

		int __len__ () {
			int res = 0;
			if (this->nombre == NULL) {
				fprintf (stderr, "\033[31;40mERROR: No se pudo encontrar el nombre de archivo.\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
			this->valor = fopen (this->nombre, "r");
			if (this->valor == NULL) {
				fprintf (stderr, "\033[31;40mERROR: No se pudo abrir el archivo.\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
			for (char c = getc (this->valor); c != EOF; c = getc (this->valor))
				res ++;

			fclose (this->valor);
			return res;
		}

		int __size__ () {
			return sizeof (*this);
		}

		Integer *words () {
			int res = 0;
			char ch;
			if (this->__len__ () > 0) {
				this->valor = fopen (this->nombre, "r");
				if (this->valor == NULL) {
					fprintf (stderr, "\033[31;40mERROR: No se pudo abrir el archivo.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
				res ++;
				while ((ch = fgetc (this->valor)) != EOF)
					if (ch == ' ')
						res ++;

				fclose (this->valor);
				return new Integer (res);
			} else
				return new Integer (0);
		}

		Integer *letters () {
			return new Integer (this->__len__ ());
		}

		void __setstr__ (const char *frase) {
			this->valor = fopen (this->nombre, "w+");
			fputs (frase, this->valor);
			fclose (this->valor);
		}

		FILE *__getcfile__ () {
			return this->valor;
		}

		void append (String *texto = new String ("")) {
			this->valor = fopen (this->nombre, "a+");
			fprintf (this->valor, "%s", texto->__repr__ ());
			fclose (this->valor);
		}

	private:
		FILE *valor;
		char *nombre;
};

#ifdef __cplusplus
}
#endif

#endif