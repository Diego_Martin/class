#ifndef __DICT_H__
#define __DICT_H__

#include "Object.h"
#include "String.h"
#include "Array.h"
#include "None.h"
#include "IndexNotInDictException.h"
#include "IndexAlreadyExistsException.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>

class Dict : public Object
{
	public:
		Dict () {}
		~Dict () {
			delete &this->valor;
			delete &this->clave;
		}

		const char *__repr__ () {
			stringstream canal;
			canal << "{";
			for (int i = 0; i < this->valor.__len__ (); i++) {
				canal << ((strcmp (this->clave [i]->__name__ (), "String") == 0) ? "'" : "") << this->clave [i]->__repr__ () << ((strcmp (this->clave [i]->__name__ (), "String") == 0) ? "'" : "") << ": " << ((strcmp (this->valor [i]->__name__ (), "String") == 0) ? "'" : "") << this->valor [i]->__repr__ () << ((strcmp (this->valor [i]->__name__ (), "String") == 0) ? "'" : "");

				if (i < this->valor.__len__ () - 1)
					canal << ", ";
			}
			canal << "}";
			const string& res = canal.str ();
			String *ret = new String (res.c_str ());
			return ret->__repr__ ();
		}

		const char *__name__ () {
			return "Dict";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return this->valor.__len__ ();
		}

		int __size__ () {
			return sizeof (*this);
		}

		Object *operator [] (Object *index) {
			bool encontrado = false;
			int i = 0;
			for (; i < this->clave.__len__ (); i++)
				if (strcmp (this->clave[i]->__repr__ (), index->__repr__ ()) == 0 && strcmp (this->clave[i]->__name__ (), index->__name__ ()) == 0) {
					encontrado = true;
					break;
				}
			
			if (!encontrado)
				throw new IndexNotInDictException (index, this, this);

			return this->valor [i];
		}

		None *update (Object *key, Object *valor) {
			// printf("En el update\n");
			// for (int i = 0; i < this->clave.__len__ (); i++) {
			// 	printf("Iterador, i: %i\n", i);
			// 	printf("Condicion: %s\n", (strcmp (this->clave [i]->__repr__ (), key->__repr__ ()) == 0) ? "True" : "False");
			// 	if (strcmp (this->clave [i]->__repr__ (), key->__repr__ ()) == 0) {
			// 		printf("En el if\n");
			// 		throw new IndexAlreadyExistsException (valor, this);
			// 	}
			// }
			
			this->clave.append (key);
			this->valor.append (valor);
		}

	private:
		Array valor;
		Array clave;
};

#endif