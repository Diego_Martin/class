#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<typeinfo>
#include<stdarg.h>

int Escribir (const char *format, ...)
{
	va_list arg;
	int done;

	va_start (arg, format);
	done = vfprintf (stdout, format, arg);
	va_end (arg);

	return done;
}

int main (int argc, char *argv[]) {
	int num = 73;
	Escribir ("Hola, num = %i\n", num);
//  print (45);
//  print (true);
//  print (0x60);
//  print ((unsigned char) 0x60);
//  printf ("%s\n", typeid("Hola").name());
//  print ((double) 23.4);
//  print ((char *)"Hola");
  return EXIT_SUCCESS;
}
