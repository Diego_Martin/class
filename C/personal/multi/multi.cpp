#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <typeinfo>
#include <string.h>
#include <sstream>
using namespace std;

string uint64ToString(uint64_t input) {
	string result = "";
	uint8_t base = 10;
	do {
		char c = input % base;
		input /= base;
		if (c < 10)
			c +='0';
		else
			c += 'A' - 10;
		result = c + result;
	} while (input);
	return result;
}

class Objeto
{
	public:
		Objeto () {
			printf("Instanciado\n");
		}
		virtual char *aCadenaParaImprimir (); //{
			//char *ret;
			//sprintf (ret, "");
			//return ret;
		//}
};

class Nada : public Objeto
{
	public:
		char *aCadenaParaImprimir () {
			return (char *)"Nada";
		}
};

class Ent : public Objeto
{
	public:
		// Ent();
		// ~Ent();
		int64_t getValor () {
			return this->valor;
		}
		void setValor (int64_t num) {
			// printf(__PRETTY_FUNCTION__);
			this->valor = num;
		}
		char *aCadenaParaImprimir () {
			printf("HolaAqui\n");
			// char *ret;
			// _i64toa(this->valor, ret, 10); // Mal
			return (char *) "HolaAqui\n";
		}
		char *retHola () {
			char *res = (char *)"Hola";
			return res;
		}
	private:
		int64_t valor;
};

template <class Type>
const char *tipo(Type var) {
	const char *str;
	str = "<clase 'Nada'>";
	if (typeid (var) == typeid (Ent))
		str = "<clase 'Ent'>";
	// if (typeid (var) == typeid (Objeto))
	//	str = "<clase 'Objeto'>";
	return str;
}

int main(int argc, char const *argv[])
{
	Ent var;
	var.setValor(45);
	int64_t valDeVar = var.getValor();
	printf("%li\n", valDeVar);
	printf("%s\n", var.retHola());
	// char *cad = var.aCadenaParaImprimir();
	// printf("%llu\n", var.getValor());
	printf("%s\n", var.aCadenaParaImprimir());
	// printf("%s\n", tipo (var));
	return EXIT_SUCCESS;
}
