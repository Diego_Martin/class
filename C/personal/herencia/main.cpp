#include <stdio.h>

class Base
{
public:
	virtual void print () {
		printf ("Base\n");
	}
};

class Derivada : public Base
{
public:
	void print ()  {
		printf("Derivada\n");
	}
};

class Derivada2 : public Base
{
public:
	void print ()  {
		printf("Derivada2\n");
	}
};

void haber (Base *obj) {
	obj->print ();
}

int main(int argc, char const *argv[])
{
	Derivada *der1 = new Derivada ();
	Derivada2 *der2 = new Derivada2 ();
	Base *base = new Base ();
	haber (der1);
	haber (der2);
	haber (base);
	return 0;
}