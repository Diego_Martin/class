#include <stdio.h>
#include <stdlib.h>

#define MAX 0x30

struct TEmpleado {
	char nombre[MAX];
	char apellidos[MAX];
	double salario;
	struct TEmpleado *siguiente;
};

const char *opts[] = {
	"Altas",
	"Listar",
	"Salir",
	NULL
};

void title () {
	system ("clear");
	system ("toilet -fpagga --gay 'Gestion de usuarios'");
	printf("\n\n");
}

unsigned menu () {
	unsigned opcion;
	static unsigned numOpt = sizeof (opts) / sizeof (char *);

	do {
		title ();
		for (int op = 0; opts[op] != NULL; op++)
			printf("\t%i.- %s\n", op + 1, opts[op]);
		printf("\n");
		printf("\tOpcion: ");
		scanf (" %u", &opcion);
		opcion --;
	} while (opcion >= numOpt - 1);
	return opcion;
}

void datos_empleado (struct TEmpleado *d) {
	title ();
	printf("Nombre: ");
	scanf (" %s", d->nombre); // Accedo al campo 'nombre' de aquello a lo que apunta 'd'
	printf("Apellidos: ");
	scanf (" %s", d->apellidos);
	printf("Salario: ");
	scanf (" %lf", &d->salario);
	// printf("Hola\n");
}

void altas (struct TEmpleado **inicio) {
	struct TEmpleado *ultimo = *inicio;
	struct TEmpleado *nuevo = (struct TEmpleado *) malloc ( sizeof (struct TEmpleado) );
	datos_empleado (nuevo);
	nuevo->siguiente = NULL;

	if (!ultimo) {
		*inicio = nuevo;
		return;
	}
	while (ultimo->siguiente != NULL)
		ultimo = ultimo->siguiente;

	ultimo->siguiente = nuevo;
}

void print (struct TEmpleado *item) {
	printf("\tNombre: %s\n", item->nombre);
	printf("    \tApellidos: %s\n", item->apellidos);
	printf("    \tSalario: %lf\n", item->salario);
}

void listar (struct TEmpleado **list) {
	struct TEmpleado *ultimo = *list;
	int cont = 0;
	int numItems = sizeof (list) / sizeof (struct TEmpleado *);

	while (ultimo->siguiente != NULL)
		cont ++;
		printf("%2i.-", cont);
		print (ultimo);
		ultimo = ultimo->siguiente;
}

void liberar (struct TEmpleado *l) {
	if (l->siguiente)
		liberar (l->siguiente);

	free (l);
}

void (*fn[]) (struct TEmpleado **) = { &altas, &listar };

int main(int argc, char const *argv[])
{
	unsigned opcion;
	struct TEmpleado *list = NULL;

	do {
		opcion = menu ();
		(*fn[opcion]) (&list);
	} while (opcion != sizeof (opts) / sizeof (char *));

	liberar (list);
	printf("\n");
	return EXIT_SUCCESS;
}