#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x20

/* Esto no reserva memoria */
struct TPersonaje {
	int municion;
	double vida;
	char nombre[MAX]; /* Para meter un valor en esta propiedad se necesita strcpy() */
};

void strctcpy (struct TPersonaje *dest, struct TPersonaje res) {
	dest->municion = res.municion;
	dest->vida = res.vida;
	dest->nombre = (char[MAX]) malloc (sizeof(res.nombre));
	strcpy (dest->nombre, res.nombre);
}

int main(int argc, char const *argv[])
{
	/* Declaración: struct TPersonaje */
	/* Nombre:      marioBros         */
	/* Valor:       { 0x84, .9 }      */
	/* Esto si reserva memoria        */
	struct TPersonaje marioBros = { 0x84, .9, "Mario" };
	struct TPersonaje luigiBros = { 0x85, 1., "Luigi" };
	/* Cambio de valor                */
	marioBros.vida = .8;
	strctcpy (&luigiBros, marioBros);
	/* Puntero a marioBros            */
	// struct Tpersonaje *puntero = &marioBros;
	printf("%s\n", marioBros.nombre);
	return EXIT_SUCCESS;
}