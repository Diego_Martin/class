#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define NT 10             /* Número de datos de la señal */
#define MAX_VALOR 5.0     /* Rango de la señal */
#define MIN_VALOR 0.0

/*    //## INICIO codigo fuente respuesta del ALUMNO ##   */
void suavizarDatos (float sen [NT]) {
  float media = 0;
  /*Calculo la media*/
  for (int i = 0; i < NT; i++) {
    media += sen [i];
  }
  media /= NT;

  for (int i = 0; i < NT; i++) {
    /* Si es par */
    if (i % 2 == 0) {
      /*Si el valor es superior a la media (Se me había olvidado) */
      if (media < sen[i])
      {
        printf("Valor inicial: %f\n", sen[i]);
        /*Si la distancia asociada es 0.4 veces mayor ... */
        if (media + 0.4 < sen[i])
        {
          sen[i] = media + 0.3;
        }
      }
    /* Si es impar */
    } else {
      if (media > sen[i])
      {
        printf("Valor inicial: %f\n", sen[i]);
        if (media - sen[i] > 0.4)
        {
          sen[i] = media - 0.3;
        }
      }
    }
  }
}

 /*    //## FIN codigo fuente respuesta del ALUMNO ##   */

 int main (){
  float sen[NT];
  int i;

  srand(time(0));
 
 
  for(i=0;i<NT;i++){
     sen[i]=MIN_VALOR+ rand()/(RAND_MAX/(MAX_VALOR-MIN_VALOR));
  }
  for(i=0;i<NT;i++){
     printf("Senal[%d] = %f\n",i, sen[i] );
  }  

  suavizarDatos(sen);

 
  return 0;
 }