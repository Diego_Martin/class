#include <stdio.h>

int mezcla (double a1[], double a2[], int m, int n, double a3[]);

int main(int argc, char const *argv[])
{
	// Esto esta hecho para poder comprobar si funciona
	double a1[] = {1.0, 3.0, 2.0};
	double a2[] = {4.0, 6.0, 5.0};
	double a3[6];
	// Aqui se llama a la función mezcla
	printf ("Suma: %i\n", mezcla (a1, a2, 3, 3, a3));
	return 0;
}

int mezcla (double a1[], double a2[], int m, int n, double a3[]) {
	int k, i, j;
	double temp;
	// muestro el contenido del array a1[] hasta la posición 'm' y añado los valores a a3[]
	printf("\ta1: "); // printf prescindible
	for (k = 0; k < m; k++) {
		a3[k] = a1[k];
		printf("%lf, ", a3[k]); // printf prescindible
	}
	printf("\n\n"); // printf prescindible

	// muestro el contenido del array a2[] hasta la posición 'n' y añado los valores a a3[]
	printf("\ta2: "); // printf prescindible
	for (; k < (m + n); k++) {
		a3[k] = a2[k - m];
		printf("%lf, ", a3[k]); // printf prescindible
	}
	printf("\n\n"); // printf prescindible

	// Muestro el contenido de a3[] (Prescindible)
	printf("\ta3 (before): ");
	for (int i = 0; i < (m + n); i++)
		printf("%lf, ", a3[i]);
	printf("\n\n");

	// Ordeno el array a3[] con el algoritmo burbuja, consiste en subir de posición aquellos valores que sean mayores que el siguiente
	for (i = 1; i < m + n; i++) // (m + n) es porque la longitud de a3[] es (m + n)
		for (j = 0; j < m + n - 1; j++)
			// Si un valor es mayor que su siguiente valor
			if (a3[j] > a3[j + 1]) {
				// Se intercambian sus posiciones con una variable temporal
				temp = a3[j];
				a3[j] = a3[j + 1];
				a3[j + 1] = temp;
			}
	
	// Muestro el contenido de a3[] ordenado (Prescindible)
	printf("\ta3  (after): ");
	for (i = 0; i < (m + n); i++)
		printf("%lf, ", a3[i]);
	printf("\n\n");

	return m + n;
}