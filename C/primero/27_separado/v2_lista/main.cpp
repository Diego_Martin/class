#include <stdlib.h>
#include <stdio.h>

#include "general.h"
#include "interfaz.h"

const char *nombre_programa = NULL;

void liberar (struct TEmpleado *l) {
	if (!l)
		return;

	if (l->siguiente)
		liberar (l->siguiente);

	free (l);
}

void altas (struct TEmpleado **inicio) {
	struct TEmpleado *ultimo = *inicio;
	struct TEmpleado *nuevo = (struct TEmpleado *) malloc (sizeof (struct TEmpleado));

	datos_empleado (nuevo);
	nuevo->siguiente = NULL;

	if (!ultimo) {
		*inicio = nuevo;
		return;
	}

	while (ultimo->siguiente != NULL)
		ultimo = ultimo->siguiente;

	ultimo->siguiente = nuevo;

}

void mostrarTodos (struct TEmpleado *primero) {
	if (!primero)
		return;

	if (primero->siguiente)
		mostrarTodos (primero->siguiente);

	printf ("\nNombre: %s\n", primero->nombre);
	printf ("Apellidos: %s\n", primero->apellidos);
	printf ("Salario: %lf\n", primero->salario);
}

void mostrarLista (struct TEmpleado **lista) {
	mostrarTodos (*lista);
	fflush (stdin);
	getchar ();
	getchar ();
	// mostrarUno (*lista);
}

void salir (struct TEmpleado **inicio) {
	liberar (*inicio);
	exit (0);
}

void (*fn[]) (struct TEmpleado **inicio) = { &altas, &mostrarLista, &salir };

int main(int argc, char const *argv[])
{
	struct TEmpleado *l = NULL;
	unsigned opcion;

	nombre_programa = argv[0];

	while (true) {
		opcion = menu ();
		(*fn [opcion]) (&l);
	}

	liberar (l);

	return 0;
}