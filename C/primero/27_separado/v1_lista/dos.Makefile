TARGET=lista
DEPS=interfaz.o main.o

$(TARGET): $(DEPS)
	g++ $^ -o $@

%.o: interfaz.cpp
	g++ -c interfaz.cpp

main.o: main.cpp
	g++ -c main.cpp
