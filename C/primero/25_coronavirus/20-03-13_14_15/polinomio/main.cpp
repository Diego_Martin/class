#include<stdio.h>
#include<stdlib.h>
//#include<string.h>
//#include<stdint.h>
//#include<ctypes.h>
//#include<math.h>
//#include<iostream>

/*  Constantes  */
//#define CONST value
//#define PI 3.14
//#define DIM valor
//#define N valor


void title () {
	system ("clear");
	system ("toilet -fpagga --gay Polinomio");
	printf ("\n\n");
}

void printArray (double *dato, int dim, const char *nombre) {
	int i = 0;
	printf ("\n\n\tdouble *%s = {", nombre);
	for (; i < dim; i++)
		printf ("%lf, ", dato[i]);
	printf ("%lf};\n\n", dato[i++]);
}

double *alReves (double *dato = NULL, int dim = 0) {
	if (dato == NULL) exit (EXIT_FAILURE);
	double *res = NULL;
	for (int i = 0; i <= dim; i++) {
		res = (double *) realloc (res, (i + 1) * sizeof (double));
		res[i] = dato[dim - i];
	}
	return res;
}

double resuelve (double *dato, int dim, double x) {
	double res = 0.0;
	double exp = 1;
	for (int i = 0; i <= dim; i++, exp *= x)
		res += dato[i] * exp;

	return res;
}

int main (int argc, char *argv[]) {
	/* Declaracion de variables */
	double *polinomiouser = NULL, *polinomio = NULL;
	double buffer, incognita, resultado = 0;
	int len = 0;
	char fin[2];
	
	title ();
	/* Entrada de datos */
	printf ("Introduce los coeficientes principales de tu polinomio ej(\"3x^2 - 2x + 1\" => \"(3 -2 1)\"): ");
	scanf (" %*[(]");
	do {
		polinomiouser = (double *) realloc (polinomiouser, (len + 1) * sizeof (double));
		scanf (" %lf", &buffer);
		polinomiouser[len++] = buffer;	
	} while (!scanf (" %1[)]", fin));

	printf ("Indicame el valor de la 'x' en el polinomio: ");
	scanf (" %lf", &incognita);
	len--;

	/* Manejo de datos */
	polinomio = alReves (polinomiouser, len);
	printArray (polinomiouser, len, "polinomiouser");
	printArray (polinomio, len, "polinomio");
	resultado = resuelve (polinomio, len, incognita);
	
	/* Salida de datos */
	printf ("\tEl resultado es: %lf.\n\n", resultado);

	free (polinomiouser);
	free (polinomio);
	return EXIT_SUCCESS;
}
