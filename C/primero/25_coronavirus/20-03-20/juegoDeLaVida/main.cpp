#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <ncurses.h>
#include <time.h>
#include <unistd.h>
using namespace std;

#define MARGENX 40
#define MARGENY 13
#define POBLACION 0.15
#define TIME 60

// El valor de las celdas no es 1 o 0, es el número de vecinos que tiene

int filas, cols;

void rellenaAleat (int poblacion, int *celda) {
	// Inicializo rand ()
	srand (time (NULL));

	for (int i = 0; i < poblacion;) {
		int f = rand () % filas;
		int c = rand () % cols;
		if (*(celda + f * cols + c) == 0) {// Si la celda no está ocupada, la ocupa, para que la población sea exactamente el 15%
			*(celda + f * cols + c) = 1;
			i++;
		}
	}
}

int celdasAlrededor (int *celdas, int f, int c) {
	if (f < 0 || f >= filas || c < 0 || c >= cols)
		return 0;

	int res = 0;
	for (int y = -1; y <= 1; y++)
		for (int x = -1; x <= 1; x++)
			res += *(celdas + (f * cols) + y + c + x);
	res--;// Quitamos el valor de la celda en concreto
	return res;
}

int main (int argc, char *argv[]) {
	// Inicion la ventana
	initscr ();
	// Inicio los colores
	start_color ();
	// Fijo los colores que quiero en 1 y 2
	init_pair (1, COLOR_WHITE, COLOR_BLACK);
	init_pair (2, COLOR_BLACK, COLOR_WHITE);
	// Tiempo transcurrido
	int time = 0;
	int maxX, maxY, poblacion;// En estas variables se almacena el ancho y alto de la pantalla y la poblacion en número del celdas activas
	int *celda;// Array con 0's o 1's que será si está activa la celda o no

	// Obtengo el tamaño de la ventana
	getmaxyx (stdscr, maxY, maxX);

	// Obtengo el tamaño de mi tablero y la poblacion
	filas = maxY - MARGENY * 2 - 2;
	cols  = maxX - MARGENX * 2 - 2;
	poblacion = POBLACION * filas * cols;

	// Reservo memoria que tendrá el tablero y lo relleno con celdas aleatorias
	celda = (int *) malloc (filas * cols * sizeof (int));
	// Lleno celda con 0's y lo relleno con 1's en posiciones aleatorias
	bzero (celda, filas * cols * sizeof (int));
	rellenaAleat (poblacion, celda);

	do {
		// Pinto los límites del tablero
		for (int y = 0; y < maxY; y++)
			for (int x = 0; x < maxX; x++)
				if ((y == MARGENY - 1 && x > MARGENX - 1 && x <= maxX - MARGENX) ||
				    (x == MARGENX && y > MARGENY - 1 && y <= maxY - MARGENY + 1) ||
				    (y == maxY - MARGENY + 1 && x > MARGENX - 1 && x <= maxX - MARGENX) ||
				    (x == maxX - MARGENX && y > MARGENY - 1 && y <= maxY - MARGENY + 1)) {
					attron (COLOR_PAIR(2));
					mvprintw (y, x, " ");
				}
		
		// Termino los limites
		// Pinto las celdas
		for (int y = 0; y < filas; y++)
			for (int x = 0; x < cols; x++)
				if (*(celda + y * cols + x) == 0) {
					// En negro las celdas muertas
					attron (COLOR_PAIR(1));
					mvprintw ((y + MARGENY + 1), (x + MARGENX + 1), " ");
				} else {
					// En blanco las celdas vivas
					attron (COLOR_PAIR(2));
					mvprintw ((y + MARGENY + 1), (x + MARGENX + 1), " ");
				}
		// Termino las celdas

		// Funcionamiento
		for (int y = 0; y < filas; y++)
			for (int x = 0; x < cols; x++) {
				int celdasEnderredor = celdasAlrededor(celda, x, y);
				if (celdasEnderredor < 2 || celdasEnderredor > 3)
					*(celda + y * cols + x) = 0;
				else
					*(celda + y * cols + x) = 1;
			}

		// Termino funcionamiento

		// pinto el titulo
		attron (COLOR_PAIR(1));
		mvprintw ((MARGENY - 2), (MARGENX + 2), "El juego de la vida de Conway");
		// Pinto el tiempo
		mvprintw ((maxY - MARGENY + 3), (MARGENX + 3), "Tiempo: %is", time);
		// Termino el tiempo
		//mvprintw (0,0, "filas: %i, columnas: %i", filas, cols);
		refresh ();
		usleep (1000000);
		time++;
	} while (TIME - time != 0);
	endwin ();
	// Libero el espacio de celda
	free (celda);
	return EXIT_SUCCESS;
}
