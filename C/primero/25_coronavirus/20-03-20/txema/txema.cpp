#include<stdio.h>
#include<stdlib.h>
#include<sys/ioctl.h>
#include<strings.h>
#include<time.h>
#include<unistd.h>

#define TIEMPO 45
#define POBL_INI 0.20

#define ESPACIO_TITULO 8
#define MARGEN 15

int filas, cols;

void title () {
	system ("clear");
	system ("toilet -fpagga --gay '   Juego de la Vida   '");
	printf ("\n\n");
}

int valor (int *celda, int f, int c) {
	if (f < 0 || f > filas || c < 0 || c > cols)
		return 0;

	return *(celda + f * cols + c);
}

int vecinos (int *celda, int f, int c) {
	int res = 0;

	for (int filal = -1; filal < 2; filal++)
		for (int colal = -1; colal < 2; colal++)
			res += valor (celda, f + filal, c + colal);

	res -= valor (celda, f, c);
	return res;
}

void computa (int *fuente, int *albaran){
	for (int f = 0; f < filas; f++)
		for (int c = 0; c < cols; c++)
			*(albaran + f * cols + c) = vecinos (fuente, f, c);
}

void pinta (int *celda, int *comp) {
	title ();

	for (int f = 0; f < MARGEN - ESPACIO_TITULO; f++)
		printf ("\n");

	for (int f = 0; f < filas; f++) {
		for (int c = 0; c < MARGEN; c++)
			printf (" ");
		for (int c = 0; c < cols; c++)
			if ( *(celda + f * cols + c) ) {
				printf ("\x1B[7m");
				printf (" ");
				printf ("\x1B[0m");
			} else {
				printf (" ");
			}
		printf ("\n");

	}
}

void actualiza (int *celula, int *computo) {
	for (int f = 0; f < filas; f++)
		for (int c = 0; c < cols; c++) {
			int vecinos = *(computo + f * cols + c);
			if (vecinos != 2 && vecinos != 3)
				*(celula + f * cols + c) = 0;
			else if (vecinos = 3)
				*(celula + f * cols + c) = 1;
		}
}

void rellena_aleatorio (int p, int *cel) {
	srand ( time (NULL) );

	for (int i = 0; i < p;) {
		int fi = rand () % filas;
		int co = rand () % cols;
		if (*(cel + fi * cols + co) == 0) {
			*(cel + fi * cols + co) = 1;
			i++;
		}
	}
}

int main (int argc, char *argv[]) {
	struct winsize w;
	int *celula, *computo;
	int poblacion_ini;
	time_t inicio, actual;

	/* Init */
	ioctl (0, TIOCGWINSZ, &w);
	filas = w.ws_row - 2 * MARGEN;
	cols  = w.ws_col - 2 * MARGEN;
	poblacion_ini = POBL_INI * filas * cols;
	inicio = time (NULL);

	/* Reserva de memoria */
	celula  = (int *) malloc (filas * cols * sizeof (int));
	computo = (int *) malloc (filas * cols * sizeof (int));
	bzero (celula,  filas * cols * sizeof (int));
	bzero (computo, filas * cols * sizeof (int));
	rellena_aleatorio (poblacion_ini, celula);
	computa (celula, computo);

	/* Bucle main */
	do {
		actual = time (NULL);
		pinta (celula, computo);
		printf ("\n\n\tTiempo restante: %lis\n", TIEMPO + inicio - actual);
		computa (celula, computo);
		actualiza (celula, computo);
		usleep (100000);
	} while (actual - inicio < TIEMPO);	

	system ("clear");
	free (computo);
	free (celula);
	return EXIT_SUCCESS;
}
