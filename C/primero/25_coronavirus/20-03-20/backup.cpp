#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <time.h>
#include <unistd.h>
using namespace std;

#define MARGENX 40
#define MARGENY 13
#define POBLACION 0.15
#define TIME 10

int filas, columnas;

int main (int argc, char *argv[]) {

	initscr ();
	start_color ();
	init_pair (1, COLOR_WHITE, COLOR_BLACK);
	int time = 0;
	int maxX, maxY;
	getmaxyx (stdscr, maxY, maxX);
	do {
		// Pinto los límites
		for (int y = 0; y < maxY; y++)
			for (int x = 0; x < maxX; x++)
				if (y == MARGENY - 1 || x == MARGENX - 1 || y == MARGENY + 1 || x == MARGENX + 1) {
					attron (COLOR_PAIR(1));
					mvprintw (y, x, " ");
				}
		
		// Termino los limites
		refresh ();
		usleep (1000000);
		time++;
	} while (TIME - time != 0);
	endwin ();

	printf ("\n\n");
//	free (filas);
//	free (columnas);
	return EXIT_SUCCESS;
}
