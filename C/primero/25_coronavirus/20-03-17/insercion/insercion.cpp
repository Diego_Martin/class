#include <stdio.h>
#include <stdlib.h>

#define N 8

void printArray (int arr[N]) {
	for (int i = 0; i < N; i++)
		printf("%i ", arr[i]);
	printf("\n");
}

int main(int argc, char *argv[]) {
	/* code */
	int miArray[N] = {1, 5, 2, 8, 4, 9, 2, 5};
	int menor;

	printArray (miArray);

	for (int buscando = 0; buscando < N - 1; buscando++)
	{
		menor = buscando;
		for (int i = buscando + 1; i < N; i++)
			if (miArray[i] < miArray[menor])
				menor = i;
		if (menor > buscando) // Se intercambian posiciones
			(miArray[buscando] ^= miArray[menor]), (miArray[menor] ^= miArray[buscando]), (miArray[buscando] ^= miArray[menor]);
	}

	printArray (miArray);

	return 0;
}