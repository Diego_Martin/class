#include <stdio.h>
#include <stdlib.h>

#define N 8

void printArray (int arr[N]) {
	for (int i = 0; i < N; i++)
		printf("%i ", arr[i]);
	printf("\n");
}

int main (int argc, char *argv[]) {
	/* Datos */
	int miArray[N] = {1, 5, 2, 8, 4, 9, 2, 5};
	// int *miOtroArray[N];

	/*Operaciones*/
	printArray (miArray);
	for (int j = 0; j < N - 1; j++)
		for (int i = 0; i < N - j - 1; i++) // Sin el - j de aqui va mas lento pero hace menos repeticiones
			if (miArray[i] < miArray[i + 1]) {
				/* Le damos la vuelta */
				int temp = miArray[i + 1];
				miArray[i + 1] = miArray[i];
				miArray[i] = temp;
			}
	printArray (miArray);
	return EXIT_SUCCESS;
}