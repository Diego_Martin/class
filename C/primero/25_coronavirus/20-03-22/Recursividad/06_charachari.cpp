#include <stdio.h>
#include <stdlib.h>

void titulo () {
	system ("clear");
	system ("toilet -fpagga --gay -F border Cadena al reves");
	printf ("\n\n");
}

void daLaVuelta (const char *frase, int i = 0) {
	
	if (*(frase + i) != '\0')
		daLaVuelta (frase, i + 1);
	
	printf("%c", *(frase + i));
}

int main(int argc, char const *argv[])
{
	const char *frase = "Yo hago yoga hoy";

	titulo ();
	daLaVuelta (frase);

	printf("\n\n");
	return EXIT_SUCCESS;
}