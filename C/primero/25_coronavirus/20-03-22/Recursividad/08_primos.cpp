#include <stdio.h>
#include <stdlib.h>

void titulo () {
	system ("clear");
	system ("toilet -fpagga --gay -F border Numeros primos");
	printf ("\n\n");
}

int numOfDivisores (int num, int div = 1, int total = 0) {
	if ((num % div) == 0) {
		total++;
		return total += numOfDivisores (num, (div + 1), total);
	} else
		return total;
}

bool esPrimo (int num) {
	return (bool) (!(numOfDivisores (num) - 2));
}

int main(int argc, char const *argv[])
{
	int num;

	titulo ();
	printf("Que numero quieres saber si es primo?: ");
	scanf (" %i", &num);
	printf("Divisores: %i\n", numOfDivisores(num) - 2);
	printf("Es primo?: %s\n", (esPrimo(num)) ? "Si" : "No");
	return 0;
}