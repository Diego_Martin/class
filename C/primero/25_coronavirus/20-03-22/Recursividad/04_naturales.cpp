#include <stdio.h>
#include <stdlib.h>

const char *comando;

void titulo () {
	system ("clear");
	system ("toilet -fpagga --gay -F border Numeros naturales");
	printf ("\n\n");
}

void print_uso (FILE *output) {
	fprintf(output, "Uso: %s <Numero>\nSuma todos los número desde 'Numero' hasta 0. Siendo 'Numero' positivo.\n\n", comando);
	exit (EXIT_FAILURE); // Salimos con error
}

void naturales (int nume) {
	if (nume != 0) {
		naturales (nume - 1);
		printf("%i ", nume);
	}
}

int main(int argc, char const *argv[])
{
	comando = argv[0];                                                     // Nombre del ejecutable
	int num;

	titulo ();
	if (argc < 2 || argc > 2)                                              // Si no hay parámetro o hay más de uno
		print_uso (stderr);			                                       // Se cierra con un error

	if (!(num = atoi(argv[1])))                                            // Pasa el número a entero
	    print_uso (stderr);                                                // y si hay algún error, cierra el programa

	if (num > 0)                                                           // Si el número es mayor que 0
		naturales (num);                                                   // Se ejecuta
	else                                                                   // Si no
		print_uso (stderr);                                                // Se cierra con un error

	printf ("\n");
	return EXIT_SUCCESS;
}