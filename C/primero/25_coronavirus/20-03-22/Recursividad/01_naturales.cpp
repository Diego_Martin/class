#include <stdio.h>
#include <stdlib.h>

const char *comando;

void titulo () {
	system ("clear");
	system ("toilet -fpagga --gay -F border 'Numeros naturales'");
	printf ("\n\n");
}

void print_uso (FILE *output) {
	fprintf(output, "Uso: %s <Numero>\nSuma todos los número desde 0 hasta 'Numero'. Siendo 'Numero' positivo.\n\n", comando);
	exit (EXIT_FAILURE); // Salimos con error
}

int naturales (int nume, int suma = 0) {
	if (nume != 0) {
		return naturales (nume - 1, suma += nume);
		printf("%i ", suma);
	} else {
		return suma;
	}
}

int main(int argc, char *argv[])
{
	comando = argv[0];                          // Nombre del ejecutable
	int num;

	titulo ();
	if (argc < 2 || argc > 2)                   // Si no hay parámetro o hay más de uno
		print_uso (stderr);			            // Se cierra con un error

	if (!(num = atoi(argv[1])))                 // Pasa el número a entero
	        print_uso (stderr);                 // y si hay algún error, cierra el programa

	if (num > 0)
		printf ("Suma: %i", naturales (num));
	else
		print_uso (stderr);

	printf ("\n");
	return EXIT_SUCCESS;
}
