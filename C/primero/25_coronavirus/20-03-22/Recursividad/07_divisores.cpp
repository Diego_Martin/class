#include <stdio.h>
#include <stdlib.h>

void titulo () {
	system ("clear");
	system ("toilet -fpagga --gay -F border 'Divisores'");
	printf ("\n\n");
}

void divisores (unsigned num, int div = 1) {
	if (num % div == 0)
		printf("%i ", div);
	if (num != (div - 1))
		divisores (num, div + 1);
}

int main(int argc, char const *argv[])
{
	unsigned num;

	titulo ();
	printf("De que numero quieres saber los divisores?: ");
	scanf (" %u", &num);

	divisores (num);

	printf("\n");
	return 0;
}