#include <stdio.h>
#include <stdlib.h>

void title () {
	system ("clear");
	system ("toilet -fpagga --gay -F border Ecuacion");
	printf("\n\n");
}

int factorial (int num) {
	int res = 1;
	for (int i = 0; i < num; i++)
		res *= i;

	return res;
}

double resolver (int num) {
	printf("%i\n", num);
	if (num > 0)
		return (double)(1 / factorial(num)) + resolver(num - 1);
	else
		return (double) 1;
}

int main(int argc, char const *argv[])
{
	// Variables
	int num = 0;
	double res;

	title ();

	// Entrada de datos
	printf("Tengo la siguiente ecuación: \n");
	printf("\n\tf(1) = \033[92;40m1/1! + 1/0!\033[37;40m = \033[93;40m1/1! + f(0)\033[37;40m = 2\n\n");
	printf("Teniendo f(x), indicame el valor de 'x' para calcular la función: ");
	scanf (" %i", &num);

	// Manejo de datos
	res = resolver (num);
	return 0;
}