#include <stdio.h>
#include <stdlib.h>

const char *comando;
const char *nombreOrdinal[4][10] = {
    { "\b", "primero", "segundo", "tercero", "cuarto", "quinto", "sexto", "séptimo", "octavo", "noveno" },
    { "\b", "décimo", "vigésimo", "trigésimo", "cuadragésimo", "quincuagésimo", "sexagésimo", "septuagésimo", "octogésimo", "nonagésimo" },
    { "\b", "centésimo", "bicentésimo", "tricentésimo", "cuadrigentésimo", "quingentésimo", "sexgentésino", "septingentésimo", "octingentésimo", "noningentésimo" },
    { "\b", "milésimo", "dosmilésimo", "tresmilésimo", "cuatromilésimo", "cincomilésimo", "seismilésimo", "sietemilésimo", "ochomilésimo", "nuevemilésimo" }
};

void titulo () {
	system ("clear");
	system ("toilet -fpagga --gay -F border Numeros ordinales");
	printf ("\n\n");
}

void print_uso (FILE *output) {
	fprintf(output, "Uso: %s <Numero>\nConvierte 'Numero', a notacion ordinal.\nSiendo 'Numero':\n\t- Positivo.\n\t- Entero.\n\t- Máximo 4 cifras.\n\n", comando);
	exit (EXIT_FAILURE); // Salimos con error
}

void ordinal (int num, int niv = 0) {
	if (num != 0) {
		ordinal (num / 10, niv + 1);
		printf("%s ", nombreOrdinal[niv][num % 10]);
	}
}

int main(int argc, char const *argv[])
{
	comando = argv[0];                                                     // Nombre del ejecutable
	int num;                                                               // Variable que contendrá el número a pasar a ordinal

	titulo ();
	if (argc < 2 || argc > 2)                                              // Si no hay parámetro o hay más de uno
		print_uso (stderr);							                       // Se cierra con un error

	if (!(num = atoi(argv[1])))                                            // Pasa el número a entero
        print_uso (stderr);                                                // y si hay algún error, cierra el programa
    // printf("%i\n", num);
	if (num > 0)                                                           // Si el número es mayor que 0
		if (num < 10000)                                                   // Si el número es menor que 10000
			ordinal (num);                                                 // Imprime el ordinal
		else
			printf("El número debe tener un máximo de 4 cifras, has escrito el %i\n", num); // Te lo comunica
	else                                                                   // Si no lo es
		printf("El número debe ser positivo, has escrito el %i\n", num);   // Te comunica el error
	printf("\n");
	return EXIT_SUCCESS;
}