#include <stdio.h>
#include <stdlib.h>

const char *comando;

void titulo () {
	system ("clear");
	system ("toilet -fpagga --gay -F border Factorial");
	printf ("\n\n");
}

void print_uso (FILE *output) {
	fprintf(output, "Uso: %s <Numero>\nHace el factorial de 'Numero'. Siendo 'Numero' positivo.\n\n", comando);
	exit (EXIT_FAILURE); // Salimos con error
}

int factorial (int nume, int fact = 1) {
	if (nume != 0) {
		return factorial (nume - 1, fact *= nume);
		//printf("%i ", fact);
	} else {
		return fact;
	}
}

int main(int argc, char *argv[])
{
	comando = argv[0];                                                     // Nombre del ejecutable
	int num;

	titulo ();
	if (argc < 2 || argc > 2)                                              // Si no hay parámetro o hay más de uno
		print_uso (stderr);			                                       // Se cierra con un error

	if (!(num = atoi(argv[1])))                                            // Pasa el número a entero
	        print_uso (stderr);                                            // y si hay algún error, cierra el programa

	if (num > 0)
		printf ("Factorial: %i", factorial (num));
	else
		print_uso (stderr);

	printf ("\n");
	return EXIT_SUCCESS;
}