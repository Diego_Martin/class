#include <stdio.h>
#include <stdlib.h>

void titulo () {
	system ("clear");
	system ("toilet -fpagga --gay -F border Cadena de caracteres");
	printf ("\n\n");
}

void daLaVuelta (const char *frase, int i = 0) {
	
	printf("%c", *(frase + i));

	if (*(frase + i) != '\0')
		daLaVuelta (frase, i + 1);
}

int main(int argc, char const *argv[])
{
	const char *frase = "Yo hago yoga hoy";

	titulo ();

	daLaVuelta (frase);

	printf("\n\n");
	return EXIT_SUCCESS;
}