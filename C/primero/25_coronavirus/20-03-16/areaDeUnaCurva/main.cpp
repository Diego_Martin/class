#include<stdio.h>
#include<stdlib.h>
//#include<string.h>
//#include<stdint.h>
//#include<ctypes.h>
//#include<math.h>
//#include<iostream>

/*  Constantes  */
//#define CONST value
//#define PI 3.14
//#define DIM valor
//#define N valor


void title () {
	system ("clear");
	system ("toilet -fpagga --gay Integrales");
	printf ("\n\n");
}

void printOp (double salto, double li, double ls, double anchura) {
	double i = li, exp = 0, saltoi = salto;
	printf ("\n\n\tArea = ");

	for (; i <= ls - saltoi; i += salto, exp = i * i)
		printf ("%.2lf * %.2lf + ", anchura, exp);

	printf ("%.2lf * %.2lf", anchura, exp);
	printf ("\n\n\tArea = (");
	i = li;
	exp = 0;
	for (; i <= ls - saltoi; i += salto, exp = i * i)
		printf ("%.2lf + ", exp);

	printf ("%.2lf) * %.2lf", exp, anchura);
}

double resuelve (double salto, double li, double ls, double anchura) {
	double res = 0, saltoi = salto, exp = 0;
	for (int i = li; i <= ls; i += salto, exp = i * i)
		res += exp;
	res *= anchura;
	return res;
}

int main (int argc, char *argv[]) {
	/* Declaracion de variables */
	double salto, ls, li, anchura, res;

	title ();
	printf ("Teniendo la función  \"y = x ^ 2\", me indicarás el valor del salto, la anchura y la \"y\" maxima y minima, y calcularé los puntos entre las \"y's\" teniendo en cuenta el salto.\n\n");

	/* Entrada de datos */
	printf ("Valor del salto: ");
	scanf (" %lf", &salto);

	printf ("Valor de la anchura: ");
	scanf (" %lf", &anchura);

	printf ("Valor de la \"x\" inferior: ");
	scanf (" %lf", &li);

	printf ("Valor de la \"x\" superior: ");
	scanf (" %lf", &ls);

	/* Manejo de datos */
	printOp (salto, li, ls, anchura);
	res = resuelve (salto, li, ls, anchura);

	/* Salida de datos */
	printf ("\n\n\t\tResultado: %.2lf", res);
	printf ("\n\n");
	return EXIT_SUCCESS;
}
