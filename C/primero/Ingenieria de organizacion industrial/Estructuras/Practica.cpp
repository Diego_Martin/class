#include <stdio.h>
#include <string.h> // Trabajar con strings

struct TReferencia {
	unsigned identificador;
	char titulo[80];
	char autor[40];
	char editorial[50];
	int anio;
	unsigned copias;
};

struct TCatalogo {
	char clave;
	char nombre[50];
	char descripcion[100];
	unsigned cuentaReferencias;
	struct TReferencia referencia[10];
};

struct TBiblioteca {
	unsigned cuenta;
	struct TCatalogo catalogo[5];
};

// variable
struct TBiblioteca Biblioteca;
// funciones
struct TCatalogo newCatalogo (char clave, char nombre[50], char descripcion[100], unsigned cuentaReferencias);
struct TReferencia newReferencia (unsigned id, char titulo[80], char autor[40], char editorial[50], int anio, unsigned copias);
unsigned menu (); // Menu + eleccion

int main(int argc, char const *argv[])
{
	unsigned opcion;
	// Nombres iniciales (Se podrian borrar despues de usarlos)
	char nombresCatalogos[3][50] = {"Articulos", "Libros", "Trabajos de fin de grado"};
	char descripcionesCatalogos[3][100] = {"Aqui van los articulos.", "Aqui van los libros.", "Aqui van los TFGs."};
	char titulosReferencias[9][80] = {"Titulo del articulo 1", "Titulo del articulo 2", "Titulo del articulo 3", "Titulo del libro 1", "Titulo del libro 2", "Titulo del libro 3", "Titulo del TFG 1", "Titulo del TFG 2", "Titulo del TFG 3"};
	char autoresReferencias[9][40] = {"Diego N", "Diego M", "JaviGar", "Diego N", "Diego M", "JaviGar", "Diego N", "Diego M", "JaviGar"};
	char editorialesReferencias[9][50] = {"EdBase", "EdBase", "EdBase", "EdBase", "EdBase", "EdBase", "EdBase", "EdBase", "EdBase"};
	// Creo los catalogos
	Biblioteca.catalogo[0] = newCatalogo ( 'A', nombresCatalogos[0], descripcionesCatalogos[0], 0 );
	Biblioteca.catalogo[1] = newCatalogo ( 'L', nombresCatalogos[1], descripcionesCatalogos[1], 0 );
	Biblioteca.catalogo[2] = newCatalogo ( 'T', nombresCatalogos[2], descripcionesCatalogos[2], 0 );
	Biblioteca.cuenta = 3;

	// Creo las referencias
	Biblioteca.catalogo[0].referencia[0] = newReferencia ( 1, titulosReferencias[0], autoresReferencias[0], editorialesReferencias[0], 2020, 45 );
	Biblioteca.catalogo[0].referencia[1] = newReferencia ( 2, titulosReferencias[1], autoresReferencias[1], editorialesReferencias[1], 2019, 54 );
	Biblioteca.catalogo[0].referencia[2] = newReferencia ( 3, titulosReferencias[2], autoresReferencias[2], editorialesReferencias[2], 2018, 73 );
	Biblioteca.catalogo[0].cuentaReferencias = 3;

	Biblioteca.catalogo[1].referencia[0] = newReferencia ( 1, titulosReferencias[3], autoresReferencias[3], editorialesReferencias[3], 2020, 45 );
	Biblioteca.catalogo[1].referencia[1] = newReferencia ( 2, titulosReferencias[4], autoresReferencias[4], editorialesReferencias[4], 2019, 54 );
	Biblioteca.catalogo[1].referencia[2] = newReferencia ( 3, titulosReferencias[5], autoresReferencias[5], editorialesReferencias[5], 2018, 73 );
	Biblioteca.catalogo[1].cuentaReferencias = 3;

	Biblioteca.catalogo[2].referencia[0] = newReferencia ( 1, titulosReferencias[6], autoresReferencias[6], editorialesReferencias[6], 2020, 45 );
	Biblioteca.catalogo[2].referencia[1] = newReferencia ( 2, titulosReferencias[7], autoresReferencias[7], editorialesReferencias[7], 2019, 54 );
	Biblioteca.catalogo[2].referencia[2] = newReferencia ( 3, titulosReferencias[8], autoresReferencias[8], editorialesReferencias[8], 2018, 73 );
	Biblioteca.catalogo[2].cuentaReferencias = 3;

	// Menu
	char clave, barraIdentificador;
	char nombre[50];
	char descripcion[100];
	char titulo[80];
	char autor[40];
	char editorial[50];
	char identificador[3];
	int anio;
	unsigned copias, indice, numeroIdentificador;
	do {
		opcion = menu ();
		switch (opcion) {
			case 1:
				printf("                 AÑADIR CATALOGO\n\n");
				printf("Clave: ");
				fflush(stdin);
				fgets (&clave, 1, stdin);
				printf("Nombre: ");
				fflush(stdin);
				fgets (nombre, 50, stdin);
				printf("Descripcion: ");
				fflush(stdin);
				fgets (descripcion, 100, stdin);
				Biblioteca.catalogo[Biblioteca.cuenta] = newCatalogo (clave, nombre, descripcion, 0);
				Biblioteca.cuenta++; // Suma 1
				break;
			case 2:
				printf("               NUEVA REFERENCIA BIBLIOGRAFICA\n\n");
				printf("En que catalogo quieres añadir la referencia bibliografica? (");
				for (int i = 0; i < Biblioteca.cuenta; i++)
					printf("%c, ", Biblioteca.catalogo[i].clave);

				printf("\b\b): "); // Borro la ultima coma y el ultimo espacio y cierro el parentesis abierto en la linea 87
				fflush (stdin);
				fgets (&clave, 1, stdin);
				// clase = toupper (clave);
				// Coje el indice del array Biblioteca.catalogo con la clave que esta almacenada en clave
				for (indice = 0; Biblioteca.catalogo[indice].clave != clave; indice++);

				printf("Titulo: ");
				fflush (stdin);
				fgets (titulo, 80, stdin);
				printf("Editorial: ");
				fflush (stdin);
				fgets (editorial, 50, stdin);
				printf("Autor: ");
				fflush (stdin);
				fgets (autor, 40, stdin);
				printf("A%co: ", 164);
				fflush (stdin);
				fgets (&anio, 4, stdin); // Ocupa 4 bytes porque 'int' ocupa 4 bytes
				printf("Número de copias: ");
				fflush (stdin);
				fgets (&copias, 4, stdin);

				Biblioteca.catalogo[indice].cuentaReferencias++;
				Biblioteca.catalogo[indice].referencia[Biblioteca.catalogo[indice].cuentaReferencias] = newReferencia (Biblioteca.catalogo[indice].cuentaReferencias, titulo, autor, editorial, anio, copias);
				break;
			case 3:
				printf("               ELIMINAR COPIA DE REFERENCIA BIBLIOGRAFICA\n\n");
				printf("Introduzca el identificador: ");
				fflush (stdin);
				fgets (identificador, 3, stdin);
				clave = identificador[0];
				barraIdentificador = identificador[1];
				numeroIdentificador = (int)(identificador[2] - '0');

				// Saca el indice del catalogo por su letra
				for (indice = 0; Biblioteca.catalogo[indice].clave != clave; indice++);

				Biblioteca.catalogo[indice].referencia[numeroIdentificador].copias--;

				printf("Se ha eliminado una copia de la referencia indicada.\n");
				if (Biblioteca.catalogo[indice].referencia[numeroIdentificador].copias == 0) {
					printf("Numero de copias restantes: 0.\n");
					printf("Alerta: ¡SE HA REALIZADO LA ELIMINACION DEFINITIVA!\n");
					// Desplazo los que quedan detras del eliminado a una posición antes en el array Biblioteca.catalogo
					for (int i = indice; i < Biblioteca.catalogo[indice].cuentaReferencias; i++)
						Biblioteca.catalogo[indice] = Biblioteca.catalogo[indice + 1];

				} else if (Biblioteca.catalogo[indice].referencia[numeroIdentificador].copias < 3) {
					printf("Numero de copias restantes: %i.\n", Biblioteca.catalogo[indice].referencia[numeroIdentificador].copias);
					printf("Alerta: ¡SE DEBEN ADQUIRIR COPIAS DE ESTA REFERENCIA!\n");
				}
				break;
			case 4:
				printf("               ELIMINAR CATALOGO\n\n");
				printf("Introduzca la clave del catalogo que desea eliminar: ");
				fflush (stdin);
				fgets (&clave, 1, stdin);

				// Saca el indice del catalogo por su letra
				for (indice = 0; Biblioteca.catalogo[indice].clave != clave; indice++);

				if (Biblioteca.catalogo[indice].cuentaReferencias > 0) {
					/* Pedir confirmacion */
					char confirmacion[2];
					printf("Seguro que quieres eliminar el catalogo con la clave '%c'? (si/no): ", clave); // Si, si, SI, sI
					fflush (stdin);
					fgets (confirmacion, 2, stdin);
					
					if (strcmp (confirmacion, "si") == 0) {
						printf("Se ha eliminado el catalogo.\n");
						for (int i = indice; i < Biblioteca.cuenta; i++)
							Biblioteca.catalogo[indice] = Biblioteca.catalogo[indice + 1];
					} else if (strcmp (confirmacion, "no") == 0) {
						printf("No se ha eliminado el catalogo.\n");
					} else {
						printf("Esa opcion no es válida.\n");
						printf("No se ha eliminado el catalogo.\n");
					}
				} else {
					for (int i = indice; i < Biblioteca.cuenta; i++)
						Biblioteca.catalogo[indice] = Biblioteca.catalogo[indice + 1];
				}
				break;
			case 5:
				printf("/****** LISTADO DEL CATALOGO DE LA AGA *******/\n");
				for (int i = 0; i < Biblioteca.cuenta; i++) {
					printf("‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐ \n");
					printf("CATALOGO \"%s\", Clave \"%c\"\n", Biblioteca.catalogo[i].nombre, Biblioteca.catalogo[i].clave);
					printf("‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐ \n");
					printf("Descripcion: %s\n\n", Biblioteca.catalogo[i].descripcion);
					printf("Total de recursos bibliograficos en este catalogo: %i\n\n", Biblioteca.catalogo[i].cuentaReferencias);
					for (int j = 0; j < Biblioteca.catalogo[i].cuentaReferencias; j++) {
						printf("- Referencia bibliografica \"%c/%i\"\n", Biblioteca.catalogo[i].clave, Biblioteca.catalogo[i].referencia[j].identificador);
						printf("- Titulo: %s\n", Biblioteca.catalogo[i].referencia[j].titulo);
						printf("- Autor: %s\n", Biblioteca.catalogo[i].referencia[j].autor);
						printf("- Editorial: %s\n", Biblioteca.catalogo[i].referencia[j].editorial);
						printf("- A%co: %i\n", 164, Biblioteca.catalogo[i].referencia[j].anio);
						printf("- Numero: %i\n\n", Biblioteca.catalogo[i].referencia[j].copias);
					}
				}

				break;
		}
	} while (opcion < 6);



	return 0;
}

struct TCatalogo newCatalogo (char clave, char nombre[50], char descripcion[100], unsigned cuentaReferencias) {
	struct TCatalogo resultado;

	resultado.clave = clave;
	strcpy (resultado.nombre, nombre);
	strcpy (resultado.descripcion, descripcion);
	resultado.cuentaReferencias = cuentaReferencias;

	return resultado;
}

struct TReferencia newReferencia (unsigned id, char titulo[80], char autor[40], char editorial[50], int anio, unsigned copias) {
	struct TReferencia resultado;

	resultado.identificador = id;
	strcpy (resultado.titulo, titulo);
	strcpy (resultado.autor, autor);
	strcpy (resultado.editorial, editorial);
	resultado.anio = anio;
	resultado.copias = copias;

	return resultado;
}

unsigned menu () {
	unsigned eleccion;
	do {
		printf ("                 MENU PRINCIPAL\n");
		printf ("1.- Alta de un nuevo catalogo en el sistema.\n");
		printf ("2.- Nueva referencia bibliografica.\n");
		printf ("3.- Eliminar referencia bibliografica.\n");
		printf ("4.- Baja de un catalogo.\n");
		printf ("5.- Listado del catalogo de la AGA.\n");
		printf ("6.- Terminar.\n");
		printf ("Elija: ");
		fflush (stdin);
		fgets (&eleccion, 4, stdin);
	} while (eleccion > 7 || eleccion < 0);

	return eleccion;
}