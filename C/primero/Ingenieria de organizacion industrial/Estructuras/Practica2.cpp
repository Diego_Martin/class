#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define _MAX_cat 5
#define _MAX_ref 10

#define _TAM_nombre 51
#define _TAM_descripcion 101
#define _TAM_titulo 81
#define _TAM_autor 41
#define _TAM_editorial 51

struct referencia {
	int identificador; 
	char titulo[_TAM_titulo];
	char autor[_TAM_autor];
	char editorial[_TAM_editorial];
	int anio;
	int copias;
};
struct catalogo {
	char clave;
	char nombre[_TAM_nombre];
	char descripcion[_TAM_descripcion];
	int totalref;
	tipoReferencia ref[_MAX_ref];
};
struct biblioteca {
	tipoCatalogo cat[_MAX_cat];
	int totalcat;
};


typedef struct referencia tipoReferencia;
typedef struct catalogo tipoCatalogo;
typedef struct biblioteca tipoBiblioteca;

tipoReferencia ref[_MAX_ref];
tipoBiblioteca AGA;


int borrarcatalogo(int);
tipoCatalogo crearcatalogo(char claveC, char nombreC[_TAM_nombre], char descripcionC[_TAM_descripcion], int totalref);
tipoReferencia crearreferencia();
unsigned menu();
void mostrarcatalogo();




int main(int argc, char** argv)
{
	unsigned seleccion;
	char claveC;
	char nombreC[_TAM_nombre];
	char descripcionC[_TAM_descripcion];
	int j,i;
	char tituloR[_TAM_titulo];
	char autorR[_TAM_autor];
	char editorialR[_TAM_editorial];
	int anioR;
	int copiasR;
	char buscarclave;
	AGA.totalcat=3;

	AGA.cat[0]=crearcatalogo('A', "Articulos", "Van los articulos",0);      //INICIALIZACION DE 3 CATALOGOS
	AGA.cat[1]=crearcatalogo('L', "Libros", "Van los libros",0);
	AGA.cat[2]=crearcatalogo('T', "TFGs", "Van los trabajos",0);

	/*AGA.cat[0].ref[0]=crearreferencia()                                      //INICIALIZACION DE 3 REFERENCIAS/CATALOGO                                                           //AQUI ME HE QUEDADO
	AGA.cat[0].ref[1]=crearreferencia()
	AGA.cat[0].ref[2]=crearreferencia()

	AGA.cat[1].ref[0]=crearreferencia()
	AGA.cat[1].ref[1]=crearreferencia()
	AGA.cat[1].ref[2]=crearreferencia()

	AGA.cat[2].ref[0]=crearreferencia()
	AGA.cat[2].ref[1]=crearreferencia()
	AGA.cat[2].ref[2]=crearreferencia()

	*/


	do {
		printf("\n    Bienvenido a la biblioteca de la Academia General de Aire.\n\n");
		
		seleccion = menu();
		
		switch(seleccion) {
		case 1:
		if(AGA.totalcat == _MAX_cat) { printf("Ha excedido el numero maximo de catalogos\n\n");
				system("pause");
				system("cls");
				
			} else {            
			printf("-DAR DE ALTA UN NUEVO CATALOGO-\n");
			printf("Introduzca la siguiente informacion:\n\n");
			printf("-Clave:");
			fflush(stdin);
			scanf("%c", &claveC);
			printf("-Nombre:");
			fflush(stdin);
			scanf("%s",nombreC);
			printf("-Descripcion:");
			fflush(stdin);
			fgets(descripcionC, _TAM_descripcion, stdin);
			printf("\nCatalogo guardado con exito\n\n");
			system("pause");
			system("cls");
			AGA.cat[AGA.totalcat] = crearcatalogo(claveC, nombreC, descripcionC, 0);
			AGA.totalcat++;
			}
			break;
			
		case 2:

			printf("Introduzca clave del catalogo a eliminar: ");
			fflush(stdin);
			scanf("%c", &buscarclave);

			for(j = 0; j < AGA.totalcat; j++) {
				if(buscarclave == AGA.cat[j].clave)
					break;
			}
			if(j >= AGA.totalcat) {
				printf("\nError. El catalogo no se encuentra ingresado actualmente.\n\n");
				system("pause");
				system("cls");
			} else
				borrarcatalogo(j);

			break;
		case 3:
		
		printf("-DAR DE ALTA UNA NUEVA REFERENCIA-\n");
		printf("Introduzca la siguiente informacion:\n");
		printf("-Clave del catalogo:");
		fflush(stdin);
		scanf("%c",&buscarclave);
		for(i = 0; i < _MAX_cat; i++) {
			if(buscarclave == AGA.cat[i].clave)
				break;
		}
		printf("-Titulo:");
		fflush(stdin);
		gets(tituloR); // b etc
		printf("-Autor:");
		fflush(stdin);
		gets(autorR);
		printf("-Editorial:");
		fflush(stdin);
		gets(editorialR);
		printf("-Ano:");
		fflush(stdin);
		scanf("%d", &anioR);
		printf("-Numero de copias:");
		fflush(stdin);
		scanf("%d", &copiasR);
		system("cls");
		
			break;
		case 4:
			break;
		case 5:
		system("cls");
			printf("\t\t\t---LISTADO---\n\n");
			if(AGA.totalcat == 0) {
				printf("\t\tNo hay catalogos registrados\n\n\t   ");
				system("pause");
				system("cls");
			} else 
				mostrarcatalogo();
			
		};
	} while(seleccion != 6);
	printf("totalcat:%d\n",AGA.totalcat);
	printf("He salido\n");

	/*
	
		} else if(o == 3) { // CREACION DE REFERENCIAS
			if(AGA.cat[AGA.totalcat].totalref < _MAX_ref) {
				// cat.ref[cat.totalref] = crearreferencia();
				AGA.cat[i].totalref++;
			} else {
				printf("Ha excedido el numero maximo de referencias\n\n");
				system("pause");
				system("cls");
			}

			o = 0;

		} else if(o == 5) { // LISTADO
			system("cls");
			printf("\t\t\t---LISTADO---\n\n");
			if(AGA.totalcat == 0) {
				printf("\tAun no hay catalogos registrados\n");
			} else
				printf("\tHay %d catalogos registrados en esta biblioteca.\n\n", AGA.totalcat);
			for(i = 0; i < AGA.totalcat; i++) {
				printf("\tCatalogo:%s\n", AGA.cat[i].nombre);
				printf("\tClave:%s\n", AGA.cat[i].clave);
				printf("\tDescripcion:%s\n\n", AGA.cat[i].descripcion);
				if(AGA.cat[i].totalref == 0) {
					printf("\tAun no hay referencias registradas para este catalogo.\n\n\n\n");
				} else
					printf("\tHay %d referencias registradas para este catalogo.\n\n\n\n", AGA.cat[i].totalref);
				for(x = 0; x < AGA.cat[i].totalref; x++) {
					printf("\tTitulo:%s\n", AGA.cat[i].ref[x].titulo);
					printf("\tAutor:%s\n", AGA.cat[i].ref[x].autor);
					printf("\tEditorial:%s\n", AGA.cat[i].ref[x].editorial);
					printf("\tAno de producion:%d\n", AGA.cat[i].ref[x].ano);
					printf("\tNumero de copias:%d\n", AGA.cat[i].ref[x].copias);
				}
			}
			system("pause");
			system("cls");
			o = 0;
		} else if(o == 6) {
			o = 6;
		}
	} while(o>6||o<1);
	printf("He salidooo\n\n");
*/
	return 0;
}







tipoCatalogo crearcatalogo(char claveC, char nombreC[_TAM_nombre], char descripcionC[_TAM_descripcion], int totalref){
	tipoCatalogo cat;
	cat.clave=claveC;
		strcpy(cat.nombre,nombreC);
		strcpy(cat.descripcion,descripcionC);
		cat.totalref=totalref;

		return cat;
   
}


int borrarcatalogo(int j)
{
	char s;
	printf("El catalogo que quiere borrar es:\n");
	printf("\tCatalogo:%s\n", AGA.cat[j].nombre);
	printf("\tClave:%c\n", AGA.cat[j].clave);
	printf("\tDescripcion:%s\n\n", AGA.cat[j].descripcion);
	printf("\t'S'=Si   'N'=NO :");
	fflush(stdin);
	scanf("%c", &s);
	if (tolower(s)=='s'){for(; j < AGA.totalcat; j++)
			AGA.cat[j] = AGA.cat[j + 1];
		AGA.totalcat--;
		printf("Catalogo eliminado\n\n");
		system("pause");
		system("cls");
	}
	system("cls");
	return j;
}



unsigned menu()
{
	unsigned o;
	do {

		printf("                 MENU PRINCIPAL\n");
		printf("\n\n\t1 - Crear catalogo\n");
		printf("\t2 - Eliminar catalogo\n");
		printf("\t3 - Crear Referencia Bibliografica\n");
		printf("\t4 - Eliminar Referencia Bibliografica\n");
		printf("\t5 - Ver listado del catalogo\n");
		printf("\t6 - Salir\n\n");
		printf("\tSu opcion: ");
		fflush(stdin);
		scanf("%d", &o);
		printf("\n");
		system("cls");
	} while(o > 6 || o < 1);
	return o;
}




void mostrarcatalogo(){
	int i;
	int x;
	printf("\tHay %d catalogos registrados en esta biblioteca.\n\n", AGA.totalcat);
			for(i = 0; i < AGA.totalcat; i++) {
				printf("\tCatalogo:%s\n", AGA.cat[i].nombre);
				printf("\tClave:%c\n", AGA.cat[i].clave);
				printf("\tDescripcion:%s\n\n", AGA.cat[i].descripcion);
				if(AGA.cat[i].totalref == 0) {
					printf("\tAun no hay referencias registradas para este catalogo.\n\n\n\n");
				} else
					printf("\tHay %d referencias registradas para este catalogo.\n\n\n\n", AGA.cat[i].totalref);
				for(x = 0; x < AGA.cat[i].totalref; x++) {
					printf("\tTitulo:%s\n", AGA.cat[i].ref[x].titulo);
					printf("\tAutor:%s\n", AGA.cat[i].ref[x].autor);
					printf("\tEditorial:%s\n", AGA.cat[i].ref[x].editorial);
					printf("\tAno de producion:%d\n", AGA.cat[i].ref[x].anio);
					printf("\tNumero de copias:%d\n", AGA.cat[i].ref[x].copias);
				}
			}
			system("pause");
			system("cls");
			
}




tipoReferencia crearreferencia(char tituloR[_TAM_titulo], char autorR[_TAM_autor],char editorialR[_TAM_editorial],int anioR,int copiasR)
	{
		tipoReferencia ref;
		strcpy(ref.titulo,tituloR);
		strcpy(ref.autor,autorR);
		strcpy(ref.editorial,editorialR);
		ref.anio=anioR;
		ref.copias=copiasR;
		
	
		return ref;
	}