#include <stdio.h>
#include <stdlib.h>
#include <string.h> // Trabajar con strings
#include <ctype.h> // Trabajar con mayusculas y minusculas

struct TReferencia {
	int identificador;
	char titulo[80];
	char autor[40];
	char editorial[50];
	int anio;
	int copias;
};

struct TCatalogo {
	char clave;
	char nombre[50];
	char descripcion[100];
	int cuentaReferencias;
	struct TReferencia referencia[10];
};

struct TBiblioteca {
	int cuenta;
	struct TCatalogo catalogo[5];
};

struct TBiblioteca Biblioteca;
struct TCatalogo newCatalogo (char clave, const char *nombre, const char *descripcion, int cuentaReferencias);
struct TReferencia newReferencia (int id, const char *titulo, const char *autor, const char *editorial, int anio, int copias);
unsigned menu ();

int main(int argc, char const *argv[])
{
	unsigned opcion;
	// Creo los catalogos
	Biblioteca.catalogo[0] = newCatalogo ( 'A', "Articulos", "Aqui van los articulos.", 0 );
	Biblioteca.catalogo[1] = newCatalogo ( 'L', "Libros", "Aqui van los libros.", 0 );
	Biblioteca.catalogo[2] = newCatalogo ( 'T', "Trabajos de fin de grado", "Aqui van los TFGs.", 0 );
	Biblioteca.cuenta = 3;

	// Creo las referencias
	Biblioteca.catalogo[0].referencia[0] = newReferencia ( 1, "Titulo del articulo 1", "Diego N", "EdBase", 2020, 45 );
	Biblioteca.catalogo[0].referencia[1] = newReferencia ( 2, "Titulo del articulo 2", "Diego M", "EdBase", 2019, 54 );
	Biblioteca.catalogo[0].referencia[2] = newReferencia ( 3, "Titulo del articulo 3", "JaviGar", "EdBase", 2018, 73 );
	Biblioteca.catalogo[0].cuentaReferencias = 3;

	Biblioteca.catalogo[1].referencia[0] = newReferencia ( 1, "Titulo del libro 1", "Diego N", "EdBase", 2020, 45 );
	Biblioteca.catalogo[1].referencia[1] = newReferencia ( 2, "Titulo del libro 2", "Diego M", "EdBase", 2019, 54 );
	Biblioteca.catalogo[1].referencia[2] = newReferencia ( 3, "Titulo del libro 3", "JaviGar", "EdBase", 2018, 73 );
	Biblioteca.catalogo[1].cuentaReferencias = 3;

	Biblioteca.catalogo[2].referencia[0] = newReferencia ( 1, "Titulo del TFG 1", "Diego N", "EdBase", 2020, 45 );
	Biblioteca.catalogo[2].referencia[1] = newReferencia ( 2, "Titulo del TFG 2", "Diego M", "EdBase", 2019, 54 );
	Biblioteca.catalogo[2].referencia[2] = newReferencia ( 3, "Titulo del TFG 3", "JaviGar", "EdBase", 2018, 73 );
	Biblioteca.catalogo[2].cuentaReferencias = 3;

	// Menu
	char clave, barraIdentificador;
	char nombre[50];
	char descripcion[100];
	char titulo[80];
	char autor[40];
	char editorial[50];
	char identificador[4];
	int anio, copias, indice, numeroIdentificador;
	do {
		opcion = menu ();
		switch (opcion) {
			case 1:
				printf("                 AÑADIR CATALOGO\n\n");
				printf("Clave: ");
				fflush(stdin);
				scanf (" %c", &clave);
				printf("Nombre: ");
				fflush(stdin);
				scanf (" %s", nombre);
				printf("Descripcion: ");
				fflush(stdin);
				scanf (" %s", descripcion);
				Biblioteca.catalogo[Biblioteca.cuenta] = newCatalogo (clave, nombre, descripcion, 0);
				Biblioteca.cuenta++;
				break;
			case 2:
				printf("               NUEVA REFERENCIA BIBLIOGRAFICA\n\n");
				printf("En que catalogo quieres añadir la referencia bibliografica? (");
				for (int i = 0; i < Biblioteca.cuenta; i++)
					printf("%c, ", Biblioteca.catalogo[i].clave);

				printf("\b\b): "); // Borro la ultima coma y el ultimo espacio y cierro el parentesis abierto en la linea 85
				fflush (stdin);
				scanf (" %c", &clave);
				// clase = toupper (clave);
				// Coje el indice del array Biblioteca.catalogo con la clave que esta almacenada en clave
				for (indice = 0; Biblioteca.catalogo[indice].clave != clave; indice++);

				printf("Titulo: ");
				fflush (stdin);
				scanf (" %s", titulo);
				printf("Editorial: ");
				fflush (stdin);
				scanf (" %s", editorial);
				printf("Autor: ");
				fflush (stdin);
				scanf (" %s", autor);
				printf("A%co: ", 164);
				fflush (stdin);
				scanf (" %i", &anio);
				printf("Número de copias: ");
				fflush (stdin);
				scanf (" %i", &copias);

				Biblioteca.catalogo[indice].cuentaReferencias++;
				Biblioteca.catalogo[indice].referencia[Biblioteca.catalogo[indice].cuentaReferencias] = newReferencia (Biblioteca.catalogo[indice].cuentaReferencias, (const char *)titulo, (const char *)autor, (const char *)editorial, anio, copias);
				break;
			case 3:
				printf("               ELIMINAR COPIA DE REFERENCIA BIBLIOGRAFICA\n\n");
				// ------------------------------------------------------------ Chorra
				// printf ("Introduzca la letra del catalogo: ");
				// fflush (stdin);
				// scanf (" %c", clave);
				// printf ("Introduzca el numero de referencia: ");
				// fflush (stdin);
				// scanf (" %i", numeroIdentificador);
				// --------------------------------------------------------- Con punteros
				// printf("Introduzca el identificador: ");
				// fflush (stdin);
				// scanf (" %s", identificador);
				// clave = identificador[0];
				// barraIdentificador = identificador[1];
				// if (identificador[3] == '\0') {
				// 	numeroIdentificador = atoi ((const char *)&identificador[2]);
				// } else {
				// 	if (atoi ((const char *)&identificador[2]) == 1 && atoi ((const char *)&identificador[3]) == 0){
				// 		numeroIdentificador = 10;
				// 	} else {
				// 		printf ("Ése es un número demasiado grande.");
				// 	}
				// }
				// -----------------------------------------------------------
				printf("Introduzca el identificador: ");
				fflush (stdin);
				scanf (" %s", identificador);
				clave = identificador[0];
				barraIdentificador = identificador[1];
				numeroIdentificador = (int)(identificador[2] - '0');
				// Saca el indice del catalogo por su letra
				for (indice = 0; Biblioteca.catalogo[indice].clave != clave; indice++);

				Biblioteca.catalogo[indice].referencia[numeroIdentificador].copias--;

				if (Biblioteca.catalogo[indice].referencia[numeroIdentificador].copias == 0) {
					printf("Numero de copias restantes: 0.\n");
					printf("Alerta: ¡SE HA REALIZADO LA ELIMINACION DEFINITIVA!\n");
				} else if (Biblioteca.catalogo[indice].referencia[numeroIdentificador].copias < 3) {
					printf("Numero de copias restantes: %i.\n", Biblioteca.catalogo[indice].referencia[numeroIdentificador].copias);
					printf("Alerta: ¡SE DEBEN ADQUIRIR COPIAS DE ESTA REFERENCIA!\n");
				}
				// Desplazo los que quedan detras del eliminado a una posición antes en el array Biblioteca.catalogo
				for (int i = indice; i < Biblioteca.catalogo[indice].cuentaReferencias; i++)
					Biblioteca.catalogo[indice] = Biblioteca.catalogo[indice + 1];
				break;
			case 4:
				break;
			case 5:
				break;
		}
	} while (opcion < 6);


	// printf("%s\n");
	return 0;
}

struct TCatalogo newCatalogo (char clave, const char *nombre, const char *descripcion, int cuentaReferencias) {
	struct TCatalogo resultado;

	resultado.clave = clave;
	strcpy (resultado.nombre, nombre);
	strcpy (resultado.descripcion, descripcion);
	resultado.cuentaReferencias = cuentaReferencias;

	return resultado;
}

struct TReferencia newReferencia (int id, const char *titulo, const char *autor, const char *editorial, int anio, int copias) {
	struct TReferencia resultado;

	resultado.identificador = id;
	strcpy (resultado.titulo, titulo);
	strcpy (resultado.autor, autor);
	strcpy (resultado.editorial, editorial);
	resultado.anio = anio;
	resultado.copias = copias;

	return resultado;
}

unsigned menu () {
	unsigned eleccion;
	do {
		printf ("                 MENU PRINCIPAL\n");
		printf ("1.- Alta de un nuevo catalogo en el sistema.\n");
		printf ("2.- Nueva referencia bibliografica.\n");
		printf ("3.- Eliminar referencia bibliografica.\n");
		printf ("4.- Baja de un catalogo.\n");
		printf ("5.- Listado del catalogo de la AGA.\n");
		printf ("6.- Terminar.\n");
		printf ("Elija: ");
		fflush (stdin);
		scanf (" %u", &eleccion);
	} while (eleccion > 7 || eleccion < 0);

	return eleccion;
}