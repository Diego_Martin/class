#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 5

double getAngulo (double angulo);

int factorial (int num);

int main(int argc, char const *argv[])
{
	double angulo; // Esto es x
	do {

		printf("Introduce un angulo en radianes [0-1]:");
		scanf (" %lf", &angulo);
		// if (angulo <= 1 && angulo >= 0) break;
	// } while (true);
	} while (!(angulo <= 1 && angulo >= 0));

	printf("%.5lf\n", getAngulo(angulo));

	return EXIT_SUCCESS;
}

int factorial (int num) {
	int res = 1;
	int i;
	for (i = num; i > 0; i--) {
		res *= i;
	}
	return res;
}

double getAngulo (double angulo) {
	double res = 0;
	int i;
	for (i = 0; i < N; i++) {
		res += pow(-1, i) * pow(angulo, 2 * i + 1) / factorial(2 * i + 1);
	}
	return res;
}