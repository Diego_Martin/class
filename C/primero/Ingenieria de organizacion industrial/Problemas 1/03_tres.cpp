#include <stdio.h>

int mas (double dato);
int caps (int soldados);
double pasarAHectareas (double dim, char uni);
void calculaEImprime (char ter, double dim, char uni);

int main(int argc, char const *argv[])
{
	char terreno, unidad;
	double dimensiones;

	/* Pedimos los datos */
	printf ("Indique la siguiente informacion:\n");
	printf ("- Tipo de terreno (S, D, M, U): ");
	scanf (" %c", &terreno);
	printf ("- Dimensiones del terreno:      ");
	scanf (" %lf", &dimensiones);
	printf ("- Unidad de medida (K, H, M):   ");
	scanf (" %c", &unidad);

	calculaEImprime (terreno, dimensiones, unidad);

	return 0;
}

double pasarAHectareas (double dim, char uni) {
	switch (uni) {
		case 'H':
		case 'h':
			return dim;
			break;
		case 'K':
		case 'k':
			return dim * 100;
			break;
		case 'M':
		case 'm':
			return dim / 10000;
			break;
	}
}

// mas ( calculos ,  cantidad de dotación por unidad de medida de superficie);
int mas (double dato, int cantidad) {
	if (dato - (int) dato > 0.00000) {
		return ((int) dato + cantidad);
	}

	return (int) dato + ((dato - (int) dato) * cantidad);
}

int caps (int soldados) {
	int res = soldados / 20;

	if (soldados % 20 > 0)
		res ++;

	return res;
}

void calculaEImprime (char ter, double dim, char uni) {
	int soldados = 0, drones = 0, aviones = 0, capitanes = 0;

	dim = pasarAHectareas (dim, uni);
	/* Evaluamos el terreno */
	switch (ter) {
		/* Selva */
		case 'S':
		case 's':
			if (dim < 0.5) {
				soldados = 20;
			} else if (dim >= 0.5 && dim <= 3) {
				drones = mas (4 * dim, 4);
			} else {
				aviones = mas (1 * (dim / 10), 1);
			}
			break;
		/* Desierto */
		case 'D':
		case 'd':
			if (dim < 2) {
				soldados = 12;
			} else if (dim >= 2 && dim <= 5) {
				drones = mas (1 * (dim / 2), 1);
				soldados = mas (1 * (dim / 2), 1);
			} else {
				aviones = mas (1 * (dim / 20), 1);
			}
			break;
		/* Montaña */
		case 'M':
		case 'm':
			aviones = mas (1 * (dim / 0.5), 1);
			soldados = mas (8 * (dim / 0.5), 8);
			break;
		/* Urbano */
		case 'U':
		case 'u':
			if (dim < 10000) {
				drones = mas (1 * (dim / 1000), 1);
				soldados = mas (10 * (dim / 1000), 10);
			} else if (dim >= 10000 && dim <= 50000) {
				drones = mas (1 * (dim / 2000), 1);
				soldados = mas (20 * (dim / 2000), 20);
			} else {
				drones = mas (1 * (dim / 5000), 1);
				soldados = mas (30 * (dim / 5000), 30);
			}
			break;
	}
	capitanes = caps (soldados);

	printf("La dotacion que se necesita es:\n");
	printf("SOLDADOS\tDRONES\t\tAVIONES\t\tN CAPITANES\n");
	printf("%04i\t\t%04i\t\t%04i\t\t%04i\n", soldados, drones, aviones, capitanes);
}