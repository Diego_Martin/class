#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char const *argv[])
{
	int num, resto, count = 0;
	double res = 0;
	int esBinario = 0;
	
	while (!esBinario) {
		printf("Introduce un número en binario: ");
		scanf (" %i", &num);

		while (num > 0) {
			printf("\033[37;32mVuelta: %i\033[37;40m\n", count);
			resto = num % 10;
			if (resto == 0) {
				num /= 10;
				count++;
				esBinario = 1;
			} else if (resto == 1) {
				res += pow (2, count);
				num /= 10;
				count++;
				esBinario = 1;
			} else {
				printf("Ese numero no esta en binario.\n");
				esBinario = 0;
				break;
			}
			printf("num: %i\n", num);
			printf("resto: %i\n", resto);
			printf("count: %i\n", count);
			printf("res (Resultado): %i\n", res);
		}
		if (num == 0) {
			res = (double) num;
			esBinario = 1;
		}
	}

	if (esBinario)
		printf("Número en decimal: %.0lf\n", res);
	return 0;
}