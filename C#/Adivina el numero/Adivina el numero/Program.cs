﻿using System;

namespace Adivina_el_numero {

    class Program {

        static void Main (string[] args){

            Random rnd = new Random ();
            int numProp = 0, numAleat, i;
            string numProps = "";
            Console.WriteLine ("+--------------------------------------------------------------+");
            Console.WriteLine ("|                      Adivina el numero                       |");
            Console.WriteLine ("+--------------------------------------------------------------+");
            Console.WriteLine ("| Voy a pensar un numero del 0 al 100, ¡debes adivinarlo!      |");
            Console.WriteLine ("+--------------------------------------------------------------+");
            Console.WriteLine ("| Adelante!                                                    |");
            Console.WriteLine ("+--------------------------------------------------------------+");
            Console.WriteLine ("| Tienes 5 intentos de limite                                  |");
            Console.WriteLine ("+--------------------------------------------------------------+");
            numAleat = rnd.Next (0, 100);
            for (i = 0; i < 5; i++) {
                Console.Write ("  ¿Cual crees que es el numero?");
                numProps = Console.ReadLine ();
                if (String.IsNullOrEmpty (numProps)) {
                    numProp = 0;
                    Console.WriteLine (numProp);
                } else {
                    numProp = Convert.ToInt32 (numProps);
                }
                if (numProp < 101 && numProp > -1) {
                    if (numProp == numAleat) {
                        break;
                    } else if (numProp > numAleat) {
                        Console.WriteLine ("¡Tu numero es demasiado grande!");
                    } else {
                        Console.WriteLine ("¡Tu numero es demasiado pequeño!");
                    }
                } else {
                    Console.WriteLine ("El numero debe ser entre 0 y 100.");
                }
                Console.WriteLine ("+--------------------------------------------------------------+");
            }
            if (numProp == numAleat) {
                i++;
                Console.WriteLine("  ¡Has ganado en {0} intentos!", i);
            } else {
                Console.WriteLine("  ¡Has perdido! El numero era {0}", numAleat);
            }
            Console.ReadLine();
        }
    }
}
