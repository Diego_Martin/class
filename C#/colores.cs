using System;
namespace HelloWorld
{
    class Hello 
    {
        static void Main() 
        {
            int valor = int.Parse(Console.ReadLine());
            switch (valor)
            {
                case 1:
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("rojo");
                    break;
                case 2:
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("azul");
                    break;
                case 3:
                    Console.BackgroundColor = ConsoleColor.Yellow; 
                    Console.WriteLine("amarillo");
                    break;
                default://defecto
                    Console.Write("Se ingreso un valor fuera de rango");
                    break;
            }
            Console.WriteLine();
        }
    }
}