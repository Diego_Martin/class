﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using OpenQA.Selenium.Chrome;

namespace AsistenteVirtual
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class VentanaPrincipal : Window
    {
        SpeechRecognitionEngine oidos = new SpeechRecognitionEngine();
        SpeechSynthesizer voz = new SpeechSynthesizer();
        public static readonly String[] meses = { null, "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
        String frase;
        int formatoHora = 12;
        bool reconociendo = true;
        private static String usuario;
        private Random NumAleat = new Random ();

        public VentanaPrincipal()
        {
            // Carga la ventana
            usuario = "Diego";
            InitializeComponent();
            Saludar();
        }

        private void VentanaCargada(object sender, RoutedEventArgs e)
        {
            cargarGramaticas();
        }

        private void cargarGramaticas ()
        {
            oidos.LoadGrammarAsync(new Grammar(new GrammarBuilder(new Choices(File.ReadAllLines(@"util\Comandos.txt")))));
            oidos.LoadGrammar(new DictationGrammar());
            oidos.RequestRecognizerUpdate();
            oidos.SpeechRecognized += Oidos_SpeechRecognized;

            voz.SpeakStarted += Voz_SpeakStarted;
            voz.SpeakCompleted += Voz_SpeakCompleted;

            oidos.SetInputToDefaultAudioDevice();
            oidos.RecognizeAsync(RecognizeMode.Multiple);
        }

        private void Voz_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        {
            reconociendo = true;
        }

        private void Voz_SpeakStarted(object sender, SpeakStartedEventArgs e)
        {
            reconociendo = false;
        }

        private void Oidos_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            frase = e.Result.Text;
            LblReconocimiento.Content = frase;
            frase = frase.ToLower();
            Ejecutar();

            if (!reconociendo)
            {
                frase = null;
            }
        }

        public bool ListaEnOrden (String []lista, String orden, String []y = null, String []o = null, String []no = null)
        {
            foreach (String item in lista)
            {
                if (y == null && o == null && no == null)
                {
                    if (orden.Contains(item))
                    {
                        return true;
                    }
                }
                else if (y != null && o == null && no == null)
                {
                    foreach (String i in y)
                    {
                        if (orden.Contains(item) && orden.Contains(i))
                        {
                            return true;
                        }
                    }
                }
                else if (y == null && o != null && no == null)
                {
                    foreach (String or in o)
                    {
                        if (orden.Contains(item) || orden.Contains(or))
                        {
                            return true;
                        }
                    }
                }
                else if (y == null && o == null && no != null)
                {
                    foreach (String not in no)
                    {
                        if (orden.Contains(item) && !orden.Contains(not))
                        {
                            return true;
                        }
                    }
                }
                else if (y != null && o == null && no != null)
                {
                    foreach (String i in y)
                    {
                        foreach (String not in no)
                        {
                            if (orden.Contains(item) && orden.Contains(i) && !orden.Contains(not))
                            {
                                return true;
                            }
                        }
                    }
                }
                else if (y != null && o != null && no == null)
                {
                    foreach (String i in y)
                    {
                        foreach (String or in o)
                        {
                            if ((orden.Contains(item) && orden.Contains(i)) || (orden.Contains(item) && orden.Contains(or)))
                            {
                                return true;
                            }
                        }
                    }
                }
                else if (y == null && o != null && no != null)
                {
                    foreach (String i in y)
                    {
                        foreach (String not in no)
                        {
                            if ((orden.Contains(item) && orden.Contains(i)) && (orden.Contains(item) && !orden.Contains (not)))
                            {
                                return true;
                            }
                        }
                    }
                }
                else if (y != null && o != null && no != null)
                {
                    foreach (String i in y)
                    {
                        foreach (String not in no)
                        {
                            foreach (String or in o)
                            {
                                if ((orden.Contains(item) && orden.Contains(i)) && (orden.Contains (item) || orden.Contains(or)) && !orden.Contains(not))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        public int FormatoHoras(int hora)
        {
            if (formatoHora == 12)
                return hora % 12;
            else if (formatoHora == 24)
                return hora % 24;
            else
            {
                formatoHora = 12;
                return hora % 12;
            }
        }

        public void Saludar()
        {
            int hora = DateTime.Now.Hour;
            String[,] saludos = { { $"Buenos dias, {usuario}", $"Buen dia, {usuario}", "Buenos dias" }, { $"Buenas tardes, {usuario}", $"Buena tarde, {usuario}", "Buenas tardes" }, { $"Buenas noches, {usuario}", $"Buena noche, {usuario}", "Buenas noches" } };
            if (hora >= 6 && hora < 13)
                voz.Speak(saludos[0, NumAleat.Next(0, 3)]);
            else if (hora >= 13 && hora < 19)
                voz.Speak(saludos[1, NumAleat.Next(0, 3)]);
            else
                voz.Speak(saludos[2, NumAleat.Next(0, 3)]);

            if (NumAleat.Next(0, 100) < 60)
                voz.Speak(new String[] { $"Son las {hora} en tu ciudad", $"Son las {hora}", $"Van siendo las {hora}", $"En el reloj pone que son las {hora}" }[NumAleat.Next(0, 4)]);

            if (NumAleat.Next(0, 100) < 60)
            {
                int []dia = GetArrayDia ();
                voz.Speak(new String[] { $"Es {dia[0]} de {meses[dia[1]]}", $"Hoy es {dia[0]} de {meses[dia[1]]}", $"Es {dia[0]} de {meses[dia[1]]} de {dia[2]}", $"Hoy es {dia[0]} de {meses[dia[1]]} de {dia[2]}" }[NumAleat.Next(0, 4)]);
            }
        }

        public int[] GetArrayDia ()
        {
            return new int[] { DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year };
        }

        public String Hora ()
        {
            DateTime ahora = new DateTime();
            String hora = "";
            if (ahora.Hour == 0)
                hora = "12";
            else
                hora = FormatoHoras(ahora.Hour).ToString();

            if (ahora.Minute == 15)
                hora = $"{hora} y cuarto";
            
            else if (ahora.Minute == 30)
                hora = $"{hora} y media";
            
            else if (ahora.Minute == 45)
                hora = $"{hora} menos cuarto";
            
            else if (ahora.Minute == 0)
                hora = $"{hora} en punto";
            
            else
                hora = $"{hora} y {DateTime.Now.ToString("mm")}";

            return hora;
        }

        public void Ejecutar ()
        {
            if (frase != null && frase != "")
            {
                if (ListaEnOrden(new String[] { "buenos dias yarvis", "buenos días yarvis", "buenos dias", "buenos días", "buen dia", "buen día", "buenas tardes yarvis", "buenas noches yarvis", "hola", "que tal", "buena noche", "buena tarde" }, frase))
                {
                    voz.Speak("Buenos dias señor");
                }
                else if (ListaEnOrden(new String[] { "abre google", "abre internet", "abrir google", "abrir internet" }, frase))
                {
                    voz.Speak("Abriendo el buscador");
                    System.Diagnostics.Process.Start("https://google.es/");
                }
                else if (ListaEnOrden(new String[] { "abre youtube", "abrir youtube" }, frase))
                {
                    voz.Speak("Abriendo youtube");
                    System.Diagnostics.Process.Start("https://youtube.com/");
                }
                else if (ListaEnOrden(new String[] { "abre spo", "abrir spo", "abre espo", "abrir espo" }, frase, no: new String[] { "navegador" }))
                {
                    voz.Speak("Abriendo Spotify en el ordenador");
                    System.Diagnostics.Process.Start(@"C:\Users\Usuario\AppData\Roaming\Spotify\Spotify.exe");
                }
                else if (ListaEnOrden(new String[] { "abre spo", "abrir spo", "abre espo", "abrir espo" }, frase, y: new String[] { "navegador" }))
                {
                    voz.Speak("Abriendo Spotify en el navegador");
                    System.Diagnostics.Process.Start("https://open.spotify.com/");
                }
                else if (ListaEnOrden(new String[] { "hora es", "dime la hora", "en el reloj" }, frase))
                {
                    String hora = Hora();
                    if (ListaEnOrden(new String[] { "en el reloj" }, frase))
                        voz.Speak(new String[] { $"En el reloj pone que son las {hora}", $"En el reloj pone que son las {hora} minutos" }[NumAleat.Next(0, 2)]);
                    else
                        voz.Speak(new String[] { $"Las {hora}", $"Son las {hora}", $"Van siendo las {hora}" }[NumAleat.Next(0, 3)]);

                }
                else if (ListaEnOrden(new String[] { "dia es", "dime el día", "dime el dia", "hoy es" }, frase))
                {
                    int []dia = GetArrayDia();
                    voz.Speak(new String[] { $"Es {dia[0]} de {meses[dia[1]]}", $"Hoy es {dia[0]} de {meses[dia[1]]}", $"Es {dia[0]} de {meses[dia[1]]} de {dia[2]}", $"Hoy es {dia[0]} de {meses[dia[1]]} de {dia[2]}" }[NumAleat.Next(0, 4)]);
                }
                else if (ListaEnOrden(new String[] { "adios", "adiós", "apagate", "apágate", "apagar", "exit", "desconectar", "desconectate", "desconéctate" }, frase, no: new String[] { "exito", "éxito" }))
                {
                    voz.Speak(new String[] { "Adios", "Hasta luego", "adios adios", $"venga, {usuario} hasta luego", $"Adios {usuario}", $"hasta luego {usuario}" }[NumAleat.Next(0, 6)]);
                    Environment.Exit(0);
                    Close();
                }
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            VentanaInfoSys infoSys = new VentanaInfoSys();
            infoSys.Show();
        }
    }
}
