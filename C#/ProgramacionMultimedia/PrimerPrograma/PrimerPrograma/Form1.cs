﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrimerPrograma
{
    public partial class Form1 : Form
    {
        private bool flag = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[] tabla = { 11, 22, 33, 44 };
            int numero;
            label1.Text = "";

            for (int i = 0; i < tabla.Length; i++)
            {
                numero = tabla[i];
                if (i < tabla.Length - 1)
                    label1.Text += numero + " - ";
                else
                    label1.Text += numero;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (flag)
                this.button2.BackColor = System.Drawing.Color.Red;
            else
                this.button2.BackColor = System.Drawing.Color.Blue;

            flag = !flag;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
            String res;
            try
            {
                int op1 = Convert.ToInt32(this.inputNumero1.Text);
                int op2 = Convert.ToInt32(this.inputNumero2.Text);
                res = $"{(op1 + op2)}";
            }
            catch
            {
                res = "Debes introducir números";
            }
            this.labelResultado.Text = res;
        }
    }
}
