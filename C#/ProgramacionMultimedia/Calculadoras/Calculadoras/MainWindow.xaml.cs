﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculadoras
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Double resultado = 0;
        private String texto = "";

        public static Double Evaluate(string expression)
        {
            System.Data.DataTable table = new System.Data.DataTable();
            table.Columns.Add("expression", string.Empty.GetType(), expression);
            System.Data.DataRow row = table.NewRow();
            table.Rows.Add(row);
            return Double.Parse((string)row["expression"]);
        }

        private int Factorial (int valor)
        {
            int res = 1;
            for (int i = 1; i <= valor; i++)
                res *= i;

            return res;
        }

        private int Factorial (double valor)
        {
            return Factorial((int)valor);
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Input_TextChanged(object sender, TextChangedEventArgs e)
        {
            texto = this.Input.Text;
        }

        private void BotonModulo_Click(object sender, RoutedEventArgs e)
        {
            texto += "%";
            this.Input.Text = texto;
        }

        private void BotonBorrarTodo_Click(object sender, RoutedEventArgs e)
        {
            texto = "";
            this.Input.Text = texto;
        }

        private void BotonBorrar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                texto = texto.Remove(texto.Length - 1);
            }
            catch (ArgumentOutOfRangeException)
            {
                texto = "";
            }
            this.Input.Text = texto;
        }

        private void BotonDivision_Click(object sender, RoutedEventArgs e)
        {
            texto += "/";
            this.Input.Text = texto;
        }

        private void Boton7_Click(object sender, RoutedEventArgs e)
        {
            texto += "7";
            this.Input.Text = texto;
        }

        private void Boton8_Click(object sender, RoutedEventArgs e)
        {
            texto += "8";
            this.Input.Text = texto;
        }

        private void Boton9_Click(object sender, RoutedEventArgs e)
        {
            texto += "9";
            this.Input.Text = texto;
        }

        private void BotonMultiplicacion_Click(object sender, RoutedEventArgs e)
        {
            texto += "*";
            this.Input.Text = texto;
        }

        private void Boton4_Click(object sender, RoutedEventArgs e)
        {
            texto += "4";
            this.Input.Text = texto;
        }

        private void Boton5_Click(object sender, RoutedEventArgs e)
        {
            texto += "5";
            this.Input.Text = texto;
        }

        private void Boton6_Click(object sender, RoutedEventArgs e)
        {
            texto += "6";
            this.Input.Text = texto;
        }

        private void BotonResta_Click(object sender, RoutedEventArgs e)
        {
            texto += "-";
            this.Input.Text = texto;
        }

        private void Boton1_Click(object sender, RoutedEventArgs e)
        {
            texto += "1";
            this.Input.Text = texto;
        }

        private void Boton2_Click(object sender, RoutedEventArgs e)
        {
            texto += "2";
            this.Input.Text = texto;
        }

        private void Boton3_Click(object sender, RoutedEventArgs e)
        {
            texto += "3";
            this.Input.Text = texto;
        }

        private void BotonSuma_Click(object sender, RoutedEventArgs e)
        {
            texto += "+";
            this.Input.Text = texto;
        }

        private void Boton0_Click(object sender, RoutedEventArgs e)
        {
            texto += "0";
            this.Input.Text = texto;
        }

        private void BotonComa_Click(object sender, RoutedEventArgs e)
        {
            texto += ".";
            this.Input.Text = texto;
        }

        private void BotonIgual_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                resultado = Evaluate(texto);
                texto = $"{resultado}";
            }
            catch
            {
                texto = "Ésa no es una operacion valida.";
                Console.WriteLine("Sa pasao");
            }
            this.Input.Text = texto;
        }

        private void BotonFactorial_Click(object sender, RoutedEventArgs e)
        {
            int res = Factorial(Evaluate(texto));
            this.Input.Text = $"{res}";
        }
    }
}
