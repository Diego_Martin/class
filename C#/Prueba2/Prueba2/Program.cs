using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prueba2
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            const string saludo = "Hola";
            string nombre = "Diego"; // String, constante
            int prueba = 23; // Entero
            uint prueba2 = 34; // Entero grande
            float prueba3 = 45.56f; // Punto flotante (SIEMPRE CON f!)
            double prueba4 = 56.67; // Double
            decimal prueba5 = 67.78m; // N�mero enorme (SIEMPRE CON m!)
            byte prueba6 = 255; // Entero hasta maximo 255
            bool prueba7 = true; // Booleano

            DateTime fecha1 = DateTime.Today; // Fecha
            // fecha1.Day; // Dia en n�mero del mes
            // fecha1.DayOfWeek; // Dia de la semana
            // fecha1.DayOfYear; // Dia del a�o (1 - 365)
            // fecha1.Year; // A�o
            // fecha1.Month; // Mes
            // fecha1.Date; // Fecha completa
            // fecha1.ToShortDateString() // Solo fecha
            // fecha1.ToShortTimeString() // Solo hora
            prueba.ToString();
        }
    }
}
