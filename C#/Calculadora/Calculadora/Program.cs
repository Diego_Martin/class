﻿using System;

namespace Calculadora
{
    class Program
    {
        static void Main()
        {
            int op, op1, op2, res;
            double eop1, eop2, eres;
            Console.WriteLine ("+-------------------------------------------------------------+");
            Console.WriteLine ("|  1. Suma.                                                   |");
            Console.WriteLine ("|  2. Resta.                                                  |");
            Console.WriteLine ("|  3. Multiplicacion.                                         |");
            Console.WriteLine ("|  4. Division.                                               |");
            Console.WriteLine ("|  5. Potencia.                                               |");
            Console.WriteLine ("|  6. Salir.                                                  |");
            Console.WriteLine ("+-------------------------------------------------------------+");
            do {
                Console.Write ("Escribe la operacion a realizar.");
                op = Convert.ToInt32 (Console.ReadLine ());
                switch (op) {
                    case 1:
                        Console.Write ("Escribe el primer sumando:");
                        op1 = Convert.ToInt32 (Console.ReadLine ());
                        Console.Write ("Escribe el segundo sumando:");
                        op2 = Convert.ToInt32 (Console.ReadLine ());
                        res = op1 + op2;
                        Console.WriteLine ("El resultado es: {0}", res);
                        Console.WriteLine ("+-------------------------------------------------------------+");
                        break;
                    case 2:
                        Console.Write ("Escribe el primer restando:");
                        op1 = Convert.ToInt32 (Console.ReadLine ());
                        Console.Write ("Escribe el segundo restando:");
                        op2 = Convert.ToInt32 (Console.ReadLine ());
                        res = op1 - op2;
                        Console.WriteLine ("El resultado es: {0}", res);
                        Console.WriteLine ("+-------------------------------------------------------------+");
                        break;
                    case 3:
                        Console.Write ("Escribe el multiplicando:");
                        op1 = Convert.ToInt32 (Console.ReadLine ());
                        Console.Write ("Escribe el multiplicador:");
                        op2 = Convert.ToInt32 (Console.ReadLine ());
                        res = op1 * op2;
                        Console.WriteLine ("El resultado es: {0}", res);
                        Console.WriteLine ("+-------------------------------------------------------------+");
                        break;
                    case 4:
                        Console.Write ("Escribe el dividendo:");
                        op1 = Convert.ToInt32 (Console.ReadLine ());
                        Console.Write ("Escribe el divisor:");
                        op2 = Convert.ToInt32 (Console.ReadLine ());
                        res = op1 / op2;
                        Console.WriteLine ("El resultado es: {0}", res);
                        Console.WriteLine ("+-------------------------------------------------------------+");
                        break;
                    case 5:
                        Console.Write ("Escribe la base:");
                        eop1 = Convert.ToDouble (Console.ReadLine ());
                        Console.Write ("Escribe el exponente:");
                        eop2 = Convert.ToDouble (Console.ReadLine ());
                        eres = Math.Pow(eop1, eop2);
                        Console.WriteLine ("El resultado es: {0}", eres);
                        Console.WriteLine ("+-------------------------------------------------------------+");
                        break;
                    case 6:
                        Console.Write ("Has salido. Taluego!");
                        Console.ReadLine ();
                        break;
                    default:
                        Console.WriteLine ("Debe ser un numero del 1 al 6");
                        break;
                }
            } while (op != 6);
        }
    }
}