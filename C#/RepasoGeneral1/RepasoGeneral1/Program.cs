﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoGeneral1
{
    class Program
    {
        static void Main(string[] args)
        {
            String cadena = null;
            int opcion = 0;
            do
            {
                opcion = menu();
                switch (opcion)
                {
                    case 0:
                        break;
                    case 1:
                        Console.WriteLine("+-----------------------------------------+");
                        Console.WriteLine("|           Inicializar la cadena         |");
                        Console.WriteLine("+-----------------------------------------+");
                        cadena = "";
                        break;
                    case 2:
                        Console.WriteLine("+-----------------------------------------+");
                        Console.WriteLine("|           Rellenar la cadena            |");
                        Console.WriteLine("+-----------------------------------------+");
                        if (cadena == null)
                            Console.WriteLine("| La cadena no está inicializada.        |");
                        else
                        {
                            Console.Write("| Introduce el valor: ");
                            cadena = Console.ReadLine();
                        }
                        Console.WriteLine("+-----------------------------------------+");
                        break;
                    case 3:
                        Console.WriteLine("+-----------------------------------------+");
                        Console.WriteLine("|            Imprimir la cadena           |");
                        Console.WriteLine("+-----------------------------------------+\n");
                        if (cadena == null)
                            Console.WriteLine("| La cadena no está inicializada.        |");
                        else
                            Console.WriteLine(cadena);
                        break;
                    case 4:
                        Console.WriteLine("+-----------------------------------------+");
                        Console.WriteLine("|            Invertir la cadena           |");
                        Console.WriteLine("+-----------------------------------------+\n");
                        Console.WriteLine(Invertir(cadena));
                        break;
                    case 5:
                        Console.WriteLine("+-----------------------------------------+");
                        Console.WriteLine("|       Pasar la cadena a mayusculas      |");
                        Console.WriteLine("+-----------------------------------------+\n");
                        cadena = cadena.ToUpper();
                        if (cadena == null)
                            Console.WriteLine("| La cadena no está inicializada.        |");
                        else
                            Console.WriteLine(cadena);
                        break;
                    case 6:
                        Console.WriteLine("+-----------------------------------------+");
                        Console.WriteLine("|       Pasar la cadena a minusculas      |");
                        Console.WriteLine("+-----------------------------------------+\n");
                        cadena = cadena.ToUpper();
                        if (cadena == null)
                            Console.WriteLine("| La cadena no está inicializada.        |");
                        else
                            Console.WriteLine(cadena);
                        break;
                    case 7:
                        Console.WriteLine("+-----------------------------------------+");
                        Console.WriteLine("|       Rotar la cadena a la derecha      |");
                        Console.WriteLine("+-----------------------------------------+\n");
                        if (cadena == null)
                            Console.WriteLine("| La cadena no está inicializada.        |");
                        else
                        {
                            cadena = Rotar(cadena, "d");
                            Console.WriteLine(cadena);
                        }
                        break;
                    case 8:
                        Console.WriteLine("+-----------------------------------------+");
                        Console.WriteLine("|      Rotar la cadena a la izquierda     |");
                        Console.WriteLine("+-----------------------------------------+\n");
                        if (cadena == null)
                            Console.WriteLine("| La cadena no está inicializada.        |");
                        else
                        {
                            cadena = Rotar(cadena, "i");
                            Console.WriteLine(cadena);
                        }
                        break;
                    case 9:
                        Console.WriteLine("+-----------------------------------------+");
                        Console.WriteLine("|                  Salir                  |");
                        Console.WriteLine("+-----------------------------------------+");
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Ésa no es una opción válida.");
                        break;
                }
            } while (opcion != 9);
        }

        private static string Invertir(string cadena)
        {
            string res = "";
            for (int i = cadena.Length - 1; i >= 0; i--)
                res += cadena[i];

            return res;
        }

        private static string Rotar (string cadena, string direccion)
        {
            string res;
            try
            {
                Console.WriteLine("Rotar():129: cadena.Substring(0, cadena.Length - 2): " + cadena.Substring(0, cadena.Length - 2) + ", cadena.Last(): " + cadena.Last());
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("Error");
            }
            if (direccion.ToLower() == "i")
                res = cadena.Substring(1, cadena.Length - 1) + cadena[0];
            else if (direccion.ToUpper() == "d") // 7 es este
                res = cadena.Last() + cadena.Substring(0, cadena.Length - 2);
            else
                res = cadena;
            return res;
        }

        public static int menu ()
        {
            int res;
            Console.WriteLine("+-----------------------------------------+");
            Console.WriteLine("|                   Menu                  |");
            Console.WriteLine("+-----------------------------------------+");
            Console.WriteLine("| 1.- Inicializar la cadena.              |");
            Console.WriteLine("| 2.- Rellenar la cadena con una palabra  |");
            Console.WriteLine("| 3.- Imprimir la cadena.                 |");
            Console.WriteLine("| 4.- Invertir la cadena.                 |");
            Console.WriteLine("| 5.- Convertir la cadena a mayusculas.   |");
            Console.WriteLine("| 6.- Convertir la cadena a minusculas.   |");
            Console.WriteLine("| 7.- Rotar la cadena a la derecha.       |");
            Console.WriteLine("| 8.- Rotar la cadena a la izquierda.     |");
            Console.WriteLine("| 9.- Salir.                              |");
            Console.WriteLine("+-----------------------------------------+");
            Console.Write    ("| Opcion: ");
            try
            {
                res = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("El valor introducido debe ser un número entero.");
                res = 0;
            }
            Console.WriteLine("+-----------------------------------------+");
            return res;
        }
    }
}
