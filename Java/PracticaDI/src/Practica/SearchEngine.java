package Practica;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class SearchEngine {
	
	public String buscar (String url, String htmlPath) {
		Document doc = null;
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error interno. No se pudo obtener la noticia de " + url + ".", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
        Element result = doc.select(htmlPath).first();
        return result.text();
	}
	
}
