package Practica;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;

import javax.swing.JLabel;

public class Opciones {
	
	private JPanel pantallaOpciones;
	private JMenuItem panelMenuinfo;
	private App app;
	private EventManager eventos;
	
	public Opciones (App app, Configuracion config, Noticias noti) {
		this.app = app;
		this.eventos = new EventManager ();
		pantallaOpciones = new JPanel ();
		pantallaOpciones.setForeground(new Color(255, 255, 255));
		pantallaOpciones.setBounds(0, 0, 834, 461);
		pantallaOpciones.setLayout(null);
		Color colorbtns = new Color (255, 0, 0);
		LineBorder bordebtns = new LineBorder (new Color (255, 150, 150), 4);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(-2, 0, 836, 22);
		pantallaOpciones.add(menuBar);
		
		JMenu btnMenuInfo = new JMenu("...");
		btnMenuInfo.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(btnMenuInfo);
		
		panelMenuinfo = new JMenuItem(""); // app.getUserInfo ().prettify()
		btnMenuInfo.add(panelMenuinfo);
		
		JMenuItem panelMenuAcercaDe = new JMenuItem("Acerca de...");
		panelMenuAcercaDe.addActionListener(this.eventos.showInfo());
		btnMenuInfo.add(panelMenuAcercaDe);
		
		Icon iconoSalir = new ImageIcon("./media/btn-salir.png");
		JButton btnSalir = new JButton(iconoSalir);
		btnSalir.setBounds(784, 411, 50, 50);
		btnSalir.addActionListener(eventos.btnSalirClicked(this.app));
		btnSalir.setBorder(bordebtns);
		pantallaOpciones.add(btnSalir);
		
		JButton btnToLog = new JButton("Log");
		btnToLog.addActionListener(this.eventos.btnOpcionesToLogClicked(this.app));
		btnToLog.setFont(new Font("Calibri", Font.PLAIN, 25));
		btnToLog.setBackground(colorbtns);
		btnToLog.setForeground(Color.WHITE);
		btnToLog.setBounds(252, 228, 327, 39);
		btnToLog.setBorder(bordebtns);
		pantallaOpciones.add(btnToLog);
		
		JButton btnToConfig = new JButton ("Configuración");
		btnToConfig.addActionListener(this.eventos.btnOpcionesToConfigClicked(this.app, config));
		btnToConfig.setFont(new Font("Calibri", Font.PLAIN, 25));
		btnToConfig.setBackground(colorbtns);
		btnToConfig.setForeground(Color.WHITE);
		btnToConfig.setBounds(252, 270, 327, 39);
		btnToConfig.setBorder(bordebtns);
		pantallaOpciones.add(btnToConfig);
		
		JButton btnToNoticias = new JButton ("Noticias");
		btnToNoticias.addActionListener(this.eventos.btnOpcionesToNoticiasClicked(this.app, noti));
		btnToNoticias.setFont(new Font("Calibri", Font.PLAIN, 25));
		btnToNoticias.setBackground(colorbtns);
		btnToNoticias.setForeground(Color.WHITE);
		btnToNoticias.setBounds(252, 312, 327, 39);
		btnToNoticias.setBorder(bordebtns);
		pantallaOpciones.add(btnToNoticias);
		
		Icon bannerMenu = new ImageIcon("./media/banner-news.png");
		JLabel lblBanner = new JLabel(bannerMenu);
		lblBanner.setBounds(-2, 21, 836, 206);
		pantallaOpciones.add(lblBanner);
	}
	
	public JPanel get () {
		return this.pantallaOpciones;
	}
	
	public void updateInfoUser (String info) {
		this.panelMenuinfo.setText(info);
	}
}
