package Practica;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Login {
	private JPanel pantallaLogin;
	private JLabel lblUsuario;
	private JLabel lblPass;
	private JTextField inputUsuario;
	private JPasswordField inputPass;
	private EventManager eventos;
	
	public Login (App app) {
		this.eventos = new EventManager ();
		pantallaLogin = new JPanel();
		pantallaLogin.setForeground(new Color(255, 255, 255));
		pantallaLogin.setBackground(UIManager.getColor("InternalFrame.activeTitleGradient"));
		pantallaLogin.setBounds(0, 0, 834, 461);
		pantallaLogin.setLayout(null);
		
		lblUsuario = new JLabel ("Usuario:");
		lblUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUsuario.setForeground(Color.BLACK);
		lblUsuario.setFont(new Font ("Calibri", Font.PLAIN, 20));
		lblUsuario.setBounds(188, 260, 140, 30);
		pantallaLogin.add(lblUsuario);
		
		lblPass = new JLabel ("Contraseņa:");
		lblPass.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPass.setForeground(Color.BLACK);
		lblPass.setFont(new Font ("Calibri", Font.PLAIN, 20));
		lblPass.setBounds(188, 300, 140, 30);
		pantallaLogin.add(lblPass);
		
		inputUsuario = new JTextField();
		inputUsuario.setBounds(337, 260, 255, 30);
		pantallaLogin.add(inputUsuario);
		inputUsuario.setColumns(10);
		
		inputPass = new JPasswordField();
		inputPass.setColumns(10);
		inputPass.setBounds(337, 300, 255, 30);
		pantallaLogin.add(inputPass);
		
		BufferedImage imgLogin = null;
		try {
			imgLogin = ImageIO.read(new File ("./media/banner-login.png"));
		} catch (IOException err) {
//			System.err.println(err.getMessage());
			JOptionPane.showMessageDialog(null, "Error interno.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		JLabel lblImgLogin = new JLabel(new ImageIcon (imgLogin));
		lblImgLogin.setBounds(62, 7, 724, 242);
		pantallaLogin.add(lblImgLogin);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnEntrar.setForeground(Color.WHITE);
		btnEntrar.setBackground(new Color(0, 149, 189));
		btnEntrar.setBounds(337, 360, 120, 30);
		btnEntrar.addActionListener(eventos.btnEntrarClicked (app, this));
		pantallaLogin.add(btnEntrar);
		
	}
	
	public JPanel get () {
		return this.pantallaLogin;
	}
	
	public JLabel getLabelUsuario () {
		return this.lblUsuario;
	}
	
	public JLabel getLabelContrasena () {
		return this.lblPass;
	}
	
	public String getNombre () {
		return this.inputUsuario.getText();
	}
	
	public String getPass () {
		String res = "";
		char []list = this.inputPass.getPassword();
		for (int i = 0; i < list.length; i++)
			res += list[i];
		
		return res;
	}
}
