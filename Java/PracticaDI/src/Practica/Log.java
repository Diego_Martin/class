package Practica;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import com.toedter.calendar.JCalendar;

public class Log {
	private JPanel pantallaLog;
	private EventManager eventos;
	private JTextArea txtArea;
	private App app;
	private JCalendar calendar;
	
	public Log (App app) {
		
		this.eventos = new EventManager();
		this.app = app;
		pantallaLog = new JPanel ();
		pantallaLog.setBackground (Color.WHITE);
		pantallaLog.setForeground (new Color (255, 255, 255));
		pantallaLog.setBounds (0, 0, 834, 461);
		pantallaLog.setLayout (null);
		
		JPanel panelBuscar = new JPanel();
		panelBuscar.setBackground(Color.WHITE);
		panelBuscar.setBounds(0, 59, 205, 64);
		panelBuscar.setLayout(null);
		pantallaLog.add(panelBuscar);
		
		JLabel lblFecha = new JLabel("Filtrar por fecha");
		lblFecha.setBackground(Color.WHITE);
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblFecha.setBounds(10, 0, 121, 32);
		panelBuscar.add(lblFecha);
		
		JButton btnFiltrar = new JButton("Buscar");
		btnFiltrar.setForeground(Color.WHITE);
		btnFiltrar.setBackground(new Color(102, 102, 255));
		btnFiltrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnFiltrar.setBounds(0, 31, 205, 32);
		btnFiltrar.addActionListener(this.eventos.filtrarLog(this));
		panelBuscar.add(btnFiltrar);
		
		txtArea = new JTextArea ();
		txtArea.setBounds(0, 0, 836, 372);
		txtArea.setLineWrap (true);
		txtArea.setEnabled (true);
		txtArea.setEditable (false);
		txtArea.setForeground (Color.BLACK);
		txtArea.setFont(new Font ("Calibri", Font.PLAIN, 25));
		
		JScrollPane panelScroll = new JScrollPane(txtArea);
		panelScroll.setBounds (206, 59, 628, 402);
		pantallaLog.add (panelScroll);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(-2, 0, 836, 22);
		pantallaLog.add(menuBar);
		
		JMenu btnMenuInfo = new JMenu("...");
		btnMenuInfo.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(btnMenuInfo);
		
		JMenuItem panelMenuAcercaDe = new JMenuItem("Acerca de...");
		panelMenuAcercaDe.addActionListener(this.eventos.showInfo());
		btnMenuInfo.add(panelMenuAcercaDe);
		
		this.calendar = new JCalendar();
		this.calendar.setBounds(0, 123, 205, 153);
		pantallaLog.add(this.calendar);
		
		JPanel panelHeader = new JPanel ();
		panelHeader.setBackground(new Color(30, 144, 255));
		panelHeader.setBounds (0, 22, 834, 38);
		panelHeader.setLayout(null);
		pantallaLog.add(panelHeader);
		
		JLabel lblTitulo = new JLabel ("Log");
		lblTitulo.setBackground(new Color(30, 144, 255));
		lblTitulo.setForeground (Color.WHITE);
		lblTitulo.setFont (new Font ("Calibri", Font.PLAIN, 30));
		lblTitulo.setHorizontalAlignment (SwingConstants.CENTER);
		lblTitulo.setBounds (0, 0, 834, 38);
		panelHeader.add (lblTitulo);
		
		Icon iconoHome = new ImageIcon ("./media/btn-home.png");
		JButton btnHome = new JButton (iconoHome);
		btnHome.setBackground (Color.WHITE);
		btnHome.setBounds (0, 0, 38, 38);
		btnHome.addActionListener (this.eventos.btnToOpcionesClicked (app, app.getUserInfo()));
		panelHeader.add (btnHome);
	}
	
	public JPanel get () {
		return this.pantallaLog;
	}
	
	public JTextArea getTxtArea () {
		return this.txtArea;
	}
	
	public String getFileContent () {
		String res = "", linea = null;
		FileInputStream fs = null;
		try {
			fs = new FileInputStream(new File ("./required/noticias/log.txt"));
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Error interno.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		BufferedReader lector = new BufferedReader(new InputStreamReader (fs));
		try {
			while ((linea = lector.readLine()) != null)
				if (linea.startsWith("+[" + this.app.getUserInfo().getPass() + "]") || linea.startsWith("-[" + this.app.getUserInfo().getPass() + "]"))
					res += linea.substring(this.app.getUserInfo().getPass().length() + 13) + "\n\n";
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error interno.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		return res;
	}
	
	@SuppressWarnings("deprecation")
	private String getFecha () {
		if (!this.calendar.getDate().equals(new Date (0, 0, 0))) {
			DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDateTime now = LocalDateTime.now();
			return formateador.format(now);
		} 
		return null;
	}
	
	public String filtrar () {
		String res = "", linea = null;
		FileInputStream fs = null;
		try {
			fs = new FileInputStream(new File ("./required/noticias/log.txt"));
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Error interno.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		BufferedReader lector = new BufferedReader(new InputStreamReader (fs));
		try {
			while ((linea = lector.readLine()) != null)
				if (linea.startsWith("+[" + this.app.getUserInfo().getPass() + "]") || linea.startsWith("-[" + this.app.getUserInfo().getPass() + "]"))
					if (this.getFecha() != null) {
						if (linea.substring(this.app.getUserInfo().getPass().length() + 3, this.app.getUserInfo().getPass().length() + 10).equals(this.getFecha())) {
							res += linea.substring(this.app.getUserInfo().getPass().length() + 13) + "\n";
						}
					} else
						res += linea.substring(this.app.getUserInfo().getPass().length() + 13) + "\n";
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error interno.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		return res;
	}
	
}
