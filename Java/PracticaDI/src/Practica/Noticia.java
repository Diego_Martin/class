package Practica;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;

public class Noticia {
	private JPanel panelNoticia;
	private JLabel lblNombrePeriodico;
	private JLabel lblTitular;
	private int alto;
	
	public Noticia (String nombre, String titular, int alto) {
		this.alto = alto;
		LineBorder borderPanel = new LineBorder (new Color (255, 255, 255), 2);
		
		this.panelNoticia = new JPanel ();
		this.panelNoticia.setForeground(new Color (255, 255, 255));
		this.panelNoticia.setBounds(0, this.alto, 834, 80);
		this.panelNoticia.setBorder(borderPanel);
		this.panelNoticia.setLayout(null);
		
		this.lblNombrePeriodico = new JLabel(nombre);
		this.lblNombrePeriodico.setFont(new Font("Tahoma", Font.PLAIN, 20));
		this.lblNombrePeriodico.setBounds(10, 11, 746, 32);
		this.panelNoticia.add(this.lblNombrePeriodico);
		
		this.lblTitular = new JLabel(titular);
		this.lblTitular.setBounds(10, 42, 746, 14);
		this.panelNoticia.add(this.lblTitular);
		
		JButton btnGuardar = new JButton("G");
		btnGuardar.setBounds(766, 11, 58, 58);
		this.panelNoticia.add(btnGuardar);
		
	}
	
	public JPanel get () {
		return this.panelNoticia;
	}
	
	public JLabel getLblNombrePeriodico () {
		return this.lblNombrePeriodico;
	}
	
	@Override
	public String toString () {
		return "Noticia [titular=" + this.lblTitular.getText() + ", nombre=" + this.lblNombrePeriodico.getText() + ", alto=" + this.alto + "]";
	}
	
}
