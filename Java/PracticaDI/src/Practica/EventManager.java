package Practica;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class EventManager {
	
	private int porcentajeCargando = 0;
	private int sec = 0;
	private FileManager misArchivos;
	
	public WindowAdapter btnSalirXClicked () {
		return new WindowAdapter () {
			@Override public void windowClosing (WindowEvent evento) {
				String botones[] = {"Aceptar", "Cancelar"};
				int resultado = JOptionPane.showOptionDialog (null, "Est�s seguro de que quieres salir?", "Atenci�n.", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, botones, botones [1]);
				if (resultado == JOptionPane.YES_OPTION)
					System.exit (0);
			}
		};
	}
	
	public ActionListener exeCarga (Carga pantallaCarga) {
		return new ActionListener () {
			@Override public void actionPerformed (ActionEvent evento) {
				porcentajeCargando ++;
				pantallaCarga.getLabel().setText("Cargando... " + porcentajeCargando + "%");
				pantallaCarga.getProgressBar().setValue(porcentajeCargando);
				if (porcentajeCargando == 30) {
					misArchivos = new FileManager();
					misArchivos.comprobarInfo();
				}
				if (porcentajeCargando == 100)
					((Timer) evento.getSource()).stop();
				
			}
		};
	}
	
	public ActionListener exeCargaaLogin (Ventana ventana, App app) {
		return new ActionListener () {
			@Override public void actionPerformed (ActionEvent evento) {
				if (sec == 5) {
					((Timer) evento.getSource()).stop();
					ventana.get().setEnabled(true);
					ventana.puedesCerrar();
					app.initializeLogin ();
				}
				sec ++;
			}
		};
	}
	
	private boolean tieneCategoriasYTitulares (UserInfo user) {
		if (user.getDatos().size() < 1)
			return false;
		
		for (int i = 0; i < user.getDatos().size(); i++)
			if (user.getDatos().get(i).getFuentes().size() < 1)
				return false;
		
		return true;
	}
	
	public ActionListener btnEntrarClicked (App app, Login login) {
		return new ActionListener () {
			@Override public void actionPerformed(ActionEvent e) {
				misArchivos = new FileManager();
				if (misArchivos.infoExists (login.getNombre(), login.getPass())) {
					UserInfo user = misArchivos.getInfoByUser(login.getNombre(), login.getPass(), app);
					if (tieneCategoriasYTitulares (user)) {
						app.initializeOpciones ();
						app.setInfoSesion (user);
					} else {
						app.initializeConfig ();
					}
					
				} else
					JOptionPane.showMessageDialog (null, "Ese usuario no existe.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		};
	}
	
	public ActionListener btnSalirClicked (App app) {
		return new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				String botones[] = {"Aceptar", "Cancelar"};
				if (JOptionPane.showOptionDialog(null, "Est�s seguro de que quieres salir?", "Atenci�n.", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, botones, botones [1]) == JOptionPane.YES_OPTION)
					System.exit (0);
			}
		};
	}
	
	public ActionListener btnOpcionesToLogClicked (App app) {
		return new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				app.initializeLog();
			}
		};
	}
	
	public ActionListener btnOpcionesToConfigClicked (App app, Configuracion config) {
		return new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				app.initializeConfig();
				config.rellenar();
			}
		};
	}
	
	public ActionListener btnOpcionesToNoticiasClicked (App app, Noticias opts) {
		return new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				app.initializeNoticias();
				opts.getFuentes ();
				opts.addNoticias();
			}
		};
	}
	
	public ActionListener btnToOpcionesClicked (App app, UserInfo datos) {
		return new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				// if (this.tieneCategoriasYTitulares ()) {
				// 	JOptionPane.showMessageDialog (null, "No hay categorias o titulares seleccionados");
				// 	return;
				// }
				app.initializeOpciones();
			}
		};
	}
	
	public ActionListener btnGuardarConfigClicked (App app, Configuracion config) {
		return new ActionListener () {
			@Override public void actionPerformed (ActionEvent arg0) {
				// if (this.tieneCategoriasYTitulares ()) {
				// 	JOptionPane.showMessageDialog (null, "No hay categorias o titulares seleccionados");
				// 	return;
				// }
				
				for (int i = 0; i < app.getUsersInfo ().size (); i++)
					if (app.getUsersInfo ().get (i).getPass ().equals (config.getNewConfig().getPass ()))
						app.getUsersInfo ().set (i, config.getNewConfig());
				
				app.setInfoSesion (config.getNewConfig());
				app.setHasGuardado (true);
				app.guardar ();
				config.disableBtnGuardar ();
			}
		};
	}
	
	public ActionListener btnGuardarNoticiasClicked (App app, String [] noticias) {
		return new ActionListener () {
			@Override public void actionPerformed(ActionEvent arg0) {
				FileManager archivos = new FileManager ();
				archivos.escribirLog(noticias, app);
			}
		};
	}
	
	public ActionListener checkCategoriaClicked (JCheckBox estado, JCheckBox[] opt) {
		return new ActionListener () {
			@Override public void actionPerformed (ActionEvent arg0) {
				for (int i = 0; i < opt.length; i++)
					opt[i].setEnabled (estado.isSelected());
				
				for (int i = 0; i < opt.length; i++)
					opt[i].setSelected (false);
			}
		};
	}
	
	public ActionListener showInfo () {
		return new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "Desrrollador: Diego Martin.\n\nCurso: 2� DAM.", "Info", JOptionPane.INFORMATION_MESSAGE);
			}
		};
	}
	
	public ActionListener filtrarLog (Log log) {
		return new ActionListener() {
			@Override public void actionPerformed(ActionEvent arg0) {
				log.filtrar();
			}
		};
	}
}
