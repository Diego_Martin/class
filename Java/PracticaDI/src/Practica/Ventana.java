package Practica;

import java.awt.Toolkit;

import javax.swing.JFrame;

public class Ventana {
	private JFrame ventana;
	private EventManager eventos;
	
	public Ventana () {
		this.ventana = new JFrame();
		this.eventos = new EventManager();
		this.ventana.setIconImage(Toolkit.getDefaultToolkit().getImage("./media/icon.png"));
		this.ventana.setBounds(100, 100, 850, 500);
		this.ventana.setLocationRelativeTo(null);
		this.ventana.setResizable(false);
		this.ventana.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.ventana.getContentPane().setLayout(null);
		this.ventana.setEnabled(false);
	}
	
	public JFrame get () {
		return this.ventana;
	}
	
	public void refresh () {
		this.ventana.invalidate();
		this.ventana.validate();
	}
	
	public void puedesCerrar () {
		this.ventana.addWindowListener(this.eventos.btnSalirXClicked());
	}
	
}
