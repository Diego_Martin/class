package Practica;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/* Terminado */
public class Carga {
	private JPanel pantallaCarga;
	private JLabel LabelCargando;
	private JProgressBar progressBar;
	
	public Carga () {
		pantallaCarga = new JPanel();
		pantallaCarga.setBackground(Color.WHITE);
		pantallaCarga.setBounds(0, 0, 834, 461);
		pantallaCarga.setLayout(null);
		
		LabelCargando = new JLabel("Cargando... 0%");
		LabelCargando.setForeground(Color.WHITE);
		LabelCargando.setFont(new Font("Tahoma", Font.PLAIN, 20));
		LabelCargando.setBounds(340, 403, 181, 33);
		pantallaCarga.add(LabelCargando);
		
		progressBar = new JProgressBar();
		progressBar.setForeground(new Color(25, 25, 112));
		progressBar.setBounds(0, 447, 834, 14);
		pantallaCarga.add(progressBar);
		
		BufferedImage imgCarga = null;
		try {
			imgCarga = ImageIO.read(new File ("./media/carga.png"));
		} catch (IOException err) {
//			System.err.println(err.getMessage());
			JOptionPane.showMessageDialog(null, "Error interno.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		JLabel lblImagenCarga = new JLabel (new ImageIcon (imgCarga));
		lblImagenCarga.setBounds(0, 0, 834, 461);
		pantallaCarga.add(lblImagenCarga);
	}
	
	public JPanel get () {
		return this.pantallaCarga;
	}
	
	public JLabel getLabel () {
		return this.LabelCargando;
	}
	
	public JProgressBar getProgressBar () {
		return this.progressBar;
	}
}
