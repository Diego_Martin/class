package Practica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class FileManager {
	
	private File log;
	private File users;
	private File config;
	
	
	public FileManager () {
		this.log = new File ("./required/noticias/log.txt");
		this.users = new File ("./required/usuarios/users.txt");
		this.config =  new File ("./required/config/config.txt");
	}
	
	public void comprobarInfo () {
		BufferedReader readerUsers = null;
		BufferedReader readerConfig = null;
		try {
			readerUsers = new BufferedReader(new FileReader(this.users));
			readerConfig = new BufferedReader(new FileReader(this.config));
		} catch (FileNotFoundException err) {
			JOptionPane.showMessageDialog(null, "Error interno. No se pudo abrir un lector de archivos.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		int usuariosEnUsers = 0;
		int usuariosEnConfig = 0;
		try {
			while (readerUsers.readLine() != null)
				usuariosEnUsers++;
			
			readerUsers.close();
		} catch (IOException err) {
			JOptionPane.showMessageDialog(null, "Error interno. No se pudo leer el archivo de usuarios.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		try {
			String str = "";
			while (true) { // No pongo condici�n (seria (str != null)) porque hasta la �ltima vuelta no encuentra el null, pero ya est� dentro de las llaves, por lo tanto a�ade un null al final de 'str'  
				str = readerConfig.readLine();
				if (str == null)
					break; // Asi que salgo aqu�, significar�a que ha llegado al final
				if (str.contains(";-;"))
					usuariosEnConfig++;
			}
			readerConfig.close();
		} catch (IOException err) {
			JOptionPane.showMessageDialog(null, "Error interno. No se pudo leer el archivo de configuraci�n.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		String linea = null;
		String indices[] = new String [usuariosEnUsers];
		int contrasenasIguales = 0;
		for (int i = 0; i < usuariosEnUsers; i++) {
			try {
				linea = Files.readAllLines(Paths.get("./required/usuarios/users.txt")).get(i);
				String pass = linea.split(";-;")[1];
				indices [i] = linea;
				for (String elemento : indices) {
					if (elemento != null)
						if (elemento.equals(pass))
							contrasenasIguales ++;
				}
				
				if (contrasenasIguales > 1) {
					JOptionPane.showMessageDialog(null, "Hay dos usuarios con contrase�as iguales.", "Error", JOptionPane.ERROR_MESSAGE);
					System.exit (1);
				}
			} catch (IOException err) {
				JOptionPane.showMessageDialog(null, "Error interno. 99", "Error", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
			contrasenasIguales = 0;
		}
		if (!log.exists()) {
			JOptionPane.showMessageDialog(null, "No se pudo cargar el log de noticias.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit (1);
		}
		if (!users.exists()) {
			JOptionPane.showMessageDialog(null, "No se pudo cargar el archivo de usuarios.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit (1);
		}
		if (!config.exists()) {
			JOptionPane.showMessageDialog(null, "No se pudo cargar el archivo de configuraci�n", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit (1);
		}
		if (usuariosEnUsers != usuariosEnConfig) {
			JOptionPane.showMessageDialog(null, "No hay el mismo n�mero de archivos de configuraci�n que usuarios.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit (1);
		}
	}
	
	public boolean infoExists (String user, String pass) {
		String info = user + ";-;" + pass;
		BufferedReader lector = null;
		try {
			lector = new BufferedReader(new InputStreamReader(new FileInputStream(this.users.getPath())));
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "El usuario no existe o no se ha escrito bien el nombre o contrase�a.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		String linea = "";
		try {
			while ((linea = lector.readLine()) != null)
				if (linea.equals(info))
					return true;
			
			return false;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "BufferedReader no pudo leer el archivo.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit (1);
		}
		return false;
	}
	
	public ArrayList<UserInfo> getUsersInfoFromFile () { // Funciona
		
		ArrayList<UserInfo> ret = new ArrayList<UserInfo>();
		
		BufferedReader lector = null;
		
		try {
			lector = new BufferedReader(new InputStreamReader(new FileInputStream("./required/config/config.txt")));
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "El usuario no existe.", "Error", JOptionPane.ERROR_MESSAGE);
		}
		
		String linea = "";
		
		try {
			while (true) {
				linea = lector.readLine();
				if (linea == null)
					break;
				
				String nombreUser = linea.split(";-;")[0];
				String passUser = linea.split(";-;")[1];
				int numeroDeCategorias = Integer.parseInt(lector.readLine());
				ArrayList<Categoria> categorias = new ArrayList<Categoria>();
				for (int i = 0; i < numeroDeCategorias; i++) {
					String nombreCategoria = lector.readLine().substring(3);
					int numeroDeNoticias = Integer.parseInt(lector.readLine());
					ArrayList<String> noticias = new ArrayList<String>();
					for (int j = 0; j < numeroDeNoticias; j++)
						noticias.add (lector.readLine());
					
					Categoria Ctemp = new Categoria ();
					Ctemp.setNombre(nombreCategoria);
					Ctemp.setFuentes(noticias);
					categorias.add (Ctemp);
				}
				UserInfo Itemp = new UserInfo();
				Itemp.setNombre(nombreUser);
				Itemp.setPass(passUser);
				Itemp.setDatos(categorias);
				ret.add(Itemp);
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "BufferedReader no pudo leer el archivo.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		return ret;
	}
	
	public UserInfo getInfoByUser (String user, String pass, App app) {

		app.setUsersInfo(this.getUsersInfoFromFile()); // Rellena App.usersInfo
		ArrayList<UserInfo> allInfo = app.getUsersInfo();
		
		for (int i = 0; i < allInfo.size(); i++)
			if (allInfo.get(i).getPass().equals(pass))
				return allInfo.get(i);
		
		return null;
	}
	
	public void guardar (ArrayList<UserInfo> datos, UserInfo user) {
		FileWriter fr = null;
		String frase = "";
		for (int i = 0; i < datos.size (); i++)
			if (datos.get(i).getPass().equals(user.getPass()))
				datos.set(i, user);
		
		for (int i = 0; i < datos.size (); i++)
			frase += datos.get (i).toFile ();
		
		try {
			fr = new FileWriter ("./required/config/config.txt", false);
			fr.write (frase);
			fr.close ();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error interno. 231", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}
	
	public void escribirLog (String []texto, App app) {
		DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDateTime now = LocalDateTime.now();
		String frase = "";
		for (int i = 0; i < texto.length; i++)
			if (texto[i] != null)
				frase += "+[" + app.getUserInfo().getPass() + "]" + formateador.format(now) + texto[i] + "\n";
		
		try {
			FileWriter fr = new FileWriter ("./required/noticias/log.txt", true);
			fr.write (frase);
			fr.close ();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error interno. 257", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}
}
