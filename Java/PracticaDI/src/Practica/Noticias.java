package Practica;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
//import java.util.Arrays;
import java.util.ArrayList;
//import java.util.Arrays;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

public class Noticias {
	
	private JPanel pantallaNoticias;
	private EventManager eventos;
	private App app;
	private SearchEngine buscador;
	private ArrayList<String> fuentesUser;
	private String infoNoticias [][];
	private Noticia noticias[];
	private JPanel panelInterno;
	private JScrollPane panelScroll;
	private JButton btnGuardar;
	
	public Noticias (App app) {
		this.buscador = new SearchEngine();
		this.fuentesUser = new ArrayList<String>();
		this.app = app;
		this.eventos = new EventManager ();
		this.pantallaNoticias = new JPanel ();
		
		this.infoNoticias = new String [][] {
				{"https://www.sport.es/es/", "h2.title a", "Sport"}, 
				{"https://as.com/", "h2.title > a", "As"},
				{"https://www.marca.com/", "h2.mod-title > a", "Marca"},
				{"https://www.elperiodico.com/es/deportes/", "h2.title.fs2 > a", "El periodico/deportes"}, 
				{"https://www.economiadigital.es/", "div.title > h2 > a", "Economia digital"},
				{"https://www.eleconomista.es/", "h2.h1 > a > center", "El economista"},
				{"https://www.expansion.com/", "a.ue-c-cover-content__link", "Expansion"},
				{"https://www.elmundo.es/espana.html", "a.ue-c-cover-content__link > h2.ue-c-cover-content__headline", "El Mundo"},
				{"https://www.larazon.es/espana/", "h3.card__headline > a", "La Razon"}, 
				{"https://www.abc.es/espana/", "h3.titular.lead-title > a", "ABC"},
				{"https://www.hoy.es/nacional/", "h2.voc-title > a", "Hoy/nacional"},
				{"https://www.elperiodico.com/es/internacional/", "h2.title.fs2 > a", "El periodico/internacional"},
				{"https://www.hoy.es/internacional/", "h2.voc-title > a", "Hoy/internacional"},
				{"https://www.europapress.es/internacional/", "h2.articulo-titulo.titulo-principal > a", "Europapress"},
				{"https://www.libertaddigital.com/internacional/", "article.noticia.centrado.conimagen > a > h2 > span", "Libertad Digital"}
		};
		this.noticias = new Noticia [infoNoticias.length];
		pantallaNoticias.setForeground(new Color(255, 255, 255));
		pantallaNoticias.setBounds(0, 0, 834, 461);
		pantallaNoticias.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(-2, 0, 836, 22);
		pantallaNoticias.add(menuBar);
		
		JMenu btnMenuInfo = new JMenu("...");
		btnMenuInfo.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(btnMenuInfo);
		
		JMenuItem panelMenuAcercaDe = new JMenuItem("Acerca de...");
		panelMenuAcercaDe.addActionListener(this.eventos.showInfo());
		btnMenuInfo.add(panelMenuAcercaDe);
		
		JPanel panelHeader = new JPanel();
		panelHeader.setBackground(new Color(255, 255, 255));
		panelHeader.setBounds(0, 20, 834, 45);
		panelHeader.setLayout(null);
		
		Icon iconoHome = new ImageIcon ("./media/btn-home.png");
		pantallaNoticias.add(panelHeader);
		JButton btnHome = new JButton (iconoHome);
		btnHome.setBounds(0, 0, 45, 45);
		panelHeader.add(btnHome);
		btnHome.addActionListener(this.eventos.btnToOpcionesClicked (this.app, this.getNewDatos()));
		btnHome.setBackground (Color.WHITE);
		btnHome.setOpaque(true);
		
		JLabel lblTitulo = new JLabel ("Noticias");
		lblTitulo.setBounds(43, 0, 746, 45);
		panelHeader.add(lblTitulo);
		lblTitulo.setForeground (Color.WHITE);
		lblTitulo.setFont (new Font ("Calibri", Font.PLAIN, 30));
		lblTitulo.setHorizontalAlignment (SwingConstants.CENTER);
		lblTitulo.setBackground (new Color (30, 144, 255));
		lblTitulo.setOpaque(true);
		
		Icon iconoGuardar = new ImageIcon ("./media/btn-guardar.png");
		this.btnGuardar = new JButton(iconoGuardar);
		this.btnGuardar.setBounds(789, 0, 45, 45);
		this.btnGuardar.setBackground (Color.WHITE);
		this.btnGuardar.setOpaque(true);
		panelHeader.add(this.btnGuardar);
		
	}
	
	public JPanel get () {
		return this.pantallaNoticias;
	}
	
	public UserInfo getNewDatos () {
		return this.app.getUserInfo();
	}
	
	public void getFuentes () {
		for (int i = 0; i < this.app.getUserInfo().getDatos().size(); i++)
			for (int j = 0; j < this.app.getUserInfo().getDatos().get(i).getFuentes().size(); j++)
				this.fuentesUser.add(this.app.getUserInfo().getDatos().get(i).getFuentes().get(j));
	}
	
	public void addNoticias () {
		this.fuentesUser.clear();
		this.getFuentes();
		String [] titularesUser = new String [this.fuentesUser.size()];
		int alto = 0;
		for (int i = 0; i < this.infoNoticias.length; i++) 
			if (this.fuentesUser.contains(this.infoNoticias[i][2])) {
				this.noticias[i] = new Noticia(this.infoNoticias[i][2], this.buscador.buscar(this.infoNoticias[i][0], this.infoNoticias[i][1]), alto);
				alto += 80;
			}
		
		int cont = 0;
		for (int i = 0; i < this.infoNoticias.length; i++) 
			if (this.fuentesUser.contains(this.infoNoticias[i][2])) {
				titularesUser[cont] = this.buscador.buscar(this.infoNoticias[i][0], this.infoNoticias[i][1]);
				cont++;
			}


		this.panelInterno = new JPanel ();
		this.panelInterno.setBackground(Color.WHITE);
		this.panelInterno.setPreferredSize(new Dimension (834, alto));
		this.panelInterno.setLayout(null);
		
		for (int i = 0; i < this.noticias.length; i++)
			if (this.fuentesUser.contains(this.infoNoticias[i][2]))
				this.panelInterno.add(this.noticias[i].get());

		
		this.panelScroll = new JScrollPane (panelInterno, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.panelScroll.setBounds(45, 89, 744, 329);
		this.pantallaNoticias.add(this.panelScroll);
		if (this.btnGuardar.getActionListeners().length < 1)
			this.btnGuardar.addActionListener(this.eventos.btnGuardarNoticiasClicked(app, titularesUser));
	}
}
