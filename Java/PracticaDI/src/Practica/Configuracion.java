package Practica;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;

public class Configuracion {
	
	private JPanel pantallaConfiguracion;
	private App app;
	private EventManager eventos;
	private JCheckBox chckbxSport;
	private JCheckBox chckbxAs;
	private JCheckBox chckbxMarca;
	private JCheckBox chckbxElPeriodicoD;
	private JCheckBox chckbxEconomiaDigital;
	private JCheckBox chckbxElEconomista;
	private JCheckBox chckbxExpansion;
	private JCheckBox chckbxElPeriodicoE;
	private JCheckBox chckbxElMundo;
	private JCheckBox chckbxLaRazon;
	private JCheckBox chckbxAbc;
	private JCheckBox chckbxHoy;
	private JCheckBox chckbxElPeriodicoI;
	private JCheckBox chckbxHoyinternacional;
	private JCheckBox chckbxEuropapress;
	private JCheckBox chckbxLibertadDigital;
	private JCheckBox chckbxEconomia;
	private JCheckBox chckbxDeportes;
	private JCheckBox chckbxInternacional;
	private JCheckBox chckbxNacional;
	private JButton btnGuardar;
	
	public Configuracion (App app) {
		this.pantallaConfiguracion = new JPanel ();
		this.app = app;
		this.eventos = new EventManager ();

		this.chckbxSport = new JCheckBox("Sport"); // URL: https://www.sport.es/es/ Selector: h2.title a (First)
		this.chckbxAs = new JCheckBox("As"); // URL: https://as.com/ Selector: div#contenedor_portada div.area-actualidad.row.flexible.separador div.col-md-8.col-sm-12.col-xs-12 div.pntc.pntc-v.pntc-tit-figure article.cf div.pntc-content div.pntc-txt p.txt (First) 
		this.chckbxMarca = new JCheckBox("Marca"); // URL: https://www.marca.com/ Selector: h2.mod-title > a (First)
		this.chckbxElPeriodicoD = new JCheckBox("El periodico/deportes"); // URL: https://www.elperiodico.com/es/deportes/ Selector: h2.title.fs2 > a (First)
		this.chckbxEconomiaDigital = new JCheckBox("Economia digital"); // URL: https://www.economiadigital.es/ Selector: div.title > h2 > a (First)
		this.chckbxElEconomista = new JCheckBox("El economista"); // URL: https://www.eleconomista.es/ Selector: h2.h1 > a > center (First)
		this.chckbxExpansion = new JCheckBox("Expansion"); // URL: https://www.expansion.com/ Selector: a.ue-c-cover-content__link (First)
		this.chckbxElPeriodicoE = new JCheckBox("El periodico/economia"); // URL: https://www.elperiodico.com/es/economia/ Selector: h2.title.noMediaSignature > a
		this.chckbxElMundo = new JCheckBox("El Mundo"); // URL: https://www.elmundo.es/espana.html Selector: a.ue-c-cover-content__link > h2.ue-c-cover-content__headline
		this.chckbxLaRazon = new JCheckBox("La Razon"); // URL: https://www.larazon.es/espana/ Selector: h3.card__headline > a (First)
		this.chckbxAbc = new JCheckBox("ABC"); // URL: https://www.abc.es/espana/ Selector: h3.titular.lead-title > a (First)
		this.chckbxHoy = new JCheckBox("Hoy/nacional"); // URL: https://www.hoy.es/nacional/ Selector: h2.voc-title > a (First)
		this.chckbxElPeriodicoI = new JCheckBox("El periodico/internacional"); // URL: https://www.elperiodico.com/es/internacional/ Selector: h2.title.fs2 > a (First)
		this.chckbxHoyinternacional = new JCheckBox("Hoy/internacional"); // URL: https://www.hoy.es/internacional/ Selector: h2.voc-title > a (First)
		this.chckbxEuropapress = new JCheckBox("Europapress"); // URL: https://www.europapress.es/internacional/ Selector: h2.articulo-titulo.titulo-principal > a (First)
		this.chckbxLibertadDigital = new JCheckBox("Libertad Digital"); // URL: https://www.libertaddigital.com/internacional/ Selector: article.noticia.centrado.conimagen > a > h2 > span (First)
		this.chckbxEconomia = new JCheckBox("");
		this.chckbxDeportes = new JCheckBox("");
		this.chckbxInternacional = new JCheckBox("");
		this.chckbxNacional = new JCheckBox("");
				
		pantallaConfiguracion.setForeground(new Color(255, 255, 255));
		pantallaConfiguracion.setBounds(0, 0, 834, 461);
		pantallaConfiguracion.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(-2, 0, 836, 22);
		pantallaConfiguracion.add(menuBar);
		
		JMenu btnMenuInfo = new JMenu("...");
		btnMenuInfo.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(btnMenuInfo);
		
		JMenuItem panelMenuAcercaDe = new JMenuItem("Acerca de...");
		panelMenuAcercaDe.addActionListener(this.eventos.showInfo());
		btnMenuInfo.add(panelMenuAcercaDe);
		
		JPanel panelHeader = new JPanel();
		panelHeader.setBackground(new Color(255, 255, 255));
		panelHeader.setBounds(0, 20, 834, 45);
		panelHeader.setLayout(null);
		
		Icon iconoHome = new ImageIcon ("./media/btn-home.png");
		JButton btnHome = new JButton (iconoHome);
		btnHome.addActionListener(this.eventos.btnToOpcionesClicked (this.app, this.getNewConfig()));
		btnHome.setBounds(0, 0, 45, 45);
		btnHome.setBackground (Color.WHITE);
		btnHome.setOpaque(true);
		panelHeader.add(btnHome);
		
		Icon iconoGuardar = new ImageIcon ("./media/btn-guardar.png");
		this.btnGuardar = new JButton (iconoGuardar);
		this.btnGuardar.addActionListener (this.eventos.btnGuardarConfigClicked (this.app, this));
		this.btnGuardar.setBounds(789, 0, 45, 45);
		this.btnGuardar.setBackground (Color.WHITE);
		this.btnGuardar.setOpaque(true);
		panelHeader.add(this.btnGuardar);
		
		JLabel lblTitulo = new JLabel ("Configuración");
		lblTitulo.setBounds(44, 0, 746, 45);
		lblTitulo.setForeground (Color.WHITE);
		lblTitulo.setFont (new Font ("Calibri", Font.PLAIN, 30));
		lblTitulo.setHorizontalAlignment (SwingConstants.CENTER);
		lblTitulo.setBackground (new Color (30, 144, 255));
		lblTitulo.setOpaque(true);
		panelHeader.add(lblTitulo);
		pantallaConfiguracion.add(panelHeader);
		
		JLabel lblInternacional = new JLabel("Internacional:");
		lblInternacional.setHorizontalAlignment(SwingConstants.RIGHT);
		lblInternacional.setFont(new Font("Calibri", Font.PLAIN, 18));
		lblInternacional.setBounds(25, 261, 118, 18);
		pantallaConfiguracion.add(lblInternacional);
		
		JLabel lblNacional = new JLabel("Nacional:");
		lblNacional.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNacional.setFont(new Font("Calibri", Font.PLAIN, 18));
		lblNacional.setBounds(25, 209, 118, 18);
		pantallaConfiguracion.add(lblNacional);
		
		JLabel lblDeportes = new JLabel("Deportes:");
		lblDeportes.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDeportes.setFont(new Font("Calibri", Font.PLAIN, 18));
		lblDeportes.setBounds(25, 317, 118, 18);
		pantallaConfiguracion.add(lblDeportes);
		
		JLabel lblEconomia = new JLabel("Econom\u00EDa:");
		lblEconomia.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEconomia.setFont(new Font("Calibri", Font.PLAIN, 18));
		lblEconomia.setBounds(25, 369, 118, 23);
		pantallaConfiguracion.add(lblEconomia);
		
		this.chckbxSport.setBounds(217, 314, 97, 23);
		pantallaConfiguracion.add(chckbxSport);
		
		this.chckbxAs.setBounds(351, 314, 97, 23);
		pantallaConfiguracion.add(chckbxAs);
		
		this.chckbxMarca.setBounds(486, 314, 97, 23);
		pantallaConfiguracion.add(chckbxMarca);
		
		this.chckbxElPeriodicoD.setBounds(608, 314, 127, 23);
		pantallaConfiguracion.add(chckbxElPeriodicoD);
		
		this.chckbxEconomiaDigital.setBounds(217, 368, 112, 23);
		pantallaConfiguracion.add(chckbxEconomiaDigital);
		
		this.chckbxElEconomista.setBounds(351, 368, 112, 23);
		pantallaConfiguracion.add(chckbxElEconomista);
		
		this.chckbxExpansion.setBounds(486, 368, 97, 23);
		pantallaConfiguracion.add(chckbxExpansion);
		
		this.chckbxElPeriodicoE.setBounds(608, 368, 134, 23);
		pantallaConfiguracion.add(chckbxElPeriodicoE);
		
		this.chckbxElMundo.setBounds(217, 206, 97, 23);
		pantallaConfiguracion.add(chckbxElMundo);
		
		this.chckbxLaRazon.setBounds(351, 206, 97, 23);
		pantallaConfiguracion.add(chckbxLaRazon);
		
		this.chckbxAbc.setBounds(486, 206, 97, 23);
		pantallaConfiguracion.add(chckbxAbc);
		
		this.chckbxHoy.setBounds(608, 206, 97, 23);
		pantallaConfiguracion.add(chckbxHoy);
		
		this.chckbxElPeriodicoI.setBounds(608, 258, 153, 23);
		pantallaConfiguracion.add(chckbxElPeriodicoI);
		
		this.chckbxHoyinternacional.setBounds(486, 258, 118, 23);
		pantallaConfiguracion.add(chckbxHoyinternacional);
		
		this.chckbxEuropapress.setBounds(351, 258, 118, 23);
		pantallaConfiguracion.add(chckbxEuropapress);
		
		this.chckbxLibertadDigital.setBounds(217, 258, 118, 23);
		pantallaConfiguracion.add(chckbxLibertadDigital);
		
		this.chckbxEconomia.setHorizontalAlignment(SwingConstants.CENTER);
		this.chckbxEconomia.setBounds(149, 369, 28, 23);
		this.chckbxEconomia.addActionListener(this.eventos.checkCategoriaClicked(chckbxEconomia, new JCheckBox[] {chckbxEconomiaDigital, chckbxElEconomista, chckbxExpansion, chckbxElPeriodicoE}));
		pantallaConfiguracion.add(chckbxEconomia);
		
		this.chckbxDeportes.setHorizontalAlignment(SwingConstants.CENTER);
		this.chckbxDeportes.setBounds(149, 314, 28, 23);
		this.chckbxDeportes.addActionListener(this.eventos.checkCategoriaClicked(chckbxDeportes, new JCheckBox[] {chckbxSport, chckbxAs, chckbxMarca, chckbxElPeriodicoD}));
		pantallaConfiguracion.add(chckbxDeportes);
		
		this.chckbxInternacional.setHorizontalAlignment(SwingConstants.CENTER);
		this.chckbxInternacional.setBounds(149, 258, 28, 23);
		this.chckbxInternacional.addActionListener(this.eventos.checkCategoriaClicked(chckbxInternacional, new JCheckBox[] {chckbxElPeriodicoI, chckbxHoyinternacional, chckbxEuropapress, chckbxLibertadDigital}));
		pantallaConfiguracion.add(chckbxInternacional);
		
		this.chckbxNacional.setHorizontalAlignment(SwingConstants.CENTER);
		this.chckbxNacional.setBounds(149, 206, 28, 23);
		this.chckbxNacional.addActionListener(this.eventos.checkCategoriaClicked(chckbxNacional, new JCheckBox[] {chckbxElMundo, chckbxLaRazon, chckbxAbc, chckbxHoy}));
		pantallaConfiguracion.add(chckbxNacional);
		
		BufferedImage img = null;
		try {
		    img = ImageIO.read(new File("./media/settings.png"));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error interno. 205.", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		Image dimg = img.getScaledInstance(161, 123, Image.SCALE_SMOOTH);
		JLabel lblImg = new JLabel(new ImageIcon (dimg));
		lblImg.setBackground(new Color(240, 240, 240));
		lblImg.setBounds(663, 62, 153, 153);
		pantallaConfiguracion.add(lblImg);
		
		JLabel lblImg2 = new JLabel(new ImageIcon (dimg));
		lblImg2.setBackground(new Color(240, 240, 240));
		lblImg2.setBounds(25, 62, 153, 153);
		pantallaConfiguracion.add(lblImg2);
		
	}
	
	public JPanel get () {
		return this.pantallaConfiguracion;
	}
	
	public UserInfo getNewConfig () {
		UserInfo res = new UserInfo();
		res.setNombre(this.app.getUserInfo().getNombre());
		res.setPass(this.app.getUserInfo().getPass());
		
		if (this.chckbxDeportes.isSelected()) {
			Categoria temp = new Categoria ();
			temp.setNombre("Deportes");
			if (this.chckbxAs.isSelected())
				temp.getFuentes().add(this.chckbxAs.getText());
			
			if (this.chckbxSport.isSelected())
				temp.getFuentes().add(this.chckbxSport.getText());
			
			if (this.chckbxMarca.isSelected())
				temp.getFuentes().add(this.chckbxMarca.getText());
			
			if (this.chckbxElPeriodicoD.isSelected())
				temp.getFuentes().add(this.chckbxElPeriodicoD.getText());
			
			res.getDatos().add(temp);
		}
		
		if (this.chckbxEconomia.isSelected()) {
			Categoria temp = new Categoria ();
			temp.setNombre("Economia");
			if (this.chckbxEconomiaDigital.isSelected())
				temp.getFuentes().add(this.chckbxEconomiaDigital.getText());
			
			if (this.chckbxElEconomista.isSelected())
				temp.getFuentes().add(this.chckbxElEconomista.getText());
			
			if (this.chckbxExpansion.isSelected())
				temp.getFuentes().add(this.chckbxExpansion.getText());
			
			if (this.chckbxElPeriodicoE.isSelected())
				temp.getFuentes().add(this.chckbxElPeriodicoE.getText());
			
			res.getDatos().add(temp);
		}
		
		if (this.chckbxInternacional.isSelected()) {
			Categoria temp = new Categoria ();
			temp.setNombre("Internacional");
			if (this.chckbxElPeriodicoI.isSelected())
				temp.getFuentes().add(this.chckbxElPeriodicoI.getText());
			
			if (this.chckbxHoyinternacional.isSelected())
				temp.getFuentes().add(this.chckbxHoyinternacional.getText());
			
			if (this.chckbxEuropapress.isSelected())
				temp.getFuentes().add(this.chckbxEuropapress.getText());
			
			if (this.chckbxLibertadDigital.isSelected())
				temp.getFuentes().add(this.chckbxLibertadDigital.getText());
			
			res.getDatos().add(temp);
		}
		
		if (this.chckbxNacional.isSelected()) {
			Categoria temp = new Categoria ();
			temp.setNombre("Nacional");
			if (this.chckbxElMundo.isSelected())
				temp.getFuentes().add(this.chckbxElMundo.getText());
			
			if (this.chckbxLaRazon.isSelected())
				temp.getFuentes().add(this.chckbxLaRazon.getText());
			
			if (this.chckbxAbc.isSelected())
				temp.getFuentes().add(this.chckbxAbc.getText());
			
			if (this.chckbxHoy.isSelected())
				temp.getFuentes().add(this.chckbxHoy.getText());
			
			res.getDatos().add(temp);
		}
		
		return res;
	}
	
	public void rellenar () {
		UserInfo info = this.app.getUserInfo();
		
		for (int i = 0; i < info.getDatos().size(); i++) {
			if (info.getDatos().get(i).getNombre().equals(this.chckbxDeportes.getText())) {
				this.chckbxDeportes.setSelected(true);
				JCheckBox opts[] = {this.chckbxSport, this.chckbxAs, this.chckbxMarca, this.chckbxElPeriodicoD};
				for (int j = 0; j < info.getDatos().get(i).getFuentes().size(); j++)
					for (int k = 0; k < opts.length; k++)
						if (info.getDatos().get(i).getFuentes().get(j).equals(opts[k].getText()))
							opts[k].setSelected(true);
			}
			
			if (info.getDatos().get(i).getNombre().equals(this.chckbxNacional.getText())) {
				this.chckbxNacional.setSelected(true);
				JCheckBox opts[] = {this.chckbxElMundo, this.chckbxLaRazon, this.chckbxAbc, this.chckbxHoy};
				for (int j = 0; j < info.getDatos().get(i).getFuentes().size(); j++)
					for (int k = 0; k < opts.length; k++)
						if (info.getDatos().get(i).getFuentes().get(j).equals(opts[k].getText()))
							opts[k].setSelected(true);
			}

			if (info.getDatos().get(i).getNombre().equals(this.chckbxInternacional.getText())) {
				this.chckbxInternacional.setSelected(true);
				JCheckBox opts[] = {this.chckbxElPeriodicoI, this.chckbxHoyinternacional, this.chckbxEuropapress, this.chckbxLibertadDigital};
				for (int j = 0; j < info.getDatos().get(i).getFuentes().size(); j++)
					for (int k = 0; k < opts.length; k++)
						if (info.getDatos().get(i).getFuentes().get(j).equals(opts[k].getText()))
							opts[k].setSelected(true);
			}

			if (info.getDatos().get(i).getNombre().equals(this.chckbxEconomia.getText())) {
				this.chckbxEconomia.setSelected(true);
				JCheckBox opts[] = {this.chckbxEconomiaDigital, this.chckbxElEconomista, this.chckbxExpansion, this.chckbxElPeriodicoE};
				for (int j = 0; j < info.getDatos().get(i).getFuentes().size(); j++)
					for (int k = 0; k < opts.length; k++)
						if (info.getDatos().get(i).getFuentes().get(j).equals(opts[k].getText()))
							opts[k].setSelected(true);
			}
		}
	}
	
	public void disableBtnGuardar () {
		this.btnGuardar.setEnabled(false);
	}
	
}
