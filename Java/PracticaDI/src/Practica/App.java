package Practica;

import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.Timer;

public class App {

	private Ventana ventana;
	private Timer crono = null;
	private Carga pantallaCarga;
	private Login pantallaLogin;
	private Opciones pantallaOpciones;
	private Configuracion pantallaConfig;
	private Noticias pantallaNoticias;
	private Log pantallaLog;
	private EventManager eventos;
	private FileManager archivos;
	private ArrayList<UserInfo> usersInfo;
	private UserInfo userInfo;
	private boolean hasGuardado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App window = new App();
					window.ventana.get().setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Error interno. 35.", "Error", JOptionPane.ERROR_MESSAGE);
					System.exit(1);
				}
			}
		});
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public App() {
		this.ventana = new Ventana ();
		this.eventos = new EventManager ();
		this.usersInfo = new ArrayList<UserInfo> ();
		this.userInfo = new UserInfo ();
		this.pantallaCarga = new Carga ();
		this.pantallaLogin = new Login (this);
		this.pantallaNoticias = new Noticias(this);
		this.pantallaLog = new Log (this);
		this.pantallaConfig = new Configuracion(this);
		this.pantallaOpciones = new Opciones (this, this.pantallaConfig, this.pantallaNoticias);
		this.archivos = new FileManager ();
		this.hasGuardado = false;
		initialize();
		crono = new Timer (40, this.eventos.exeCarga (this.pantallaCarga));
		crono.start();
		crono = new Timer (800, this.eventos.exeCargaaLogin (this.ventana, this));
		crono.restart();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize () {
		this.ventana.get().getContentPane().add(this.pantallaCarga.get());
	}
	
	public void initializeLogin () {
		this.ventana.get().setContentPane(this.pantallaLogin.get());
		this.ventana.refresh();
	}
	
	public void initializeOpciones () {
		this.ventana.get().setContentPane(this.pantallaOpciones.get ());
		this.ventana.refresh();
	}
	
	public void initializeLog () {
		this.ventana.get().setContentPane(this.pantallaLog.get ());
		this.pantallaLog.getTxtArea().setText(this.pantallaLog.getFileContent());
		this.ventana.refresh();
	}
	
	public void initializeConfig () {
		this.ventana.get().setContentPane(this.pantallaConfig.get ());
		this.ventana.refresh();
	}
	
	public void initializeNoticias () {
		this.ventana.get().setContentPane(this.pantallaNoticias.get ());
		this.ventana.refresh();
		this.pantallaNoticias.getFuentes();
		this.pantallaNoticias.addNoticias ();
	}
	
	public void setInfoSesion (UserInfo info) {
		this.userInfo.setDatos(info.getDatos());
		this.userInfo.setNombre(info.getNombre());
		this.userInfo.setPass(info.getPass());
		this.pantallaOpciones.updateInfoUser(this.userInfo.prettify());
	}
	
	public ArrayList<UserInfo> getUsersInfo () {
		return this.usersInfo;
	}
	
	public void setUsersInfo (ArrayList<UserInfo> datos) {
		this.usersInfo = datos;
	}
	
	public UserInfo getUserInfo () {
		return this.userInfo;
	}
	
	public void guardar () {
		this.archivos.guardar (this.usersInfo, this.userInfo);
	}

	public boolean getHasGuardado() {
		return hasGuardado;
	}

	public void setHasGuardado(boolean hasGuardado) {
		this.hasGuardado = hasGuardado;
	}
	
}