package Practica;

import java.util.ArrayList;

public class Categoria {
	
	private String nombre;
	private ArrayList<String> fuentes;
	
	public Categoria () {
		this.nombre = "";
		this.fuentes = new ArrayList<String> ();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<String> getFuentes() {
		return fuentes;
	}

	public void setFuentes(ArrayList<String> titulares) {
		this.fuentes = titulares;
	}

	@Override public String toString() {
		return "Categoria (nombre=" + nombre + ", titulares=" + fuentes.toString() + ")";
	}
	
	public String toFile () {
		String res = "-C-" + this.nombre + "\n" + this.fuentes.size() + "\n";
		for (int i = 0; i < this.fuentes.size(); i++)
			res += this.fuentes.get(i) + "\n";
		return res;
	}

}
