package Practica;

import java.util.ArrayList;

public class UserInfo {
	
	private String nombre;
	private String pass;
	private ArrayList<Categoria> datos;
	
	public UserInfo () {
		this.nombre = "";
		this.pass = "";
		this.datos = new ArrayList<Categoria> ();
	}
	
	public UserInfo (UserInfo datos) {
		this.nombre = datos.getNombre();
		this.pass = datos.getPass();
		this.datos = datos.getDatos();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public ArrayList<Categoria> getDatos() {
		return datos;
	}

	public void setDatos(ArrayList<Categoria> datos) {
		this.datos = datos;
	}

	@Override
	public String toString() {
		return "UserInfo (nombre=" + nombre + ", pass=" + pass + ", datos=" + datos.toString() + ")";
	}
	
	public String toFile () {
		String res = this.nombre + ";-;" + this.pass + "\n" + this.datos.size() + "\n";
		for (int i = 0; i < this.datos.size(); i++)
			res += this.datos.get(i).toFile();
		return res;
	}
	
	public String prettify () {
		return "<html>Nombre: <span style=\"color: green;\">" + nombre + "</span><br><br>Contraseņa: <span style=\"color: green;\">" + pass + "</span><br><br>Categorias: <span style=\"color: blue;\">" + datos.size() + "</span></html>";
	}
	
}
