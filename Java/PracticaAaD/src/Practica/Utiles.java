package Practica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Utiles {
	
	final static Scanner teclado = new Scanner (System.in);
	
	public static String Mostrar (ArrayList<Itinerario> lista) {
		String res = "";
		for (int i = 0; i < lista.size (); i++)
			res += (i + 1) + "-------\n" + lista.get (i).toString();
		return res;
	}

	public static String[] getNombreSeparado (String nombre) {
		String []data = nombre.split("\\.");
		String []res = new String[2];
		String path = "";

		res[1] = data[data.length - 1];
		for (int i = 0; i < data.length - 1; i++)
			if (i < data.length - 2)
				path += data[i] + ".";
			else
				path += data[i];
		
		res[0] = path;
		return res;
	}
	
	public static ArrayList<Itinerario> Modificar (int index, ArrayList<Itinerario> datos) {
		index --; // Es porque les muestro los elementos numerados del 1 a la longitud del array
		if (!(index > -1 && index < datos.size())) {
			System.err.println ("�se �ndice no est� entre las opciones.");
			return null;
		}
		System.out.print ("| Nuevo nombre: ");
		teclado.nextLine(); // Se come el \n de la pedida de opcion anterior
		String nombre = teclado.nextLine();
		System.out.print ("| Nuevo n�mero de destinos: ");
		int numDestinos = teclado.nextInt();
		teclado.nextLine(); // Se come el \n de la pedida de opcion anterior
		ArrayList<String> destinos = new ArrayList<String>();
		for (int i = 0; i < numDestinos; i++) {
			System.out.print ("| Nuevo destino " + (i + 1) + ": ");
			String eteDestino = teclado.nextLine();
			destinos.add (eteDestino);
		}
		Itinerario nuevo = new Itinerario(nombre, destinos);
		datos.set(index, nuevo);
		return datos;
	}
	
	public static long numLineas (File archivo) {
		long res = 0;
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(archivo));
			String linea = buffer.readLine();
			while (linea != null) {
				res ++;
				linea = buffer.readLine();
			}
			buffer.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(0);
		}
		return res;
	}
	
	public static ArrayList<Itinerario> Nuevo (ArrayList<Itinerario> datos) {
		System.out.print ("| Nombre: ");
		teclado.nextLine(); // Se come el \n de la pedida de opcion anterior
		String nombre = teclado.nextLine();
		System.out.print ("| N�mero de destinos: ");
		int numDestinos = teclado.nextInt();
		teclado.nextLine(); // Se come el \n de la pedida de opcion anterior
		ArrayList<String> destinos = new ArrayList<String>();
		for (int i = 0; i < numDestinos; i++) {
			System.out.print ("| Destino " + (i + 1) + ": ");
			String eteDestino = teclado.nextLine();
			destinos.add (eteDestino);
		}
		Itinerario nuevo = new Itinerario(nombre, destinos);
		datos.add(nuevo);
		return datos;
	}
	
	public static void Separar (int destinos, String nombreFile, ArrayList<Itinerario> datos) {
		FicheroItinerario mayores = new FicheroItinerario ();
		FicheroItinerario menores = new FicheroItinerario ();
		// System.out.println("Aqui -> " + Arrays.toString(this.getNombreSeparado(this.nombreFile)));
		mayores.Abrir (Utiles.getNombreSeparado(nombreFile)[0] + "_gt_" + destinos + "." + Utiles.getNombreSeparado(nombreFile)[1], 'x');
		menores.Abrir (Utiles.getNombreSeparado(nombreFile)[0] + "_lt_" + destinos + "." + Utiles.getNombreSeparado(nombreFile)[1], 'x');
		ArrayList<Itinerario> Imayores = new ArrayList<Itinerario> ();
		ArrayList<Itinerario> Imenores = new ArrayList<Itinerario> ();
		
		for (int i = 0; i < datos.size(); i++)
			if (datos.get (i).countDestinos () >= destinos)
				Imayores.add (datos.get (i));
			else
				Imenores.add (datos.get (i));
		
		mayores.Escribir(Imayores);
		menores.Escribir(Imenores);
		mayores.Cerrar();
		menores.Cerrar();
	}
	
	public static String FicheroToString (ArrayList<Itinerario> datos) {
		String res = "";
		for (int i = 0; i < datos.size (); i++)
			res += datos.get (i).toString ();
		return res;
	}
}
