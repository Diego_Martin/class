package Practica;

import java.util.ArrayList;

public class Itinerario {
	// El n�mero de itinerarios es destinos.size()
	private ArrayList<String> destinos;
	private String nombreItinerario;
	
	public Itinerario (String nombre, ArrayList<String> destinos) {
		this.nombreItinerario = nombre;
		this.destinos = destinos;
	}
	
	public int countDestinos () {
		return destinos.size();
	}
	
	public String getNombre () {
		return this.nombreItinerario;
	}
	
	public ArrayList<String> getDestinos () {
		return this.destinos;
	}
	
	@Override public String toString () {
		String res = "";
		res += this.nombreItinerario + "\n" + this.destinos.size() + "\n";
		for (int i = 0; i < this.destinos.size(); i++)
			res += this.destinos.get(i) + "\n";
		return res;
	}
}
