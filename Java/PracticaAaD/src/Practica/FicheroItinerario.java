package Practica;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class FicheroItinerario {
	
	final static Scanner teclado = new Scanner (System.in);
	private File archivo;
	private String nombreFile;
	private boolean abierto = false;
	private FileInputStream fs;
	private BufferedReader lector;
	private char modo;
	private int lineaLectura;
	
	public boolean isAbierto () {
		return this.abierto;
	}
	
	public char getModo () {
		return this.modo;
	}
	
	public File getArchivo () {
		return this.archivo;
	}
	
	public void Abrir (String nombre, char mode) {
		try {
			fs = new FileInputStream(nombre);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		}
		lector = new BufferedReader(new InputStreamReader(fs));
		if (!(mode == 'r' || mode == 'w' || mode == 'x')) {
			System.err.println("Los modos de visualizaci�n del documento son 'lectura' (r), 'escritura' (w) o 'lectura y escritura' (x)");
			System.exit(1);
		}
		if (!this.abierto) {
			this.nombreFile = nombre;
			this.archivo = new File (this.nombreFile);
			this.modo = mode;
			if (!this.archivo.exists())
				try {
					this.archivo.createNewFile();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			this.abierto = true;
		} else
			System.err.println("El fichero ya est� abierto.");
		
		this.abierto = true;
	}
	
	public ArrayList<Itinerario> Leer () {
		if (this.abierto) {
			if (this.modo == 'r' || this.modo == 'x') {
				try {
					fs = new FileInputStream(this.archivo.getPath());
					lector = new BufferedReader(new InputStreamReader(fs)); // Llega
					ArrayList<Itinerario> res = new ArrayList<Itinerario> ();
					String linea = "";
					while ((linea = lector.readLine()) != null) {
						String nombre = linea;
						int numDestinos = Integer.parseInt(lector.readLine());
						ArrayList<String> DestinosEnCuestion = new ArrayList<String>();
						for (int i = 0; i < numDestinos; i++)
							DestinosEnCuestion.add(lector.readLine());
							
						Itinerario nuevoItinerario = new Itinerario (nombre, DestinosEnCuestion);
						res.add(nuevoItinerario);
					}
					return res;
				} catch (FileNotFoundException e) {
					// System.err.println("FileNotFoundException");
					System.err.println(e.getMessage());
					System.exit(0);
				} catch (IOException e) {
					// System.err.println("IOException");
					System.err.println(e.getMessage());
					System.exit(0);
				}
			} else
				System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println("�ste fichero no est� abierto.");
		return null;
	}
	
	public void Cerrar () {
		if (this.abierto) {
			this.abierto = false;
			try {
				this.fs.close();
				this.lector.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		} else
			System.err.println("El fichero no est� abierto.");
	}
	
	public void Vaciar () {
		if (this.abierto) {
			if (this.modo == 'w' || this.modo == 'x') {
				try {
					BufferedWriter buffer = new BufferedWriter(new FileWriter(this.archivo));
					buffer.write("");
					buffer.close();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			} else
				System.err.println("El fichero debe estar abierto en modo 'escritura' (w) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println("El fichero no est� abierto.");
	}
	
	public void Escribir (ArrayList<Itinerario> datos) {
		if (this.abierto) {
			if (this.modo == 'w' || this.modo == 'x') {
				this.Vaciar ();
				FileWriter fr = null;
				String frase = "";
				for (int i = 0; i < datos.size (); i++)
					frase += datos.get (i).toString ();
				
				try {
					fr = new FileWriter (this.archivo, true);
					fr.write (frase);
					fr.close ();
				} catch (IOException e) {
					System.err.println (e.getMessage ());
				}
			} else
				System.err.println("El fichero debe estar abierto en modo 'escritura' (w) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println ("El fichero no est� abierto.");
	}
	
	public boolean Final () {
		if (this.abierto)
			if (this.modo == 'r' || this.modo == 'x')
				return (this.lineaLectura == 0);
			else
				System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
		else
			System.err.println("El fichero no est� abierto.");
		
		return false;
	}
	
}
