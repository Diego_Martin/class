package Practica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Menu {
	final static Scanner teclado = new Scanner (System.in);

	public static void main(String[] args) {
		
		FicheroItinerario miFichero = new FicheroItinerario ();
		ArrayList<Itinerario> datos = new ArrayList<Itinerario> ();
		String nombreFichero = "./media/itinerarios.txt"; 
		miFichero.Abrir(nombreFichero, 'x'); // r, w o x
		boolean salir = false;
		int opcion = 0, subopcion = 0;
		do {
			opcion = menu ();
			switch (opcion) {
				case 1:
					if (miFichero.Final())
						datos = miFichero.Leer();
					else
						System.err.println("No he ha llegado al final del fichero.");
					System.out.println ("Hecho!");
					break;
				case 2:
					System.out.println ("+----------------------------------------------------------------------+");
					System.out.println ("|                            Nuevo Itinerario                          |");
					System.out.println ("+----------------------------------------------------------------------+");
					datos = Utiles.Nuevo (datos);
					break;
				case 3:
					System.out.println ("+----------------------------------------------------------------------+");
					System.out.println ("|                          Cual quieres borrar?                        |");
					System.out.println ("+----------------------------------------------------------------------+");
					System.out.println (Utiles.Mostrar (datos));
					subopcion = teclado.nextInt ();
					if (miFichero.isAbierto())
						if (miFichero.getModo() == 'w' || miFichero.getModo() == 'x') {
							subopcion--; // Es porque les muestro los elementos numerados del 1 a la longitud del array
							try {
								datos.remove(subopcion);
							} catch (IndexOutOfBoundsException err) {
								System.err.println("�se �ndice no est� entre las opciones.");
							}
						} else
							System.err.println("El fichero debe estar abierto en modo 'escritura' (w) o en modo 'lectura y escritura' (x).");
					else
						System.err.println("El fichero no est� abierto.");
					System.out.println ("Hecho!");
					break;
				case 4:
					miFichero.Vaciar ();
					System.out.println ("Hecho!");
					break;
				case 5:
					System.out.println ("+----------------------------------------------------------------------+");
					System.out.println ("|                        Cual quieres modificar?                       |");
					System.out.println ("+----------------------------------------------------------------------+");
					System.out.println (Utiles.Mostrar(datos));
					subopcion = teclado.nextInt ();
					teclado.nextLine();
					datos = Utiles.Modificar (subopcion, datos);
					System.out.println ("Hecho!");
					break;
				case 6:
					miFichero.Escribir (datos);
					System.out.println ("Hecho!");
					break;
				case 7:
					System.out.print ("| A partir de qu� (numero de destinos) quieres separar? (No incluido): ");
					subopcion = teclado.nextInt();
					Utiles.Separar (subopcion, nombreFichero, datos);
					break;
				case 8:
					if (Utiles.numLineas (miFichero.getArchivo()) == 0)
						System.err.println ("No hay nada en el fichero.");
					else {
						System.out.println (Utiles.FicheroToString(datos));
						System.out.println ("�sto es lo que hay dentro del fichero.");
					}
					break;
				case 9:
					if (miFichero.isAbierto()) {
						if (miFichero.getModo() == 'r' || miFichero.getModo() == 'x') {
							ArrayList <Itinerario>tempList = datos;
							Itinerario temp = null;
							for (int i = 0; i < datos.size(); i++)
								for (int j = i + 1; j < datos.size(); j++)
									if (datos.get(i).countDestinos() > datos.get(j).countDestinos()) {
										temp = datos.get(i);
										datos.set(i, datos.get(j));
										datos.set(j, temp);
									}
							System.out.println(Utiles.Mostrar(tempList));
						} else
							System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
					} else
						System.err.println("El fichero no est� abierto.");
					System.out.println ("Hecho!");
					break;
				case 10:
					if (miFichero.isAbierto()) {
						if (miFichero.getModo() == 'r' || miFichero.getModo() == 'x') {
							ArrayList <Itinerario>tempList = datos;
							Itinerario temp = null;
							for (int i = 0; i < datos.size(); i++)
								for (int j = i + 1; j < datos.size(); j++)
									if (datos.get(i).countDestinos() < datos.get(j).countDestinos()) {
										temp = datos.get(i);
										datos.set(i, datos.get(j));
										datos.set(j, temp);
									}
							System.out.println(Utiles.Mostrar(tempList));
						} else
							System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
					} else
						System.err.println("El fichero no est� abierto.");
					System.out.println ("Hecho!");
					break;
				case 11:
					System.out.println ("El destino m�s solicitado es: ");
					if (miFichero.isAbierto()) {
						if (miFichero.getModo() == 'r' || miFichero.getModo() == 'x') {
							// Meto todos los destinos en una coleccion de Strings
							ArrayList<String> destinos = new ArrayList<String> ();
							for (int i = 0; i < datos.size(); i++)
								for (int j = 0; j < datos.get(i).getDestinos().size(); j++)
									destinos.add(datos.get(i).getDestinos().get(j));
							// Ya los tengo todos en teoria
							// Ahora veo cual es el mas repetido
							int elQueMasApareceAparece = 0;
							int indexDelQueMasAparece = -1;
							for (int i = 0; i < destinos.size(); i++)
								if (Collections.frequency(destinos, destinos.get(i)) > elQueMasApareceAparece) {
									elQueMasApareceAparece = Collections.frequency(destinos, destinos.get(i));
									indexDelQueMasAparece = i;
								}
							System.out.println(destinos.get(indexDelQueMasAparece));
						} else
							System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
					} else 
						System.err.println("El fichero no est� abierto.");
					break;
				case 12:
					System.out.println ("Hasta luego!");
					System.exit(0);
				default:
					System.err.println ("�sa no es una opci�n v�lida.");
					break;
			}
			System.out.println("\n\n");
		} while (!salir);
		teclado.close();
	}
	
	private static int menu () {
		int opcion = 0;
		System.out.println ("+----------------------------------------------------------------------+");
		System.out.println ("|                                  Menu                                |");
		System.out.println ("+----------------------------------------------------------------------+");
		System.out.println ("| 1.  Leer Fichero                                                     |");
		System.out.println ("| 2.  Insertar                                                         |");
		System.out.println ("| 3.  Borrar                                                           |");
		System.out.println ("| 4.  Vaciar                                                           |");
		System.out.println ("| 5.  Modificar                                                        |");
		System.out.println ("| 6.  Guardar Fichero                                                  |");
		System.out.println ("| 7.  Separar Ficheros                                                 |");
		System.out.println ("| 8.  Mostrar Contenido                                                |");
		System.out.println ("| 9.  Mostrar Itinearios de Menor a Mayor                              |");
		System.out.println ("| 10. Mostrar Itinearios de Mayor a Menor                              |");
		System.out.println ("| 11. Mostrar el Destino m�s Repetido de Todos los Intinerios          |");
		System.out.println ("| 12. Salir                                                            |");
		System.out.println ("+----------------------------------------------------------------------+");
		System.out.print   ("| Opcion: ");
        opcion = teclado.nextInt();
		System.out.println ("+----------------------------------------------------------------------+");
		return opcion;
	}

}