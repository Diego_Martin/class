package Practica;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class FicheroItinerario2 {
	
	final static Scanner teclado = new Scanner (System.in);
	private File archivo;
	private String nombreFile;
	private boolean abierto = false;
	private ArrayList<Itinerario> datos;
	private FileInputStream fs;
	private BufferedReader lector;
	private char modo;
	private int lineaLectura;
	
	public FicheroItinerario2 () {
		this.datos = new ArrayList<Itinerario> ();
	}
	
	public void Abrir (String nombre, char mode) {
		try {
			fs = new FileInputStream(this.archivo.getPath());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		lector = new BufferedReader(new InputStreamReader(fs));
		if (!(mode == 'r' || mode == 'w' || mode == 'x')) {
			System.err.println("Los modos de visualizaci�n del documento son 'lectura' (r), 'escritura' (w) o 'lectura y escritura' (x)");
			System.exit(1);
		}
		if (!this.abierto) {
			this.nombreFile = nombre;
			this.archivo = new File (this.nombreFile);
			this.modo = mode;
			if (!this.archivo.exists())
				try {
					this.archivo.createNewFile();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			this.abierto = true;
		} else
			System.err.println("El fichero ya est� abierto.");
		
		this.abierto = true;
	}
	
	public void Leer () {
		/*if (this.abierto) {
			if (this.modo == 'r' || this.modo == 'x') {
				try {
					fs = new FileInputStream(this.archivo.getPath());
					lector = new BufferedReader(new InputStreamReader(fs)); // Llega
					String linea = "";
					while ((linea = lector.readLine()) != null) {
						String nombre = linea;
						int numDestinos = Integer.parseInt(lector.readLine());
						ArrayList<String> DestinosEnCuestion = new ArrayList<String>();
						for (int i = 0; i < numDestinos; i++)
							DestinosEnCuestion.add(lector.readLine());
							
						Itinerario nuevoItinerario = new Itinerario (nombre, DestinosEnCuestion);
						this.datos.add(nuevoItinerario);
					}
				} catch (FileNotFoundException e) {
					// System.err.println("FileNotFoundException");
					System.err.println(e.getMessage());
					System.exit(0);
				} catch (IOException e) {
					// System.err.println("IOException");
					System.err.println(e.getMessage());
					System.exit(0);
				}
			} else
				System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println("�ste fichero no est� abierto.");*/
	}
	
	public void Leer (int itinerario) {
		if (this.abierto) {
			if (this.modo == 'r' || this.modo == 'x') {
				try {
					fs = new FileInputStream(this.archivo.getPath());
					lector = new BufferedReader(new InputStreamReader(fs)); // Llega
					String linea = "";
					if (itinerario > this.numLineas()) {
						System.err.println ("");
					}
					for (int i = 0; i < itinerario; i++, this.lineaLectura++) {
						if ((linea = lector.readLine()) == null) {
							this.lineaLectura = 0;
							System.err.println("Se ha acabado el fichero antes de llegar al itinerario solicitado.");
							System.exit(0);
						}
						
						String nombre = linea;
						int numDestinos = Integer.parseInt(lector.readLine());
						this.lineaLectura++;
						ArrayList<String> DestinosEnCuestion = new ArrayList<String>();
						for (int j = 0; j < numDestinos; j++, this.lineaLectura++)
							DestinosEnCuestion.add(lector.readLine());
							
						Itinerario nuevoItinerario = new Itinerario (nombre, DestinosEnCuestion);
						this.datos.add(nuevoItinerario);
					}
				} catch (FileNotFoundException e) {
					System.err.println(e.getMessage());
					System.exit(0);
				} catch (IOException e) {
					System.err.println(e.getMessage());
					System.exit(0);
				}
			} else
				System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println("�ste fichero no est� abierto.");
	}
	
	public void Cerrar () {
		if (this.abierto) {
			this.abierto = false;
			try {
				this.fs.close();
				this.lector.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		} else
			System.err.println("El fichero no est� abierto.");
	}
	
	public void Vaciar () {
		if (this.abierto) {
			if (this.modo == 'w' || this.modo == 'x') {
				try {
					BufferedWriter buffer = new BufferedWriter(new FileWriter(this.archivo));
					buffer.write("");
					buffer.close();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			} else
				System.err.println("El fichero debe estar abierto en modo 'escritura' (w) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println("El fichero no est� abierto.");
	}
	
	public void Escribir () {
		if (this.abierto) {
			if (this.modo == 'w' || this.modo == 'x') {
				this.Vaciar ();
				FileWriter fr = null;
				String frase = "";
				for (int i = 0; i < this.datos.size (); i++)
					frase += this.datos.get (i).toString ();
				
				try {
					fr = new FileWriter (this.archivo, true);
					fr.write (frase);
					fr.close ();
				} catch (IOException e) {
					System.err.println (e.getMessage ());
				}
			} else
				System.err.println("El fichero debe estar abierto en modo 'escritura' (w) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println ("El fichero no est� abierto.");
	}
	
	public long numLineas () {
		if (this.abierto) {
			if (this.modo == 'r' || this.modo == 'x') {
				long res = 0;
				try {
					BufferedReader buffer = new BufferedReader(new FileReader(this.archivo));
					String linea = buffer.readLine();
					while (linea != null) {
						res ++;
						linea = buffer.readLine();
					}
					buffer.close();
				} catch (IOException e) {
					System.err.println(e.getMessage());
					System.exit(0);
				}
				return res;
			} else {
				System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
				return 0;
			}
		} else {
			System.err.println ("El fichero no est� abierto.");
			return 0;
		}
	}
	
	public void Borrar (int numero) {
		if (this.abierto)
			if (this.modo == 'w' || this.modo == 'x') {
				numero--; // Es porque les muestro los elementos numerados del 1 a la longitud del array
				try {
					this.datos.remove(numero);
				} catch (IndexOutOfBoundsException err) {
					System.err.println("�se �ndice no est� entre las opciones.");
				}
			} else
				System.err.println("El fichero debe estar abierto en modo 'escritura' (w) o en modo 'lectura y escritura' (x).");
		else
			System.err.println("El fichero no est� abierto.");
	}
	
	public void Nuevo () {
		if (this.abierto) {
			if (this.modo == 'w' || this.modo == 'x') {
				System.out.print ("| Nombre: ");
				teclado.nextLine(); // Se come el \n de la pedida de opcion anterior
				String nombre = teclado.nextLine();
				System.out.print ("| N�mero de destinos: ");
				int numDestinos = teclado.nextInt();
				teclado.nextLine(); // Se come el \n de la pedida de opcion anterior
				ArrayList<String> destinos = new ArrayList<String>();
				for (int i = 0; i < numDestinos; i++) {
					System.out.print ("| Destino " + (i + 1) + ": ");
					String eteDestino = teclado.nextLine();
					destinos.add (eteDestino);
				}
				Itinerario nuevo = new Itinerario(nombre, destinos);
				this.datos.add(nuevo);
			} else
				System.err.println("El fichero debe estar abierto en modo 'escritura' (w) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println("El fichero no est� abierto.");
	}
	
	public void Modificar (int index) {
		if (this.abierto) {
			if (this.modo == 'w' || this.modo == 'x') {
				index --; // Es porque les muestro los elementos numerados del 1 a la longitud del array
				if (!(index > -1 && index < this.datos.size())) {
					System.err.println ("�se �ndice no est� entre las opciones.");
					return;
				}
				System.out.print ("| Nuevo nombre: ");
				teclado.nextLine(); // Se come el \n de la pedida de opcion anterior
				String nombre = teclado.nextLine();
				System.out.print ("| Nuevo n�mero de destinos: ");
				int numDestinos = teclado.nextInt();
				teclado.nextLine(); // Se come el \n de la pedida de opcion anterior
				ArrayList<String> destinos = new ArrayList<String>();
				for (int i = 0; i < numDestinos; i++) {
					System.out.print ("| Nuevo destino " + (i + 1) + ": ");
					String eteDestino = teclado.nextLine();
					destinos.add (eteDestino);
				}
				Itinerario nuevo = new Itinerario(nombre, destinos);
				this.datos.set(index, nuevo);
			} else
				System.err.println("El fichero debe estar abierto en modo 'escritura' (w) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println ("El fichero no est� abierto.");
	}
	
	public String ordenarMam () { // Mayor a menor
		if (this.abierto) {
			if (this.modo == 'r' || this.modo == 'x') {
				ArrayList <Itinerario>tempList = this.datos;
				Itinerario temp = null;
				for (int i = 0; i < this.datos.size(); i++)
					for (int j = i + 1; j < this.datos.size(); j++)
						if (this.datos.get(i).countDestinos() < this.datos.get(j).countDestinos()) {
							temp = this.datos.get(i);
							this.datos.set(i, this.datos.get(j));
							this.datos.set(j, temp);
						}
				return this.Mostrar(tempList);
			} else
				System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println("El fichero no est� abierto.");
		
		return null;
	}
	
	public String ordenarmaM () { // Menor a mayor
		if (this.abierto) {
			if (this.modo == 'r' || this.modo == 'x') {
				ArrayList <Itinerario>tempList = this.datos;
				Itinerario temp = null;
				for (int i = 0; i < this.datos.size(); i++)
					for (int j = i + 1; j < this.datos.size(); j++)
						if (this.datos.get(i).countDestinos() > this.datos.get(j).countDestinos()) {
							temp = this.datos.get(i);
							this.datos.set(i, this.datos.get(j));
							this.datos.set(j, temp);
						}
				return this.Mostrar(tempList);
			} else
				System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println("El fichero no est� abierto.");
		
		return null;
	}
	
	public String getDestinoMasSolicitado () {
		if (this.abierto) {
			if (this.modo == 'r' || this.modo == 'x') {
				// Meto todos los destinos en una coleccion de Strings
				ArrayList<String> destinos = new ArrayList<String> ();
				for (int i = 0; i < this.datos.size(); i++)
					for (int j = 0; j < this.datos.get(i).getDestinos().size(); j++)
						destinos.add(this.datos.get(i).getDestinos().get(j));
				// Ya los tengo todos en teoria
				// Ahora veo cual es el mas repetido
				int elQueMasApareceAparece = 0;
				int indexDelQueMasAparece = -1;
				for (int i = 0; i < destinos.size(); i++)
					if (Collections.frequency(destinos, destinos.get(i)) > elQueMasApareceAparece) {
						elQueMasApareceAparece = Collections.frequency(destinos, destinos.get(i));
						indexDelQueMasAparece = i;
					}
				String res = destinos.get(indexDelQueMasAparece);
				return res;
			} else
				System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
		} else 
			System.err.println("El fichero no est� abierto.");
		return null;
	}
	
	private String[] getNombreSeparado (String nombre) {
		String []data = nombre.split("\\.");
		String []res = new String[2];
		String path = "";
		System.out.println(nombre);
		System.out.println(Arrays.toString(data));
		res[1] = data[data.length - 1];
		for (int i = 0; i < data.length - 1; i++)
			if (i < data.length - 2)
				path += data[i] + ".";
			else
				path += data[i];
		
		res[0] = path;
		return res;
	}
	
	public void Separar (int destinos) {
		if (this.abierto) {
			if (this.modo == 'r' || this.modo == 'x') {
				FicheroItinerario2 mayores = new FicheroItinerario2 ();
				FicheroItinerario2 menores = new FicheroItinerario2 ();
				// System.out.println("Aqui -> " + Arrays.toString(this.getNombreSeparado(this.nombreFile)));
				mayores.Abrir (this.getNombreSeparado(this.nombreFile)[0] + "_gt_" + destinos + "." + this.getNombreSeparado(this.nombreFile)[1], 'x');
				menores.Abrir (this.getNombreSeparado(this.nombreFile)[0] + "_lt_" + destinos + "." + this.getNombreSeparado(this.nombreFile)[1], 'x');
				
				for (int i = 0; i < this.datos.size(); i++)
					if (this.datos.get (i).countDestinos () >= destinos)
						mayores.Anadir (this.datos.get (i));
					else
						menores.Anadir (this.datos.get (i));
				mayores.Escribir();
				menores.Escribir();
				mayores.Cerrar();
				menores.Cerrar();
			} else
				System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println("El fichero no est� abierto.");
	}
	
	public boolean Final () {
		if (this.abierto) {
			if (this.modo == 'r' || this.modo == 'x') {
				return (this.lineaLectura == 0);
			} else
				System.err.println("El fichero debe estar abierto en modo 'lectura' (r) o en modo 'lectura y escritura' (x).");
		} else
			System.err.println("El fichero no est� abierto.");
		
		return false;
	}
	
	private void Anadir(Itinerario itinerario) {
		this.datos.add (itinerario);
	}

	@Override public String toString () {
		String res = "";
		for (int i = 0; i < this.datos.size (); i++)
			res += this.datos.get (i).toString ();
		return res;
	}
	
	public String Mostrar () {
		String res = "";
		for (int i = 0; i < this.datos.size (); i++)
			res += (i + 1) + "-------\n" + this.datos.get (i).toString();
		return res;
	}
	
	public String Mostrar (ArrayList<Itinerario> lista) {
		String res = "";
		for (int i = 0; i < lista.size (); i++)
			res += (i + 1) + "-------\n" + lista.get (i).toString();
		return res;
	}
	
}
