package Jarvis;

import java.util.Scanner;

public class Asistente {
	String nombre, accion = "";
	boolean salir = false, modoSilencio = true, modoDictado = false;
	Scanner teclado = new Scanner (System.in);
	
	private static final String UNICODE = "ÀàÈèÌìÒòÙùÁáÉéÍíÓóÚúÝýÂâÊêÎîÔôÛûŶŷÃãÕõÑñÄäËëÏïÖöÜüŸÿÅåÇçŐőŰű";
	private static final String PLAIN_ASCII = "AaEeIiOoUuAaEeIiOoUuYyAaEeIiOoUuYyAaOoNnAaEeIiOoUuYyAaCcOoUu";
	
	public Asistente (String nombre, String[] args) {
		this.nombre = nombre;
	}
	
	public Asistente (String[] args) {
		this.nombre = "Jarvis";
	}
	
	public static String acent(String str) {
	    if (str == null)
	        return null;
	    
	    StringBuilder sb = new StringBuilder();
	    for (int index = 0; index < str.length(); index++) {
	        char c = str.charAt(index);
	        int pos = UNICODE.indexOf(c);
	        if (pos > -1)
	            sb.append(PLAIN_ASCII.charAt(pos));
	        else {
	            sb.append(c);
	        }
	    }
	    return sb.toString();
	}
	
	public String escuchar () {
		System.out.print ("Escuchando: ");
		if (!this.modoSilencio) {
			return "Esta opcion no esta programada";
		} else {
			String accion = this.teclado.nextLine();
			return acent(accion.toLowerCase());
		}
	}
	
	public void hablar (String texto) {
		if (!this.modoSilencio)
			System.out.println ("Esta opcion no esta programada");
		else
			System.out.println (texto);
	}
	
	public boolean listaEnOrden (String [] lista, String orden, String [] no, String [] y) {
		for (String item : lista) {
			if (no.length > 0 && y.length > 0) {
				for (String n : no)
					for (String i : y)
						if (orden.contains(item) && !(orden.contains(n) && orden.contains(i)))
							return false;
			} else if (no.length > 0 && y.length == 0) {
				for (String n : no)
					if (orden.contains(item) && !orden.contains(n))
						return true;
			} else if (no.length == 0 && y.length > 0) {
				for (String i : y)
					if (orden.contains(item) && orden.contains(i))
						return true;
			} else {
				if (orden.contains(item))
					return true;
			}
		}
		return false;
	}
	
	public void ejecutar () {
		while (!this.salir) {
			this.accion = this.escuchar();
			
			if (this.accion != "") {
				System.out.println (this.accion);
				if (listaEnOrden (new String [] {"salir", "exit", "adios", "apagar", "apagate", "desconectar"}, this.accion, new String [] {"exito"}, new String [] {})) {
					this.salir = true;
				}
			} else {
				System.out.println ("Vacio");
			}
		}
		this.teclado.close();
		System.out.println("Hasta luego!");
		System.exit(0);
	}
}