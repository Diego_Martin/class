package Clase1;

import java.util.Scanner;

public class JuegosDAM {

	public static void main(String[] args) {
		int base = 5;
		String palabra = "romA";
		Scanner teclado = new Scanner (System.in);
		
		System.out.println("Bienvenid@ a Juegos DAM");
		System.out.println("Las opciones son:\n1.- Dibujar pir�mide.\n2.-Jugar palabra (romA)\n3.- Salir\n");
		int opcion = teclado.nextInt();
		boolean exit = false;
		
		do {
			switch (opcion) {
				case 1:
					dibujar (base);
					break;
				case 2:
					jugarPalabra (palabra);
					break;
				case 3:
					System.out.println("*** Gracias por jugar ***\n    Hasta la pr�xima.");
					exit = true;
					break;
				default:
					System.out.println("Recuerda que las opciones son:\n1.- Dibujar pir�mide.\n2.-Jugar palabra (romA)\n3.- Salir\n");
			}
			if (!exit) {
				System.out.println("�Quieres seguir jugando?\nLas opciones son:\n1.- Dibujar pir�mide.\n2.-Jugar palabra (romA)\n3.- Salir\n");
				opcion = teclado.nextInt();
			}
		} while (exit == false);
		teclado.close();
	}
	
	private static void dibujar (int base) {
		if (base % 2 == 1 && base > 0) {
			for (int i = 0; i < (base + 1) / 2; i++) {
				for (int j = 0; j < (base + 1) / 2 - i - 1; j++)
					System.out.print("  ");
					
				for (int j = 0; j < 2 * i + 1; j++)
					System.out.print("* ");
				
				System.out.println();
			}
		} else {
			System.out.println("La base de la pir�mide debe ser un n�mero impar entero.");
		}
	}
	private static void jugarPalabra(String palabra) {
		for (int i = palabra.length() - 1; i >= 0; i--)
			System.out.print(palabra.toUpperCase().charAt(i));
		System.out.println();
	}
}