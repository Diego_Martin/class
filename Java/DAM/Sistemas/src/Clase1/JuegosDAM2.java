package Clase1;

import java.util.*;

public class JuegosDAM2 {

	public static void main(String[] args) {
		// int base = 5;
		// String palabra = "romA";
		Scanner teclado = new Scanner (System.in);
		
		System.out.println("Bienvenid@ a Juegos DAM");
		System.out.println("Las opciones son:\n1.- Dibujar formas.\n2.-Jugar palabra\n3.- Salir\n");
		int opcion = teclado.nextInt();
		boolean exit = false;
		
		// Bucle del menu
		do {
			switch (opcion) {
				case 1:
					dibujar (teclado); // La funcion dibujar() es el men� que da lugar a dibujar los cuadrados, triangulos o rombos. Le paso el teclado para no tener que instanciar otro
					break;
				case 2:
					palabra (teclado); // La funcion palabra() es el men� que da lugar a dar la vuelta a las palabras o contar consonantes y vocales. Le paso el teclado para no tener que instanciar otro
					break;
				case 3: // Opcion de salir
					System.out.println("#########################\n##  Gracias por jugar  ##\n##  Hasta la pr�xima.  ##\n#########################");
					exit = true; // Indica que has salido para no ejecutar el if de la l�nea 33
					break;
				default: // Cuando no ha puesto una opcion valida
					System.out.println("Recuerda que las opciones son:\n1.- Dibujar formas.\n2.-Jugar palabra\n3.- Salir\n");
			}
			if (!exit) { // Si no se indica que se sale del bucle, imprime el menu y pide una opcion
				System.out.println("�Quieres seguir jugando?\nLas opciones son:\n1.- Dibujar formas.\n2.- Jugar palabra\n3.- Salir\n");
				opcion = teclado.nextInt();
			}
		} while (exit == false);
		teclado.close(); // Finaliza el teclado
	}
	
	private static void dibujar (Scanner teclado) { // Menu de dibujar
		// Imprime las opciones
		System.out.print ("+----------------------+\n|�Que quieres dibujar? |\n+----------------------+\n| 1.- Cuadrado.        |\n| 2.- Triangulo.       |\n| 3.- Rombo            |\n| 4.- Rombo Relleno    |\n| Tu eleccion: ");
		int opcion;
		boolean exit = false;
		// En este bucle el programa se encarga repetir el input hasta que la opcion que se introduzca sea v�lida
		do {
			opcion = teclado.nextInt();
			switch (opcion) {
				case 1:
					dibuja (teclado, "Cuadrado"); // En la funci�n dibuja() se le pasa el teclado (Para no abrir otro) y se le dice que lo que dibuja es un cuadrado
					exit = true;
					break;
				case 2:
					dibuja (teclado, "Triangulo"); // En la funci�n dibuja() se le pasa el teclado (Para no abrir otro) y se le dice que lo que dibuja es un triangulo
					exit = true;
					break;
				case 3:
					dibuja (teclado, "Rombo"); // En la funci�n dibuja() se le pasa el teclado (Para no abrir otro) y se le dice que lo que dibuja es un rombo
					exit = true;
					break;
				case 4:
					dibuja (teclado, "RomboR"); // En la funci�n dibuja() se le pasa el teclado (Para no abrir otro) y se le dice que lo que dibuja es un rombo relleno
					exit = true;
					break;
				default:
					System.out.println("Debe ser un n�mero entre el 1 y el 3.");
			}
		} while (!exit);
	}
	
	private static void dibuja (Scanner teclado, String forma) {
		int base;
		switch (forma) { // Este switch no tiene sentencia "default" porque "forma" la doy yo, no el usuario, los posibles errores de usuario los contempla la funcion dibujar()
			case "Cuadrado": // Dibuja un cuadrado preguntando la base
				System.out.print("Indica la base: ");
				base = teclado.nextInt();
				for	(int y = 0; y < base; y++) {
					for (int x = 0; x < base; x++)
						if (y == 0 || y == (base - 1) || x == 0 || x == (base - 1))
							System.out.print("*");
						else
							System.out.print(" ");
					System.out.println("");
				}
				break;
			case "Triangulo": // Dibuja un triangulo rectangulo preguntando la base
				System.out.print("Indica la base: ");
				base = teclado.nextInt();
				for (int i = 0; i < base; i++) {
					
					for (int j = 0; j <= i; j++)
						
						if (j == 0 || i == (base - 1) || i == j)
							System.out.print("*");
						else
							System.out.print(" ");
					
					System.out.println();
				}
				break;
			case "Rombo": // Dibuja un rombo rombo vacio indicando la base
				System.out.print("Indica la mayor anchura: ");
				base = teclado.nextInt();
				if (base % 2 == 1 && base > 0) {
					for (int i = 0; i < (base + 1) / 2; i++) {
						for (int j = 0; j < 2 * i + 1; j++)
							if (j == 0 || j == 2 * i)
								System.out.print("*");
							else
								System.out.print(" ");
						
						System.out.println();
					}
					for (int i = (base - 3) / 2; i >= 0 ; i--) {
						for (int j = 0; j < (base + 1) / 2 - i - 1; j++)
							System.out.print(" ");
							
						for (int j = 0; j < 2 * i + 1; j++)
							if (j == 0 || j == 2 * i)
								System.out.print("*");
							else
								System.out.print(" ");
						
						System.out.println();
					}
				} else {
					System.out.println("La base del rombo debe ser un n�mero impar entero.");
				}
				break;
			case "RomboR": // Dibuja un rombo relleno indicando la base
				System.out.print("Indica la mayor anchura: ");
				base = teclado.nextInt();
				if (base % 2 == 1 && base > 0) {
					for (int i = 0; i < (base + 1) / 2; i++) {
						for (int j = 0; j < (base + 1) / 2 - i - 1; j++)
							System.out.print(" ");
							
						for (int j = 0; j < 2 * i + 1; j++)
							System.out.print("*");
						
						System.out.println();
					}
					for (int i = (base - 3) / 2; i >= 0 ; i--) {
						for (int j = 0; j < (base + 1) / 2 - i - 1; j++)
							System.out.print(" ");
							
						for (int j = 0; j < 2 * i + 1; j++)
							System.out.print("*");
						
						System.out.println();
					}
				} else {
					System.out.println("La base del rombo debe ser un n�mero impar entero.");
				}
				break;
			// No existe la posibilidad de que haya otros valores porque los estoy dando yo, no el usuario
		}
	}
	
	private static void palabra(Scanner teclado) { // Menu de los juegos de palabras, se encarga de que al llamar a jugarPalabra() no haya errores
		System.out.print ("+---------------------------------------+\n| �Que quieres hacer?                   |\n+---------------------------------------+\n| 1.- Dar la vuelta a una palabra.      |\n| 2.- Separar vocales y consonantes.    |\n| Tu eleccion: ");
		int opcion = teclado.nextInt();
		boolean exit = false;
		do {
			switch (opcion) {
				case 1:
					jugarPalabra (teclado, "Vuelta");
					exit = true;
					break;
				case 2:
					jugarPalabra (teclado, "Vocales");
					exit = true;
					break;
				default:
					System.out.println("Debe ser un n�mero entre el 1 y el 2.");
			}
		} while (!exit);
	}
	
	private static void jugarPalabra(Scanner teclado, String juego) { // Los juegos con palabras
		String palabra;
		teclado.nextLine(); // Por un error de Java, despues de hacer teclado.nextInt() el '\n' se queda en el b�ffer y �sta linea lo consume
		switch (juego) {
			case "Vuelta": // Programa para dar la vuelta a la palabra
				System.out.print ("Introduce la palabra: ");
				palabra = teclado.nextLine();
				for (int i = palabra.length() - 1; i >= 0; i--)
					System.out.print(palabra.charAt(i));
				
				System.out.println();
				break;
			case "Vocales": // Programa para separar vocales de consonantes
				System.out.print ("Intoduce la frase: ");
				palabra = teclado.nextLine();

				List<Character> consonantes = new ArrayList<Character>(); // En este array se almacenan las consonantes
				List<Character> vocales = new ArrayList<Character>(); // En este array se almacenan las vocales
				
				for (int i = 0; i < palabra.length(); i++) { // Recorro la palabra 
					char letra = palabra.toLowerCase().charAt(i); 
					if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u') { // Si la letra es vocal se mete la letra en el Array de vocales
						vocales.add(palabra.charAt(i));
					}
					if (letra != 'a' && letra != 'e' && letra != 'i' && letra != 'o' && letra != 'u' && letra != ' ') { // Si no es vocal (o espacio) se mete en el Array de consonantes
						consonantes.add(palabra.charAt(i));
					}
				}
				System.out.println();
				System.out.println("Consonantes: " + consonantes.toString()); // Imprimo los Arrays
				System.out.println("Vocales: " + vocales.toString() + "\n");
				break;
		}
	}
}