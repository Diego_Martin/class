package Clase1;

import java.util.Scanner;

class _02_suma {
	
	_02_suma () {
		int resultado = 0;
		
		resultado = suma (2, 3);
		
		System.out.println ("Resultado: " + resultado);
	}
	
	public static int suma (int op1, int op2) {
		return op1 + op2;
	}
}

public class CtoJava {
	public static int sizeof(Class dataType) { 
		if (dataType == null) throw new NullPointerException();
		if (dataType == byte.class || dataType == Byte.class) return 1;
		if (dataType == short.class || dataType == Short.class) return 2;
		if (dataType == char.class || dataType == Character.class) return 2;
		if (dataType == int.class || dataType == Integer.class) return 4;
		if (dataType == long.class || dataType == Long.class) return 8;
		if (dataType == float.class || dataType == Float.class) return 4;
		if (dataType == double.class || dataType == Double.class) return 8;
		return 4; // default for 32-bit memory pointer
	}

	public static void _00_prueba_main () {
		System.out.println ("Hola mundo");
	}
	
	public static void _01_sizeof_sizeof() {
		System.out.println ("int: " + sizeof(Integer.class) + " bytes");
		System.out.println ("short: " + sizeof(Short.class) + " bytes");
		System.out.println ("long: " + sizeof(Long.class) + " bytes");
		System.out.println ("byte: " + sizeof(Byte.class) + " bytes");
		System.out.println ("char: " + sizeof(Character.class) + " bytes");
		System.out.println ("float: " + sizeof(Float.class) + " bytes");
		System.out.println ("double: " + sizeof(Double.class) + " bytes");
	}
	
	public static void showMenu () {
		System.out.println("\n\n");
		System.out.println ("+-------------------------------------------------+");
		System.out.println ("|                     C to Java                   |");
		System.out.println ("+-------------------------------------------------+");
		System.out.println ("| 0. Hola mundo.                                  |");
		System.out.println ("| 1. Sizeof.                                      |");
		System.out.println ("| 2. Suma.                                        |");
		System.out.println ("| -1. Salir.                                      |");
		System.out.println ("+-------------------------------------------------+");
	}
	
	public static void main(String[] args) {
		int programa = 0;
		String opt;
		boolean seguir = true, continua = false;
		Scanner teclado = new Scanner (System.in);
		while (seguir) {
			showMenu ();
			System.out.print ("| Qu� programa ejecuto?: ");
			opt = teclado.nextLine();
			try {
				programa = Integer.parseInt(opt);
				if (programa == -1) break;
				continua = true;
			} catch (Exception err) {
				System.out.println ("Debes introducir un numero :)");
			}
			if (continua) {
				System.out.println("\n\n");
				switch (programa) {
					case 0:
						_00_prueba_main ();
						break;
					case 1:
						_01_sizeof_sizeof ();
						break;
					case 2:
						_02_suma ej2 = new _02_suma ();
						System.gc();
						break;
					default:
						System.out.println ("Esa opcion no est� programada.");
				}
			}
		}
		System.out.println ("Hasta luego.");
		teclado.close();
	}
}