package Clase1;

import java.util.Scanner;

public class Ec2grado {

	
	public static void saca2res (double a, double b, double c) {
		double raiz = Math.sqrt(Math.pow(b, 2.0) - (4 * a * c));
		double dosA = 2 * a;
		double res1 = ((b * -1) + raiz) / dosA;
		double res2 = ((b * -1) - raiz) / dosA;
		System.out.println ("Resultado 1: " + res1);
		System.out.println ("Resultado 2: " + res2);
	}


	public static void saca1res (double a, double b) {
		double dosA = 2 * a;
		double res = (b * -1) / dosA;
		System.out.println ("Resultado: " + res);
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner teclado = new Scanner (System.in);
		double a, b, c;
		boolean seguir = true;
		int resp = 0;
		
		while (seguir) {
			System.out.println ("+----------------------------+");
			System.out.println ("|   Ecuaciones de 2� grado   |");
			System.out.println ("+----------------------------+");
			System.out.println ("| ax^2 + bx + c = 0          |");
			System.out.println ("+----------------------------+");
			System.out.print ("| T�rmino a: ");
			a = teclado.nextDouble();
			System.out.print ("| T�rmino b: ");
			b = teclado.nextDouble();
			System.out.print ("| T�rmino c: ");
			c = teclado.nextDouble();
			
			System.out.println ("+----------------------------+");
			System.out.println("| " + a + "x^2 " + b + "x " + c);
			
			System.out.print ("| Es �sta tu ecuaci�n? ((No) 0/(Si) 1): ");
			resp = teclado.nextInt();
			
			if (resp == 1) {
				seguir = false;
				if (Math.pow(b, 2.0) - (4 * a * c) > 0)
					saca2res (a, b, c);

				if (Math.pow(b, 2.0) - (4 * a * c) == 0)
					saca1res (a, b);

				if (Math.pow(b, 2.0) - (4 * a * c) < 0)
					System.out.println ("�sta ecuaci�n no tiene soluciones con n�meros reales.");

			} else {
				System.out.println("Vale, te preguntare de nuevo.");
			}
		}
		teclado.close();
	}
}