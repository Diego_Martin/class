package Clase1;

import java.util.Scanner;

public class JavaIO {

	public static void main(String[] args) {
		final double PI = 3.1416;
		double radio, altura;
		boolean seguir = true;
		String seguir1;
		
		Scanner teclado = new Scanner(System.in);
		while (seguir) {
			System.out.println("Programa para calcular el volumen de un cilindro");
			System.out.println("Introduzca los valores para la base y la altura");
			System.out.print("Radio de la base: ");
			radio = teclado.nextDouble();
			System.out.print("Valor de la altura: ");
			altura = teclado.nextDouble();
			System.out.println("El volumen del cilindro es: " + (PI * radio * altura));
			System.out.print("¿Quieres seguir calculando? (s/n): ");
			seguir1 = teclado.nextLine();
			if (seguir1 == "s")
				seguir = true;
			else 
				seguir = false;
		}
		System.out.println("Hasta luego.");
		teclado.close();
	}
}