package TablaNumeros;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner teclado = new Scanner (System.in);
		String aux = "";
		ArrayNumeros lista = new ArrayNumeros (5);
		int opcion = 0;
		boolean repetir = true;
		
		do {
			Numero num = null;
			// System.out.println(lista.length ());
			System.out.println("+-----------------------------------+");
			System.out.println("| 1.- Introducir numero.            |");
			System.out.println("| 2.- Escribir numeros.             |");
			System.out.println("| 3.- Escribir el numero mas alto.  |");
			System.out.println("| 4.- Escribir el numero mas bajo.  |");
			System.out.println("| 5.- Borrar numero.                |");
			System.out.println("| 6.- Ordenar.                      |");
			System.out.println("| 7.- Salir.                        |");
			System.out.println("+-----------------------------------+");
			do {
				repetir = false;
				try {
					System.out.print ("| Que quieres hacer?: ");
					aux = teclado.nextLine ();
					
					opcion = Integer.parseInt (aux);
					repetir = true;
					
				} catch (NumberFormatException err) { // De java.lang
					System.out.println ("| Debes escribir un numero entero.");
				}
			} while (!repetir);
			
			repetir = false;
			switch (opcion) {
				case 1:
					System.out.println("+-----------------------------------+");
					System.out.println("| 1.- Introducir numero.            |");
					System.out.println("+-----------------------------------+");
					do {
						repetir = false;
						try {
							System.out.print ("| Introduce el numero para a�adir: ");
							aux = teclado.nextLine ();
							
							num = new Numero(Integer.parseInt(aux));
							repetir = true;
							
						} catch (NumberFormatException err) { // De java.lang
							System.out.println ("| Debes escribir un numero.");
						}
					} while (!repetir);
					
					repetir = false;
					lista.insertarNumero(num);
					System.out.println("Hecho!");
					System.out.println("\n\n");
					break;
				case 2:
					System.out.println("+-----------------------------------+");
					System.out.println("| 2.- Escribir numeros.             |");
					System.out.println("+-----------------------------------+");
					System.out.println(lista.imprimir ()); // lista.toString ()
					System.out.println("\n");
					System.out.println("Hecho!");
					System.out.println("\n\n");
					break;
				case 3:
					System.out.println("+-----------------------------------+");
					System.out.println("| 3.- Escribir el numero mas alto.  |");
					System.out.println("+-----------------------------------+");
					try {
						System.out.println("El numero mas alto es el " + lista.mayor().imprimir());
					}catch (Exception err) {
						System.out.println("La lista est� vac�a.");
						System.out.println(err);
					}
					System.out.println("\n\n");
					break;
				case 4:
					System.out.println("+-----------------------------------+");
					System.out.println("| 4.- Escribir el numero mas bajo.  |");
					System.out.println("+-----------------------------------+");
					try {
						System.out.println("El numero mas bajo es el " + lista.menor().imprimir());
					} catch (Exception err) {
						System.out.println("La lista est� vac�a.");
						System.out.println(err);
					}
					System.out.println("\n\n");
					break;
				case 5:
					num = null;
					System.out.println("+-----------------------------------+");
					System.out.println("| 5.- Borrar numero.                |");
					System.out.println("+-----------------------------------+");
					lista.borrar();
					System.out.println("Hecho!\n\n");
					break;
				case 6:
					System.out.println("+-----------------------------------+");
					System.out.println("| 6.- Ordenar.                      |");
					System.out.println("+-----------------------------------+");
					lista.ordenar ();
					System.out.println(lista.imprimir ()); // lista.toString ()
					System.out.println("\n\n");
					break;
				case 7:
					System.out.println("+-----------------------------------+");
					System.out.println("|            Hasta luego.           |");
					System.out.println("+-----------------------------------+");
					break;
				default:
					System.out.println("Esa no es una opcion no es valida. debe ser un numero del 1 al 7.");
			}
		} while (opcion != 7);
		
		teclado.close ();
	}

}
