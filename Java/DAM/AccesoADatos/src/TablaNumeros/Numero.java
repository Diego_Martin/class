package TablaNumeros;

public class Numero {
	private int numero;

	public Numero(int numero) {
		this.numero = numero;
	}
	
	public int getValor () {
		return this.numero;
	}
	
	public void setValor (int numero) {
		this.numero = numero;
	}
	
	public String imprimir () {
		return Integer.toString(this.numero);
	}
}
