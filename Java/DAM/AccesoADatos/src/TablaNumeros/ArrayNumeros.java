package TablaNumeros;

public class ArrayNumeros {
	private Numero Tabla[];
	private int posicion; // Longitud
	private int longitud; // Longitud maxima
	
	public ArrayNumeros (int longitud) {
		this.Tabla = new Numero[longitud];
		this.posicion = 0;
		this.longitud = longitud;
	}
	
	public void borrar () {
		if (this.posicion > 0) {
			this.posicion--;
			this.Tabla [this.posicion] = null;
		} else
			System.out.println("La longitud de la lista es 0, no puedes quitar elementos.");
	}
	
	public int getPosicion (Numero item) {
		if (this.posicion > 0) {
			Integer res = null;
			for (int i = 0; i < this.longitud; i++)
				if (this.Tabla [i].getValor () == item.getValor ()) {
					res = (Integer) i;
					break;
				}
			
			if (res != null) {
				System.out.println("Numero encontrado");
				return (int) res;
			} else
				System.out.println("Indice fuera de rango");
		} else
			System.out.println("La longitud de la lista es 0");
		return -1;
	}
	
	public String imprimir () {
		String res = "[";
		for (int i = 0; i < this.posicion; i++)
			if (i < this.posicion - 1)
				res += this.Tabla[i].imprimir () + ", ";
			else
				res += this.Tabla[i].imprimir ();
		res += "]";
		return res;
	}
	
	public void insertarNumero (Numero num) {
		if (this.longitud > this.posicion) {
			this.Tabla [this.posicion] = num;
			this.posicion++;
		} else
			System.out.println("La lista tiene todos los item ocupados.");
	}
	
	public Numero mayor () throws Exception {
		if (this.posicion > 0) {
			Numero res = this.Tabla[0];
			for (int i = 0; i < this.posicion; i++)
				if (this.Tabla[i].getValor() > res.getValor())
					res = this.Tabla[i];
			
			return res;
		} else
			throw new Exception ("La longitud de la lista es 0");
	}
	
	public Numero menor () throws Exception {
		if (this.posicion > 0) {
			Numero res = this.Tabla[0];
			for (int i = 0; i < this.posicion; i++)
				if (this.Tabla[i].getValor() < res.getValor())
					res = this.Tabla[i];
			
			return res;
		} else
			throw new Exception ("La longitud de la lista es 0");
	}
	
	public int length () {
		return this.posicion;
	}
	
	public void ordenar () {
		if (this.length () > 1)
			for (int i = 0; i < this.posicion; i++)
				for (int j = 0; j < (this.posicion - 1 - i); j++)
					if (this.Tabla [j].getValor () > this.Tabla [j + 1].getValor ()) {
						int temp = this.Tabla [j].getValor();
						this.Tabla [j].setValor (this.Tabla [j + 1].getValor ());
						this.Tabla [j + 1].setValor (temp);
					}
	}
}
