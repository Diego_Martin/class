package TablaPersona;

public class Menu {

	public static void main(String[] args) {
		Persona p = new Persona ("Diego", "123F", 23);
		Tabla tabla = new Tabla (5);
		tabla.add (p);
		tabla.add (new Persona ("Navarro", "456F", 22));
		System.out.println(tabla.toString());
	}

}
