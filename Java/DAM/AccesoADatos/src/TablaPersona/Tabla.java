package TablaPersona;

public class Tabla {
	private Persona tabla [];
	private int longitud;
	private int maxLongitud;
	
	public Tabla (int maxLongitud) {
		this.longitud = 0;
		this.maxLongitud = maxLongitud;
		this.tabla = new Persona [this.maxLongitud];
	}

	public void add (Persona p) {
		this.tabla [this.longitud] = p;
		this.longitud++;
	}
	
	public Persona get (int index) {
		return this.tabla [index];
	}
	
	@Override public String toString () {
		String res = "[";
		for (int i = 0; i < this.longitud; i++) {
			res += this.tabla [i].toString();
			if (i < this.longitud - 1)
				res += ", ";
		}
		res += "]";
		return res;
	}

}
