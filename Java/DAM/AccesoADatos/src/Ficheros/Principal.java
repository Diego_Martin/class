package Ficheros;

import java.io.File;
import java.io.IOException;

public class Principal {

	public static void main(String[] args) {
		File carpetaNueva = new File ("Datos");
		File fichero  = new File (carpetaNueva, "Fichero.txt" );
		File fichero2 = new File (carpetaNueva, "Fichero2.txt");
		File fichero3 = new File (carpetaNueva, "Fichero3.txt");
		
		if (carpetaNueva.mkdir()) {
			System.out.println("Se ha creado la carpeta: " + carpetaNueva.getName());
		}
		
		try {
			fichero.createNewFile();
			fichero2.createNewFile();
			fichero3.createNewFile();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}

}
