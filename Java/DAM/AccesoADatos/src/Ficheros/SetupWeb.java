package Ficheros;

import java.io.File;
import java.io.IOException;

public class SetupWeb {
	
	public static boolean esFichero (String nombre) {
		String ext[] = {"php", "jpg", "js", "css"};
		
		for (int i = 0; i < ext.length; i++)
			if (nombre.endsWith("." + ext[i]))
				return true;
		
		return false;
	}

	public static void main(String[] args) {
		/* TODO Crear el siguiente arbol de directorios
		 * - Proyecto
		 * 	- Estilos
		 *   - CSS3
		 *    - estilo3.css
		 *   - CSS4
		 *    - estilo1.css
		 *    - estilo2.css
		 * 	- Script
		 *   - js
		 *    - script1.js
		 *    - script2.js
		 *   - php
		 *    - cabecera.php
		 *    - modelo.php
		 * 	- Imagenes
		 * 	 - Alta
		 *    - paisaje.jpg
		 *   - Media
		 *    - paisaje.jpg
		 *   - Baja
		 *    - paisaje.jpg
		 */
		File nombres[] = new File [21];
		
		nombres [0] = new File ("./Proyecto");
		nombres [1] = new File (nombres [0], "Estilos");
		nombres [2] = new File (nombres [1], "CSS3");
		nombres [3] = new File (nombres [2], "estilo3.css");
		nombres [4] = new File (nombres [1], "CSS4");
		nombres [5] = new File (nombres [4], "estilo1.css");
		nombres [6] = new File (nombres [4], "estilo2.css");
		nombres [7] = new File (nombres [0], "Script");
		nombres [8] = new File (nombres [7], "js");
		nombres [9] = new File (nombres [8], "script1.js");
		nombres [10] = new File (nombres [8], "script2.js");
		nombres [11] = new File (nombres [7], "php");
		nombres [12] = new File (nombres [11], "cabecera.php");
		nombres [13] = new File (nombres [11], "modelo.php");
		nombres [14] = new File (nombres [0], "Imagenes");
		nombres [15] = new File (nombres [14], "Alta");
		nombres [16] = new File (nombres [15], "paisaje.jpg");
		nombres [17] = new File (nombres [14], "Media");
		nombres [18] = new File (nombres [17], "paisaje.jpg");
		nombres [19] = new File (nombres [14], "Baja");
		nombres [20] = new File (nombres [19], "paisaje.jpg");
		
		for (int i = 0; i < nombres.length; i++) {
			if (esFichero (nombres[i].getName())) {
				try {
					nombres[i].createNewFile();
				} catch (IOException err) {
					System.out.println("No se ha creado");
				}
			} else {
				nombres[i].mkdir();
			}
		}
		
		System.out.println("Hecho");
	}

}
