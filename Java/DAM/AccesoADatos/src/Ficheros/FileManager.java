package Ficheros;

//import java.util.ArrayList;

public class FileManager {
	
	private Archivo misArchivos [];
	private int posicion; // Longitud
	private int longitud; // Longitud maxima
	
	public FileManager (int longitud) {
		this.posicion = 0;
		this.longitud = longitud;
		this.misArchivos = new Archivo [this.longitud];
	}
	
	public void addFile (String nombre) {
		if (this.posicion < this.longitud) {
			this.misArchivos [this.posicion] = new Archivo (nombre);
			this.misArchivos [this.posicion].crearArchivo();
			this.posicion++;
		}
	}
}
