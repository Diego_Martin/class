package Ficheros;

import java.io.File;
import java.io.IOException;

public class Archivo {
	private String nombre;
	private File archivo;
	
	public Archivo (String nombre) {
		this.nombre = nombre;
		this.archivo = new File (nombre);
	}
	
	public void crearArchivo () {
		try {
			this.archivo.createNewFile ();
		} catch (IOException err) {
			System.err.println(err.getMessage());
		}
	}
}
