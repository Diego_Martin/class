package Ficheros;

import java.io.File;
import java.io.IOException;

public class SetupWeb2 {

	public static boolean esFichero (String nombre) {
		String ext[] = {"php", "jpg", "js", "css"};
		
		for (int i = 0; i < ext.length; i++)
			if (nombre.endsWith("." + ext[i]))
				return true;
		
		return false;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String nombres[] = {
				"./Proyecto",
				"./Proyecto/Estilos",
				"./Proyecto/Estilos/CSS3",
				"./Proyecto/Estilos/CSS3/estilo3.css",
				"./Proyecto/Estilos/CSS4",
				"./Proyecto/Estilos/CSS4/estilo1.css",
				"./Proyecto/Estilos/CSS4/estilo2.css",
				"./Proyecto/Script",
				"./Proyecto/Script/js",
				"./Proyecto/Script/js/script1.js",
				"./Proyecto/Script/js/script2.js",
				"./Proyecto/Script/php",
				"./Proyecto/Script/php/cabecera.php",
				"./Proyecto/Script/php/modelo.php",
				"./Proyecto/Imagenes",
				"./Proyecto/Imagenes/Alta",
				"./Proyecto/Imagenes/Alta/paisaje.jpg",
				"./Proyecto/Imagenes/Media",
				"./Proyecto/Imagenes/Media/paisaje.jpg",
				"./Proyecto/Imagenes/Baja",
				"./Proyecto/Imagenes/Baja/paisaje.jpg",
		};
		File archivos[] = new File [nombres.length];
		
		for (int i = 0; i < nombres.length; i++)
			archivos [i] = new File (nombres [i]);
		
		for (int i = 0; i < archivos.length; i++)
			if (esFichero (archivos[i].getName()))
				try {
					archivos[i].createNewFile();
				} catch (IOException err) {
					System.out.println("No se ha creado el archivo.");
				}
			else
				archivos[i].mkdir();
		
		System.out.println("Hecho");
	}

}
