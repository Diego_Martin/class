package TablaNumerosMejorado;

public class ArrayNumeros {
	private Numero lista[];
	private int posicion;
	private int longitud;
	
	public ArrayNumeros (int longitud) {
		this.longitud = longitud;
		this.lista = new Numero [this.longitud];
		this.posicion = 0;
		for (int i = 0; i < this.longitud; i++)
			this.lista [i] = null;
	}
	
	public Numero get (int index) throws TeHasPasadoException, NoPuedesException {
		if (this.posicion > 0) {
			if (index < this.posicion && index > -1)
				return this.lista [index];
			else if (index < 0 && index >= (this.posicion * -1))
				return this.lista [this.posicion + index];
			else
				throw new TeHasPasadoException ("Indice fuera de rango.");
		} else 
			throw new NoPuedesException ("No hay elementos en la lista.");
	}
	
	public void add (Numero num) throws NoPuedesException {
		if (this.posicion < this.longitud) {
			this.lista [this.posicion] = num;
			this.posicion ++;
		} else
			throw new NoPuedesException ("Has indicado que la longitud m�xima de la lista es " + this.longitud);
	}
	
	public void add (int num) throws NoPuedesException {
		Numero nume = new Numero (num);
		this.add(nume);
	}
	
	public void add (Integer num) throws NoPuedesException {
		Numero nume = new Numero (num);
		this.add(nume);
	}
	
	public void add (double num) throws NoPuedesException {
		Numero nume = new Numero (num);
		this.add(nume);
	}
	
	public void add (Double num) throws NoPuedesException {
		Numero nume = new Numero (num);
		this.add(nume);
	}
	
	public void add (float num) throws NoPuedesException {
		Numero nume = new Numero (num);
		this.add(nume);
	}
	
	public void add (Float num) throws NoPuedesException {
		Numero nume = new Numero (num);
		this.add(nume);
	}
	
	public void pop () throws NoPuedesException {
		if (this.posicion > 0) {
			this.posicion --;
			this.lista [this.posicion] = null;
		} else
			throw new NoPuedesException ("La longitud de la lista es 0, no puedes quitar elementos.");
	}
	
	public Numero shift () throws NoPuedesException {
		if (this.posicion > 0) {
			Numero res = this.lista [0];
			for (int i = 1; i < this.longitud; i ++)
				if (i != this.longitud - 1)
					this.lista [i - 1] = this.lista [i];
				else
					this.lista [i] = null;
			this.posicion --;
			return res;
		} else
			throw new NoPuedesException ("La longitud de la lista es 0, no puedes quitar elementos.");
	}
	
	public Numero remove (int index) throws TeHasPasadoException, NoPuedesException {
		if (this.posicion > 0) {
			Numero res = null;
			int mi_index = 0;
			if (index < this.posicion && index > -1) {
				res = this.lista [index];
				mi_index = index;
			} else if (index < 0 && index >= (this.posicion * -1)) {
				res = this.lista [this.posicion + index];
				mi_index = -1 * index;
			} else
				throw new TeHasPasadoException ("Indice fuera de rango.");
			
			this.lista [mi_index] = null;
			for (int i = mi_index; i < this.longitud - 1; i++)
				if (i < this.longitud - 1)
					this.lista [i] = this.lista [i + 1];
				else
					this.lista [i] = null;
			
			//res = this.lista [mi_index];
			this.posicion --;
			return res;
		} else 
			throw new NoPuedesException ("La longitud de la lista es 0, no puedes quitar elementos.");
	}
	
	public Numero remove (Integer index) throws TeHasPasadoException, NoPuedesException {
		return this.remove ((int) index);
	}
	
	public void sort () {
		if (this.length () > 1)
			for (int i = 0; i < this.posicion; i++)
				for (int j = 0; j < (this.posicion - 1 - i); j++)
					if (this.lista [j].value () > this.lista [j + 1].value ()) {
						double temp = this.lista [j].value ();
						this.lista [j].set (this.lista [j + 1].value ());
						this.lista [j + 1].set (temp);
					}
	}
	
	public int length () {
		return this.posicion;
	}
	
	public int size () {
		return this.length ();
	}
	
	public int indexOf (Numero item) throws TeHasPasadoException, NoPuedesException {
		if (this.posicion > 0) {
			Integer res = null;
			for (int i = 0; i < this.longitud; i++)
				if (this.lista [i].value () == item.value ()) {
					res = (Integer) i;
					break;
				}
			
			if (res != null) {
				System.out.println("Numero encontrado");
				return (int) res;
			} else
				throw new TeHasPasadoException ("Indice fuera de rango");
		} else
			throw new NoPuedesException ("La longitud de la lista es 0");
	}
	
	public Numero min () throws NoPuedesException {
		if (this.posicion > 0) {
			Numero res = this.lista[0];
			for (int i = 0; i < this.posicion; i++)
				if (this.lista[i] != null)
					if (res.value () > this.lista[i].value ())
						res = this.lista[i];
			return res;
		} else
			throw new NoPuedesException ("La longitud de la lista es 0");
	}
	
	public Numero max () throws NoPuedesException {
		if (this.posicion > 0) {
			Numero res = this.lista[0];
			for (int i = 0; i < this.posicion; i++)
				if (this.lista[i] != null)
					if (res.value () < this.lista[i].value ())
						res = this.lista[i];
			return res;
		} else 
			throw new NoPuedesException ("La longitud de la lista es 0");
	}
	
	/*public String imprimir () {
		String res = "[";
		for (int i = 0; i < this.longitud; i++)
			if (i < this.longitud - 1)
				if (this.lista[i] == null)
					res += "null, ";
				else
					res += this.lista[i].toString() + ", ";
			else
				if (this.lista[i] == null)
					res += "null";
				else
					res += this.lista[i].toString();
		res += "]";
		return res;
	}*/
	
	@Override
	public String toString () {
		String res = "[";
		for (int i = 0; i < this.posicion; i++)
			if (i < this.posicion - 1)
				res += this.lista[i].toString() + ", ";
			else
				res += this.lista[i].toString();
		res += "]";
 		return res;
	}
}
