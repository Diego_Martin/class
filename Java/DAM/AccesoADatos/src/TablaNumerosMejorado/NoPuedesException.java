package TablaNumerosMejorado;

@SuppressWarnings("serial")
public class NoPuedesException extends Exception {

	public NoPuedesException (String msg) {
		super (msg);
	}
}
