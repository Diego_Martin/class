package TablaNumerosMejorado;

public class Numero {
	private double valor;
	
	public Numero (int valor) {
		this.valor = (double) valor;
	}
	
	public Numero (Integer valor) {
		this.valor = (double) valor;
	}
	
	public Numero (float valor) {
		this.valor = (double) valor;
	}
	
	public Numero (Float valor) {
		this.valor = (double) valor;
	}
	
	public Numero (double valor) {
		this.valor = valor;
	}
	
	public Numero (Double valor) {
		this.valor = (double) valor;
	}
	
	public void set (int valor) {
		this.valor = (double) valor;
	}
	
	public void set (Integer valor) {
		this.valor = (double) valor;
	}
	
	public void set (float valor) {
		this.valor = (double) valor;
	}
	
	public void set (Float valor) {
		this.valor = (double) valor;
	}
	
	public void set (double valor) {
		this.valor = valor;
	}
	
	public void set (Double valor) {
		this.valor = (double) valor;
	}
	
	public int toInt () {
		return (int) this.valor;
	}
	
	public Integer toInteger () {
		return (Integer) (int) this.valor;
	}
	
	public float toFloat () {
		return (float) this.valor;
	}
	
	public Float toFFloat () {
		return (Float) (float) this.valor;
	}
	
	public double toDouble () {
		return this.valor;
	}
	
	public Double toDDouble () {
		return (Double) this.valor;
	}
	
	public double value () {
		return this.valor;
	}
	
	@Override
	public String toString () {
		if (this.valor - ((int) this.valor) > 0)
			return Double.toString(this.valor);
		else
			return Integer.toString((int) this.valor);
	}
}
