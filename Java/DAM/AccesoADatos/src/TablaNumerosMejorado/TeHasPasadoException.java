package TablaNumerosMejorado;

@SuppressWarnings("serial")
public class TeHasPasadoException extends Exception {

	public TeHasPasadoException (String msg) {
		super (msg);
	}
}
