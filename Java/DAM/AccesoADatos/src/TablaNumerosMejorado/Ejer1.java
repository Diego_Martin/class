package TablaNumerosMejorado;

import java.util.Scanner;

public class Ejer1 {

	public static void main(String[] args) {

		int opcion = 0, numeroInt = 0;
		Scanner teclado = new Scanner (System.in);
		String aux = "";
		boolean repetir = true;
		
		System.out.println("+-------------------------------------------------+");
		do {
			repetir = false;
			try {
				System.out.print ("| Cuantos elementos como m�ximo tendr� tu array?: ");
				aux = teclado.nextLine ();
				
				opcion = Integer.parseInt (aux);
				repetir = true;
				
			} catch (NumberFormatException err) { // De java.lang
				System.out.println ("| Debes escribir un numero entero.");
			}
		} while (!repetir);
		
		repetir = false;
		ArrayNumeros lista = new ArrayNumeros (opcion);
		
		do {
			Numero num = null;
			// System.out.println(lista.imprimir());
			System.out.println("+-----------------------------------+");
			System.out.println("| 1.- Introducir numero.            |");
			System.out.println("| 2.- Escribir numeros.             |");
			System.out.println("| 3.- Escribir el numero mas alto.  |");
			System.out.println("| 4.- Escribir el numero mas bajo.  |");
			System.out.println("| 5.- Borrar x numero.              |");
			System.out.println("| 6.- Ordenar.                      |");
			System.out.println("| 7.- Borrar el primer elemento     |");
			System.out.println("| 8.- Borrar el ultimo elemento     |");
			System.out.println("| 9.- Mostar el numero en x posicion|");
			System.out.println("| 10.- Salir.                       |");
			System.out.println("+-----------------------------------+");
			do {
				repetir = false;
				try {
					System.out.print ("| Que quieres hacer?: ");
					aux = teclado.nextLine ();
					
					opcion = Integer.parseInt (aux);
					repetir = true;
					
				} catch (NumberFormatException err) { // De java.lang
					System.out.println ("| Debes escribir un numero entero.");
				}
			} while (!repetir);
			
			repetir = false;
			switch (opcion) {
				case 1:
					System.out.println("+-----------------------------------+");
					System.out.println("| 1.- Introducir numero.            |");
					System.out.println("+-----------------------------------+");
					do {
						repetir = false;
						try {
							System.out.print ("| Introduce el numero para a�adir: ");
							aux = teclado.nextLine ();
							
							num = new Numero(Double.parseDouble (aux));
							repetir = true;
							
						} catch (NumberFormatException err) { // De java.lang
							System.out.println ("| Debes escribir un numero.");
						}
					} while (!repetir);
					
					repetir = false;
					try {
						lista.add(num);
					} catch (NoPuedesException err) {
						System.out.println(err);
					}
					System.out.println("Hecho!");
					System.out.println("\n\n");
					break;
				case 2:
					System.out.println("+-----------------------------------+");
					System.out.println("| 2.- Escribir numeros.             |");
					System.out.println("+-----------------------------------+");
					System.out.println(lista.toString ()); // lista.toString ()
					System.out.println("\n");
					System.out.println("Hecho!");
					System.out.println("\n\n");
					break;
				case 3:
					System.out.println("+-----------------------------------+");
					System.out.println("| 3.- Escribir el numero mas alto.  |");
					System.out.println("+-----------------------------------+");
					try {
						num = lista.max();
					} catch (NoPuedesException err) {
						System.out.println(err);
					}
					
					System.out.println("El numero mas alto es el " + num.toString());
					System.out.println("\n\n");
					break;
				case 4:
					System.out.println("+-----------------------------------+");
					System.out.println("| 4.- Escribir el numero mas bajo.  |");
					System.out.println("+-----------------------------------+");
					try {
						num = lista.min();
					} catch (NoPuedesException err) {
						System.out.println(err);
					}
					
					System.out.println("El numero mas bajo es el " + num.toString());
					System.out.println("\n\n");
					break;
				case 5:
					num = null;
					System.out.println("+-----------------------------------+");
					System.out.println("| 5.- Borrar x numero.              |");
					System.out.println("+-----------------------------------+");
					if (lista.size () > 0) {
						lista.toString (); // lista.toString ()
						System.out.println("\n\n");
						do {
							repetir = false;
							try {
								System.out.print ("| Que numero quieres borrar?: ");
								aux = teclado.nextLine ();
								
								num = new Numero(Double.parseDouble (aux));
								repetir = true;
								
							} catch (NumberFormatException err) { // De java.lang
								System.out.println ("| Debes escribir un numero.");
							}
						} while (!repetir);
						
						repetir = false;
						try {
							lista.remove (lista.indexOf (num));
						} catch (TeHasPasadoException err) {
							System.out.println(err);
						} catch (NoPuedesException err) {
							System.out.println(err);
						}
						System.out.println("Hecho!");
					} else
						System.out.println("La lista esta vacia!");
					System.out.println("\n\n");
					break;
				case 6:
					System.out.println("+-----------------------------------+");
					System.out.println("| 6.- Ordenar.                      |");
					System.out.println("+-----------------------------------+");
					lista.sort ();
					System.out.println(lista.toString ()); // lista.toString ()
					System.out.println("\n\n");
					break;
				case 7:
					System.out.println("+-----------------------------------+");
					System.out.println("| 7.- Borrar el primer elemento     |");
					System.out.println("+-----------------------------------+");
					try {
						num = lista.shift();
					} catch (NoPuedesException err) {
						System.out.println(err);
					}
					System.out.println("Hecho!\n\n");
					break;
				case 8:
					System.out.println("+-----------------------------------+");
					System.out.println("| 8.- Borrar el ultimo elemento     |");
					System.out.println("+-----------------------------------+");
					try {
						lista.pop();
					} catch (NoPuedesException err) {
						System.out.println(err);
					}
					System.out.println("Hecho!\n\n");
					break;
				case 9:
					System.out.println("+-----------------------------------+");
					System.out.println("| 9.- Mostar el numero en x posicion|");
					System.out.println("+-----------------------------------+");
					do {
						repetir = false;
						try {
							System.out.print ("| Que numero quieres mostrar?: ");
							aux = teclado.nextLine ();
							
							numeroInt = Integer.parseInt (aux);
							repetir = true;
							
						} catch (NumberFormatException err) { // De java.lang
							System.out.println ("| Debes escribir un numero.");
						}
					} while (!repetir);
					
					repetir = false;
					try {
						System.out.println("| El numero es: " + lista.get(numeroInt).toString());
					} catch (NoPuedesException err) {
						System.out.println(err);
					} catch (TeHasPasadoException err) {
						System.out.println(err);
					}
					break;
				case 10:
					System.out.println("+-----------------------------------+");
					System.out.println("|            Hasta luego.           |");
					System.out.println("+-----------------------------------+");
					System.exit (0);
				default:
					System.out.println("Esa no es una opcion no es valida. debe ser un numero del 1 al 10.");
			}
		} while (opcion != 10);
		teclado.close ();

	}
	
}
