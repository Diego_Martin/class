package Diego_Martin_Recuperacion;

import java.util.InputMismatchException;
import java.util.Scanner;

import Diego_Martin_Recuperacion.Barco.Tipo;

import java.util.ArrayList;


public class menu {
	private static ArrayList<Avion> aviones = new ArrayList<Avion>();
	private static ArrayList<Barco> barcos = new ArrayList<Barco>();
	private static ArrayList<Coche> coches = new ArrayList<Coche>();
	
	public static void main(String[] args) {
		int opcion;
		boolean salir = false;
		Scanner teclado = new Scanner (System.in);
		
		do {
			System.out.println("+--------------------------+");
			System.out.println("|            MENU          |");
			System.out.println("+--------------------------+");
			System.out.println("| 1.- A�adir Coche");
			System.out.println("| 2.- A�adir Barco");
			System.out.println("| 3.- A�adir Avion");
			System.out.println("| 4.- Comparar edad");
			System.out.println("| 5.- Cambiar");
			System.out.print("| Que quieres hacer?: ");
			try {
				opcion = teclado.nextInt();
				switch (opcion) {
					case 1:
						newCoche ();
						break;
					case 2:
						newBarco ();
						break;
					case 3:
						newAvion ();
						break;
					case 4:
						comparar ();
						break;
					case 5:
						cambio ();
						break;
					default:
						System.out.println("Esa no es una opcion valida");
						
				}
			} catch (InputMismatchException err) {
				System.out.println("El valor de entrada debe ser un n�mero.");
			}
		} while (!salir);
		mostrarVehiculos ();
		teclado.close();
	}
	
	public static void cambio () {
		Scanner teclado = new Scanner (System.in);
		int opcion = 0;
		boolean siguiente = false;
		String eleccion = "";
		System.out.println("Estos son tus vehiculos:");
		mostrarVehiculos ();
		System.out.println("Cual quieres cambiar?: ");
		
		do {
			try {
				System.out.print("Que veh�culo quieres modificar?: ");
				opcion = teclado.nextInt();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		if (opcion < coches.size()) {
			eleccion = "coche";
		} else if (opcion < (coches.size() + barcos.size())) {
			opcion -= coches.size();
			eleccion = "barco";
		} else {
			eleccion = "avion";
			opcion -= (coches.size() + barcos.size());
		}
		
		if (eleccion == "coche") {
			
		}
		if (eleccion == "coche") {
			
		}
		if (eleccion == "coche") {
			
		}
		teclado.close();
	}
	
	public static void comparar () {
		String tipoMasAntiguo = "";
		int anioDelMasAntiguo = 0;
		int indexMasAntiguo = 0;
		for (int i = 0; i < coches.size(); i++) {
			tipoMasAntiguo = "coche";
			if (anioDelMasAntiguo < coches.get(i).getYearFabricacion()) {
				anioDelMasAntiguo = coches.get(i).getYearFabricacion();
				indexMasAntiguo = i;
			}
		}
		for (int i = 0; i < barcos.size(); i++) {
			if (anioDelMasAntiguo < barcos.get(i).getYearFabricacion()) {
				anioDelMasAntiguo = barcos.get(i).getYearFabricacion();
				tipoMasAntiguo = "barco";
				indexMasAntiguo = i;
			}
		}
		for (int i = 0; i < coches.size(); i++) {
			if (anioDelMasAntiguo < aviones.get(i).getYearFabricacion()) {
				anioDelMasAntiguo = aviones.get(i).getYearFabricacion();
				tipoMasAntiguo = "avion";
				indexMasAntiguo = i;
			}
		}
		System.out.println("+--------------------------+");
		System.out.println("|         ANTIGUO          |");
		System.out.println("+--------------------------+");
		if (tipoMasAntiguo == "coche")
			System.out.println(coches.get(indexMasAntiguo).toString());
		if (tipoMasAntiguo == "barco")
			System.out.println(barcos.get(indexMasAntiguo).toString());
		if (tipoMasAntiguo == "avion")
			System.out.println(aviones.get(indexMasAntiguo).toString());
	}
	
	public static void mostrarVehiculos () {
		int contador = 0;
		for (int i = 0; i < coches.size(); i++) {
			contador++;
			System.out.println(contador + ".- " + coches.get(i).toString());
		}
		for (int i = 0; i < barcos.size(); i++) {
			contador++;
			System.out.println(contador + ".- " + barcos.get(i).toString());
		}
		for (int i = 0; i < aviones.size(); i++) {
			contador++;
			System.out.println(contador + ".- " + aviones.get(i).toString());
		}
	}
	
	public static void newCoche () {
		Scanner teclado = new Scanner (System.in);
		boolean siguiente = false;
		String marca = "", color = "", bastidor = "";
		int km = 0, anio = 0;
		boolean electrico = false;
		System.out.println("+--------------------------+");
		System.out.println("|          COCHE           |");
		System.out.println("+--------------------------+");
		
		do {
			try {
				System.out.print("Que marca es?: ");
				marca = teclado.nextLine();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;

		do {
			try {
				System.out.print("Que color es?: ");
				color = teclado.nextLine();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("Que numero de bastidor tiene?: ");
				bastidor = teclado.nextLine();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("Cuantos kilometros tiene?: ");
				km = teclado.nextInt();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("De que a�o es?: ");
				anio = teclado.nextInt();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;

		do {
			try {
				System.out.print("Es electrico? (Si/No): ");
				String res = "";
				res = teclado.nextLine();
				siguiente = true;
				if (res.toLowerCase() == "si") {
					electrico = true;
				} else if (res.toLowerCase() == "no") {
					electrico = false;
				} else {
					siguiente = false; 
				}
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		Coche resultado = new Coche (marca, color, bastidor, km, anio, electrico);
		coches.add(resultado);
		teclado.close();
	}
	
	public static void newBarco () {
		Scanner teclado = new Scanner (System.in);
		boolean siguiente = false;
		String marca = "", color = "", bastidor = "";
		int km = 0, anio = 0, eslora = 0, calado = 0;
		Tipo tipo = null;
		System.out.println("+--------------------------+");
		System.out.println("|           BARCO          |");
		System.out.println("+--------------------------+");
		
		do {
			try {
				System.out.print("Que marca es?: ");
				marca = teclado.nextLine();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;

		do {
			try {
				System.out.print("Que color es?: ");
				color = teclado.nextLine();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("Que numero de bastidor tiene?: ");
				bastidor = teclado.nextLine();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("Cuantos kilometros tiene?: ");
				km = teclado.nextInt();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("De que a�o es?: ");
				anio = teclado.nextInt();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("Cuanto mide de calado?: ");
				calado = teclado.nextInt();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("Cuanto mide de eslora?: ");
				eslora = teclado.nextInt();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("Cuanto mide de eslora?: ");
				tipo = Tipo.valueOf(teclado.next());
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		Barco resultado = new Barco (marca, color, bastidor, km, anio, eslora, calado, tipo);
		barcos.add(resultado);
		teclado.close();
	}
	
	public static void newAvion () {
		Scanner teclado = new Scanner (System.in);
		boolean siguiente = false;
		String marca = "", color = "", bastidor = "";
		double velocidad = 0.0;
		int km = 0, anio = 0;
		byte motores = 0;
		boolean combate = false;
		System.out.println("+--------------------------+");
		System.out.println("|           AVION          |");
		System.out.println("+--------------------------+");
		
		do {
			try {
				System.out.print("Es de combate? (Si/No): ");
				String res = "";
				res = teclado.nextLine();
				siguiente = true;
				if (res.toLowerCase() == "si") {
					combate = true;
				} else if (res.toLowerCase() == "no") {
					combate = false;
				} else {
					siguiente = false; 
				}
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("Que marca es?: ");
				marca = teclado.nextLine();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;

		do {
			try {
				System.out.print("Que color es?: ");
				color = teclado.nextLine();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("Que numero de bastidor tiene?: ");
				bastidor = teclado.nextLine();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("Cuantos kilometros tiene?: ");
				km = teclado.nextInt();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		do {
			try {
				System.out.print("De que a�o es?: ");
				anio = teclado.nextInt();
				siguiente = true;
			} catch (InputMismatchException err) {
				System.out.println("Ese tipo de dato no es valido");
			}
		} while (!siguiente);
		siguiente = false;
		
		if (combate) {
			Avion resultado = new Avion (marca, color, bastidor, km, anio, combate);
			aviones.add(resultado);
		} else {
			do {
				try {
					System.out.print("Cuantos motores tiene?: ");
					motores = teclado.nextByte();
					siguiente = true;
				} catch (InputMismatchException err) {
					System.out.println("Ese tipo de dato no es valido");
				}
			} while (!siguiente);
			siguiente = false;
			
			do {
				try {
					System.out.print("De que a�o es?: ");
					velocidad = teclado.nextDouble();
					siguiente = true;
				} catch (InputMismatchException err) {
					System.out.println("Ese tipo de dato no es valido");
				}
			} while (!siguiente);
			siguiente = false;
			
			Avion resultado = new Avion (marca, color, bastidor, km, anio, motores, velocidad, combate);
			aviones.add(resultado);
		}
		teclado.close();
	}
}
