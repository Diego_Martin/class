package Diego_Martin_Recuperacion;

public abstract class Vehiculo {
	protected String marca;
	protected String color;
	protected String numBastidor;
	protected int kilometros;
	protected int yearFabricacion;
	
	public Vehiculo(String marca, String color, String numBastidor, int kilometros, int yearFabricacion) {
		this.marca = marca;
		this.color = color;
		this.numBastidor = numBastidor;
		this.kilometros = kilometros;
		this.yearFabricacion = yearFabricacion;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getNumBastidor() {
		return numBastidor;
	}

	public void setNumBastidor(String numBastidor) {
		this.numBastidor = numBastidor;
	}

	public int getKilometros() {
		return kilometros;
	}

	public void setKilometros(int kilometros) {
		this.kilometros = kilometros;
	}

	public int getYearFabricacion() {
		return yearFabricacion;
	}

	public void setYearFabricacion(int yearFabricacion) {
		this.yearFabricacion = yearFabricacion;
	}

	@Override
	public String toString() {
		return "Vehiculo [marca=" + marca + ", color=" + color + ", numBastidor=" + numBastidor + ", kilometros=" + kilometros + ", yearFabricacion=" + yearFabricacion + "]";
	}
}
