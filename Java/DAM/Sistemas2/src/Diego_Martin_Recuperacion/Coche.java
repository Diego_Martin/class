package Diego_Martin_Recuperacion;

import java.util.Calendar;

public class Coche extends Vehiculo {
	private boolean electrico;
	private boolean antiguo;
	
	public Coche(String marca, String color, String numBastidor, int kilometros, int yearFabricacion, boolean electrico) {
		super(marca, color, numBastidor, kilometros, yearFabricacion);
		this.electrico = electrico;
		this.antiguo = this.isAntiguo();
	}

	public boolean isElectrico() {
		return electrico;
	}

	public void setElectrico(boolean electrico) {
		this.electrico = electrico;
	}

	public boolean isAntiguo() {
		return ((Calendar.YEAR - super.getYearFabricacion()) > 25);
	}

	public void setAntiguo(boolean antiguo) {
		this.antiguo = antiguo;
	}

	@Override
	public String toString() {
		return "Coche [electrico=" + this.electrico + ", antiguo=" + this.antiguo + ", marca=" + super.getMarca() + ", color=" + super.getColor() + ", numBastidor=" + super.getNumBastidor() + ", kilometros=" + super.getKilometros() + ", yearFabricacion=" + super.getYearFabricacion() + "]";
	}
	
}
