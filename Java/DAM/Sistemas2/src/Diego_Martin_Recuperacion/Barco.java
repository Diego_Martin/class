package Diego_Martin_Recuperacion;

public class Barco extends Vehiculo {
	public enum Tipo {Vela, Pesca, Pasajeros};
	private int eslora;
	private int calado;
	private Tipo tipo;
	
	public Barco(String marca, String color, String numBastidor, int kilometros, int yearFabricacion, int eslora, int calado, Tipo tipo) {
		super(marca, color, numBastidor, kilometros, yearFabricacion);
		this.eslora = eslora;
		this.calado = calado;
		this.tipo = tipo;
	}

	public int getEslora() {
		return eslora;
	}

	public void setEslora(int eslora) {
		this.eslora = eslora;
	}

	public int getCalado() {
		return calado;
	}

	public void setCalado(int calado) {
		this.calado = calado;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Barco [eslora=" + this.eslora + ", calado=" + this.calado + ", tipo=" + this.tipo + ", marca=" + super.getMarca() + ", color=" + super.getColor() + ", numBastidor=" + super.getNumBastidor() + ", kilometros=" + super.getKilometros() + ", yearFabricacion=" + super.getYearFabricacion() + "]";
	}
	
}
