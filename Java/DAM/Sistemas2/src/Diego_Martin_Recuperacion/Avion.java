package Diego_Martin_Recuperacion;

public class Avion extends Vehiculo {
	private byte motores;
	private double velocidadMax;
	private boolean combate;
	
	public Avion(String marca, String color, String numBastidor, int kilometros, int yearFabricacion, byte motores, double velocidadMax, boolean combate) {
		super(marca, color, numBastidor, kilometros, yearFabricacion);
		this.motores = motores;
		this.velocidadMax = velocidadMax;
		this.combate = combate;
	}

	public Avion(String marca, String color, String numBastidor, int kilometros, int yearFabricacion, boolean combate) {
		super(marca, color, numBastidor, kilometros, yearFabricacion);
		this.combate = combate;
		this.motores = -1;
		this.velocidadMax = -1.0;
	}

	public byte getMotores() {
		return motores;
	}

	public void setMotores(byte motores) {
		this.motores = motores;
	}

	public double getVelocidadMax() {
		return velocidadMax;
	}

	public void setVelocidadMax(double velocidadMax) {
		this.velocidadMax = velocidadMax;
	}

	public boolean isCombate() {
		return combate;
	}

	public void setCombate(boolean combate) {
		this.combate = combate;
	}

	@Override
	public String toString() {
		return "Avion [marca=" + super.getMarca() + ", color=" + super.getColor() + ", numBastidor=" + super.getNumBastidor() + ", kilometros=" + super.getKilometros() + ", yearFabricacion=" + super.getYearFabricacion() + "motores=" + this.motores + ", velocidadMax=" + this.velocidadMax + ", combate=" + this.combate + "]";
	}
	
}
