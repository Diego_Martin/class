package Diego_Martin_Examen;

public class Coche extends Vehiculo {
	protected boolean electrico;
	private boolean antiguo;
	
	public Coche (String marca, String color, String numeroBastidor, int kilometros, int yearFabricacion, boolean electrico) {
		super (marca, color, numeroBastidor, kilometros, yearFabricacion);
		this.electrico = electrico;
	}
	
	public boolean isElectrico() {
		return electrico;
	}

	public void setElectrico(boolean electrico) {
		this.electrico = electrico;
	}

	public boolean isAntiguo() {
		return antiguo;
	}

	public void setAntiguo(boolean antiguo) {
		this.antiguo = antiguo;
	}

	public String toString () {
		return "Coche (" + super.toString() + ", electrico = " + this.electrico + ", antiguo = " + this.antiguo + ")";
	}
}
