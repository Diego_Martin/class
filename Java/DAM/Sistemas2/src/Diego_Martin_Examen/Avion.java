package Diego_Martin_Examen;

public class Avion extends Vehiculo {
	private byte motores;
	private double velocidadMax;
	private boolean combate;
	
	public Avion (String marca, String color, String numeroBastidor, int kilometros, int yearFabricacion, byte motores, double velocidadMax, boolean combate) {
		super(marca, color, numeroBastidor, kilometros, yearFabricacion);
		this.motores = motores;
		this.velocidadMax = velocidadMax;
		this.combate = combate;
	}
	
	public Avion (String marca, String color, String numeroBastidor, int kilometros, int yearFabricacion, boolean combate) {
		super(marca, color, numeroBastidor, kilometros, yearFabricacion);
		this.combate = combate;
	}
	
	public byte getMotores() {
		return motores;
	}

	public void setMotores(byte motores) {
		this.motores = motores;
	}

	public double getVelocidadMax() {
		return velocidadMax;
	}

	public void setVelocidadMax(double velocidadMax) {
		this.velocidadMax = velocidadMax;
	}

	public boolean isCombate() {
		return combate;
	}

	public void setCombate(boolean combate) {
		this.combate = combate;
	}

	public String toString () {
		return "Avion (" + super.toString() + ", motores = " + this.motores + ", velocidadMax = " + this.velocidadMax + ", combate = " + this.combate + ")";
	}
}
