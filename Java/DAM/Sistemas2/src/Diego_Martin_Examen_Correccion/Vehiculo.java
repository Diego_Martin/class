package Diego_Martin_Examen_Correccion;

public abstract class Vehiculo {
	public String marca;
	public String color;
	public String numeroBastidor;
	public int kilometros;
	protected int yearFabricacion;
	
	public Vehiculo (String marca, String color, String numeroBastidor, int kilometros, int yearFabricacion) {
		this.marca = marca;
		this.color = color;
		this.numeroBastidor = numeroBastidor;
		this.kilometros = kilometros;
		this.yearFabricacion = yearFabricacion;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getNumeroBastidor() {
		return numeroBastidor;
	}

	public void setNumeroBastidor(String numeroBastidor) {
		this.numeroBastidor = numeroBastidor;
	}

	public int getKilometros() {
		return kilometros;
	}

	public void setKilometros(int kilometros) {
		this.kilometros = kilometros;
	}

	public int getYearFabricacion() {
		return yearFabricacion;
	}

	public void setYearFabricacion(int yearFabricacion) {
		this.yearFabricacion = yearFabricacion;
	}
	
	@Override
	public String toString () {
		return "Vehiculo (marca = " + this.marca + ", color = " + this.color + ", numeroBastidor = " + this.numeroBastidor + ", kilometros = " + this.kilometros + ", yearFabricacion = " + this.yearFabricacion + ")";
	}
}
