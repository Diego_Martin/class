package Diego_Martin_Examen_Correccion;

import java.util.Calendar;

public class Coche extends Vehiculo {
	protected boolean electrico;
	private boolean antiguo;
	
	public Coche (String marca, String color, String numeroBastidor, int kilometros, int yearFabricacion, boolean electrico) {
		super (marca, color, numeroBastidor, kilometros, yearFabricacion);
		this.electrico = electrico;
		this.antiguo = this.isAntiguo();
	}
	
	public boolean isElectrico() {
		return electrico;
	}

	public void setElectrico(boolean electrico) {
		this.electrico = electrico;
	}

	public boolean isAntiguo() {
		return ((Calendar.YEAR - this.yearFabricacion) > 25);
	}

	public void setAntiguo(boolean antiguo) {
		this.antiguo = antiguo;
	}

	@Override
	public String toString() {
		return "Coche [" + super.toString() + ", electrico=" + electrico + ", antiguo=" + antiguo + "]";
	}
	
}
