package jotasoup;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import javax.swing.SwingConstants;
//import org.jsoup.select.Elements;

public class Titular {

	private JFrame frame;
	private JTextField InputTitular;
	private JLabel OutputTitular;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Titular window = new Titular();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Titular() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 890, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel LabelTema = new JLabel("Introduce el tema del titular: ");
		LabelTema.setHorizontalAlignment(SwingConstants.RIGHT);
		LabelTema.setBounds(10, 61, 178, 14);
		frame.getContentPane().add(LabelTema);
		
		InputTitular = new JTextField();
		InputTitular.setBounds(198, 58, 666, 20);
		frame.getContentPane().add(InputTitular);
		InputTitular.setColumns(10);
		
		JLabel LabelTitular = new JLabel("Primer titular obtenido: ");
		LabelTitular.setHorizontalAlignment(SwingConstants.RIGHT);
		LabelTitular.setBounds(10, 94, 178, 14);
		frame.getContentPane().add(LabelTitular);
		
		OutputTitular = new JLabel("");
		OutputTitular.setBounds(198, 94, 666, 14);
		frame.getContentPane().add(OutputTitular);
		
		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.setBounds(10, 119, 854, 23);
		frame.getContentPane().add(btnNewButton);
		
		btnNewButton.addActionListener(new ActionListener () {
			public void actionPerformed (ActionEvent arg) {
				if (InputTitular.getText().isEmpty())
					JOptionPane.showMessageDialog(null, "No hay ningun tema para buscar.", "Error", 0);
				else
					try {
						buscar();
					} catch (IOException err) {
						System.out.println("Ha habido algun error");
					}
			}
		});
	}
	
	public void buscar () throws IOException {
		String tema = InputTitular.getText();
		Document doc = Jsoup.connect("https://news.google.com/search?q=" + tema + "&hl=es-419&gl=US&ceid=US%3Aes-419").get();
        Element result = doc.select("h3.ipQwMb.ekueJc.RD0gLb > a").first();
        OutputTitular.setText(result.text());
        System.out.println(result.text());
	}
}
