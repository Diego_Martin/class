package jotasoup;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class TraductorVentana {

	private JFrame frame;
	private JTextField InputPalabra;
	private JTextField output;
	private JLabel LabelPalabraATraducir;
	private JLabel lblNewLabel;
	private JButton btnTraducir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TraductorVentana window = new TraductorVentana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TraductorVentana() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 9));
		frame.setResizable(false);
		frame.setLocationRelativeTo(null); // centra la ventana en la pantalla
		frame.setBounds(100, 100, 395, 190);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel LabelPalabraTraducida = new JLabel("Palabra traducida:");
		LabelPalabraTraducida.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LabelPalabraTraducida.setBounds(27, 76, 90, 14);
		frame.getContentPane().add(LabelPalabraTraducida);
		
		InputPalabra = new JTextField();
		InputPalabra.setBounds(120, 48, 227, 20);
		frame.getContentPane().add(InputPalabra);
		InputPalabra.setColumns(10);
		
		LabelPalabraATraducir = new JLabel("Palabra a traducir:");
		LabelPalabraATraducir.setFont(new Font("Tahoma", Font.PLAIN, 10));
		LabelPalabraATraducir.setBounds(27, 51, 90, 14);
		frame.getContentPane().add(LabelPalabraATraducir);
		
		output = new JTextField();
		output.setEditable(false);
		output.setBounds(120, 73, 227, 20);
		frame.getContentPane().add(output);
		output.setColumns(10);
		
		lblNewLabel = new JLabel("Traductor");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(140, 11, 90, 23);
		frame.getContentPane().add(lblNewLabel);
		
		btnTraducir = new JButton("Traducir");
		btnTraducir.setBounds(120, 101, 227, 23);
		frame.getContentPane().add(btnTraducir);
		btnTraducir.addActionListener(new ActionListener () {
			public void actionPerformed (ActionEvent arg) {
				if (InputPalabra.getText().isEmpty())
					JOptionPane.showMessageDialog(null, "No hay ninguna palabra para traducir.");
				else
					traducir();
			}
		});
	}
	
	public void traducir () {
		String texto = InputPalabra.getText();
		Document pagina;
		String url = "https://www.spanishdict.com/traductor/" + texto;
		try {
			pagina = Jsoup.connect(url).get();
			Elements palabra = pagina.getElementById("quickdef1-es").getElementsByClass("_1btShz4h");
			System.out.println("Palabra: " + palabra.html());
			output.setText(palabra.html());
		} catch (Exception err) {
			System.out.println("ERROR: No se pudo conectar con la p�gina.");
		}
	}
}
