package jotasoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class traductor1 {

	public static void main(String[] args) throws Exception {
		Document pagina;
		String url = "https://www.spanishdict.com/traductor/CAJA";
		pagina = Jsoup.connect(url).get();
		Elements palabra = pagina.getElementById("quickdef1-es").getElementsByClass("_1btShz4h");
		System.out.println("Palabra: " + palabra.html());
	}

}
