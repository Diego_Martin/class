package jotasoup;

import java.awt.EventQueue;
//import java.
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.io.IOException;

import javax.swing.SwingConstants;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class coronavirus {

	private JFrame frame;
	private JLabel OutputCoronavirus;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					coronavirus window = new coronavirus();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public coronavirus() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCasosDeCoronavirus = new JLabel("Casos de coronavirus");
		lblCasosDeCoronavirus.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblCasosDeCoronavirus.setBounds(112, 11, 195, 35);
		frame.getContentPane().add(lblCasosDeCoronavirus);
		
		OutputCoronavirus = new JLabel("");
		OutputCoronavirus.setHorizontalAlignment(SwingConstants.CENTER);
		OutputCoronavirus.setFont(new Font("Tahoma", Font.PLAIN, 40));
		OutputCoronavirus.setBounds(10, 53, 414, 35);
		frame.getContentPane().add(OutputCoronavirus);
		
		try {
			displayData ();
		} catch (IOException err) {
			System.out.println("Ha habido algun error");
			System.exit(0);
		}

	}
	
	private void displayData () throws IOException {
		Document CovidPage;
		Element resultCov;
		CovidPage = Jsoup.connect("https://www.worldometers.info/coronavirus/").get();
        resultCov = CovidPage.select(".maincounter-number > span").first();
        OutputCoronavirus.setText(resultCov.text());
		
	}
}
