package Semana2;

import java.util.ArrayList;

public class menu2 {

	public static void main(String[] args) {
		/* Escriba un programa para una biblioteca que contenga libros y revistas
		 * 
		 * a) Las caracteristicas comunes que se  almacenan tanto para las revistas como para los libros son:
		 * 	- Codigo
		 *  - Titulo
		 *  - A�o
		 *    Deben estar en el constructor
		 *    
		 * b) Los libros tienen adem�s un atributo prestado. Los libros cuando se crean no est�n prestados
		 * 
		 * c) Las revistas tienen un n�mero. En el momento de crear las revistas se pasa el n�mero por par�metro
		 * 
		 * d) Tanto las revista como los libros deben tener (Aparte de los contructores) un m�todo toString () que devuelve
		 *    el valor de todos los atributos en una cadena de caracteres. Tambien tienen un m�todo que devuelve el a�o de publicacion
		 *    y otro para el c�digo.
		 * 
		 * e) Para prevenir posibles cambios en el programa se tiene que implementar una interfaz 'Prestable' con los metodos prestar ()
		 *    devolver () y prestado (). La clase Libro implementa esta interfaz.
		 */
		
		ArrayList<Prestable>items = new ArrayList<Prestable>();
		items.add (new Libro (0, "Libro 1", 2000));
		items.add (new Revista2 (1, "Revista 1", 2004, 1));
		
		for (int i = 0; i < items.size(); i++)
			System.out.println(items.get(i).toString());

	}

}
