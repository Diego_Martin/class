package Semana2;

public class menu {

	public static void main(String[] args) {
		/* Escriba un programa para una biblioteca que contenga libros y revistas
		 * 
		 * a) Las caracteristicas comunes que se  almacenan tanto para las revistas como para los libros son:
		 * 	- Codigo
		 *  - Titulo
		 *  - A�o
		 *    Deben estar en el constructor
		 *    
		 * b) Los libros tienen adem�s un atributo prestado. Los libros cuando se crean no est�n prestados
		 * 
		 * c) Las revistas tienen un n�mero. En el momento de crear las revistas se pasa el n�mero por par�metro
		 * 
		 * d) Tanto las revista como los libros deben tener (Aparte de los contructores) un m�todo toString () que devuelve
		 *    el valor de todos los atributos en una cadena de caracteres. Tambien tienen un m�todo que devuelve el a�o de publicacion
		 *    y otro para el c�digo.
		 * 
		 * e) Para prevenir posibles cambios en el programa se tiene que implementar una interfaz 'Prestable' con los metodos prestar ()
		 *    devolver () y prestado (). La clase Libro implementa esta interfaz.
		 */
		
		Libro articulo1 = new Libro (1, "Articulo 1", 2000);
		Libro articulo2 = new Libro (2, "Articulo 2", 2010);
		
		Revista articulo3 = new Revista (3, "Revista 1", 2020, 1);
		Revista articulo4 = new Revista (4, "Revista 2", 2030, 2);
		
		System.out.println(articulo1.toString());
		System.out.println(articulo2.toString());
		System.out.println(articulo3.toString());
		System.out.println(articulo4.toString());
		articulo1.prestar();
		System.out.println("Articulo1 prestado: " + articulo1.isPrestado());
		articulo1.devolver();
		System.out.println("Articulo1 prestado: " + articulo1.isPrestado());

	}

}
