package Semana2;

public class Libro extends Articulo implements Prestable {
	private boolean prestado;

	public Libro(int codigo, String titulo, int anio) {
		super(codigo, titulo, anio);
		this.prestado = false;
	}

	public boolean isPrestado() {
		return prestado;
	}

	public void setPrestado(boolean prestado) {
		this.prestado = prestado;
	}
	
	public void prestar () {
		this.prestado = true;
	}
	
	public void devolver () {
		this.prestado = false;
	}
	
	public boolean prestado () {
		return prestado;
	}

	@Override
	public String toString() {
		return "Libro [prestado=" + this.prestado + ", codigo=" + getCodigo() + ", titulo=" + getTitulo() + ", a�o=" + getAnio() + "]";
	}

}
