package Semana2;

public class Revista extends Articulo {
	private int numero;
	
	public Revista (int codigo, String titulo, int anio, int numero) {
		super (codigo, titulo, anio);
		this.numero = numero;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "Revista [numero=" + numero + ", codigo=" + getCodigo() + ", titulo=" + getTitulo() + ", a�o=" + getAnio() + "]";
	}
	
	
}
