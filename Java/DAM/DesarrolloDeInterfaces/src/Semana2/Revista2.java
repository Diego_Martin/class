package Semana2;

public class Revista2 extends Articulo implements Prestable {
	private int numero;
	private boolean prestado;
	
	public Revista2 (int codigo, String titulo, int anio, int numero) {
		super (codigo, titulo, anio);
		this.numero = numero;
		this.prestado = false;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public void prestar () {
		this.prestado = true;
	}
	
	public void devolver () {
		this.prestado = false;
	}
	
	public boolean prestado () {
		return prestado;
	}

	@Override
	public String toString() {
		return "Revista [numero=" + numero + ", codigo=" + getCodigo() + ", titulo=" + getTitulo() + ", a�o=" + getAnio() + "]";
	}
	
	
}
