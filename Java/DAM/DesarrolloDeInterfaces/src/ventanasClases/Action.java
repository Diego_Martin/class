package ventanasClases;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JTextField;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Action implements ActionListener {
	
	private JTextField in, out;
	
	public Action (JTextField out, JTextField in) {
		this.in = in;
		this.out = out;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		traducir ();
	}
	
	public void traducir () {
		String texto = in.getText();
		Document pagina;
		String url = "https://www.spanishdict.com/traductor/" + texto;
		try {
			pagina = Jsoup.connect(url).get();
			Elements palabra = pagina.getElementById("quickdef1-es").getElementsByClass("_1btShz4h");
			System.out.println("Palabra: " + palabra.html());
			out.setText(palabra.html());
		} catch (IOException err){
			System.out.println(err.toString());
		} catch (Exception err) {
			System.out.println("ERROR: No se pudo conectar con la p�gina.");
		}
	}
}
