package ventanasClases;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Panel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private JLabel palabraIn, palabraOut;
	private JTextField textIn, textOut;
	private JButton btnTraducir;

	public Panel () {
		setBackground (new Color(0, 100, 255));
		
		setLayout(null);
		palabraIn = new JLabel ();
		palabraIn.setBounds(80, 80, 50, 50);
		palabraIn.setText("Palabra a traducir:");
		palabraIn.setForeground(Color.white);
		palabraIn.setSize(palabraIn.getPreferredSize());
		add(palabraIn);
		
		palabraOut = new JLabel ();
		palabraOut.setBounds(80, 100, 50, 50);
		palabraOut.setText("Palabra traducida:");
		palabraOut.setForeground(Color.white);
		palabraOut.setSize(palabraOut.getPreferredSize());
		add(palabraOut);
		
		textIn = new JTextField ();
		textIn.setBounds(200, 80, 130, palabraIn.getHeight());
		add(textIn);
		
		textOut = new JTextField ();
		textOut.setBounds(200, 100, 130, palabraOut.getHeight());
		textOut.setEditable(false);
		add(textOut);
		
		btnTraducir = new JButton ("Traducir");
		btnTraducir.setBounds(150, 120, 120, palabraIn.getHeight());
		Action accion = new Action (textIn, textOut);
		btnTraducir.addActionListener(accion);
		add(btnTraducir);
	}
	
}
