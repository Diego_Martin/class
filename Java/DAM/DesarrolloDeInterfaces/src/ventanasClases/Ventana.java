package ventanasClases;

import javax.swing.JFrame;

public class Ventana extends JFrame {
	
	private static final long serialVersionUID = 1L;

	public Ventana (String titulo) {
		setTitle (titulo);
		setBounds (100, 100, 450, 300);
		setLocationRelativeTo (null);
		Panel panelPrincipal = new Panel();
		add(panelPrincipal);
		setResizable(false);
	}
}
