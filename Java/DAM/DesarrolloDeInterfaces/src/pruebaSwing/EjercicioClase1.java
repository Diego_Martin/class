package pruebaSwing;

import java.awt.EventQueue;

import javax.swing.JFrame;
//import javax.swing.JLabel;
//import java.awt.BorderLayout;
//import javax.swing.BoxLayout;
//import java.awt.GridLayout;
//import javax.swing.JButton;
import java.awt.Panel;
import java.awt.Color;
import javax.swing.JTextPane;
import java.awt.Label;
import java.awt.TextField;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import java.awt.Toolkit;

public class EjercicioClase1 {

	private JFrame frmTituloDeLa;
	private JPasswordField inputPass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EjercicioClase1 window = new EjercicioClase1();
					window.frmTituloDeLa.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EjercicioClase1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTituloDeLa = new JFrame();
		frmTituloDeLa.setTitle("Titulo de la app");
		frmTituloDeLa.setIconImage(Toolkit.getDefaultToolkit().getImage("D:\\GitLab\\class\\Java\\DAM\\DesarrolloDeInterfaces\\src\\pruebaSwing\\leaf.png"));
		frmTituloDeLa.setResizable(false);
		frmTituloDeLa.setBounds(100, 100, 450, 300);
		frmTituloDeLa.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTituloDeLa.getContentPane().setLayout(null);
		
		Panel panel = new Panel();
		panel.setBackground(new Color(50, 205, 50));
		panel.setBounds(0, 0, 444, 30);
		frmTituloDeLa.getContentPane().add(panel);
		
		JTextPane txtpnHola = new JTextPane();
		txtpnHola.setToolTipText("Titulo de la app");
		txtpnHola.setText("Titulo de la app");
		panel.add(txtpnHola);
		
		TextField inputNombre = new TextField();
		inputNombre.setBounds(95, 36, 160, 22);
		frmTituloDeLa.getContentPane().add(inputNombre);
		
		Label labelNombre = new Label("Nombre");
		labelNombre.setAlignment(Label.RIGHT);
		labelNombre.setBounds(10, 36, 62, 22);
		frmTituloDeLa.getContentPane().add(labelNombre);
		
		Label labelApellido = new Label("Apellido");
		labelApellido.setAlignment(Label.RIGHT);
		labelApellido.setBounds(10, 64, 62, 22);
		frmTituloDeLa.getContentPane().add(labelApellido);
		
		TextField inputApellido = new TextField();
		inputApellido.setBounds(95, 64, 160, 22);
		frmTituloDeLa.getContentPane().add(inputApellido);
		
		Label labelPass = new Label("Contrase\u00F1a");
		labelPass.setAlignment(Label.RIGHT);
		labelPass.setBounds(10, 92, 62, 22);
		frmTituloDeLa.getContentPane().add(labelPass);
		
		inputPass = new JPasswordField();
		inputPass.setBounds(95, 92, 160, 20);
		frmTituloDeLa.getContentPane().add(inputPass);
		
		JRadioButton radioSexo = new JRadioButton("Varon");
		radioSexo.setBounds(95, 111, 109, 23);
		frmTituloDeLa.getContentPane().add(radioSexo);
		
		JRadioButton radioSexo2 = new JRadioButton("Mujer");
		radioSexo2.setBounds(95, 129, 109, 23);
		frmTituloDeLa.getContentPane().add(radioSexo2);
		System.out.println(frmTituloDeLa.getClass().getSimpleName());
		System.out.println(frmTituloDeLa.getClass().getName());
	}
}
