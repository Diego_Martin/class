package Semana1;

import Semana1.Directivo.Turno;

public class Ejercicio6_7 {

	public static void main(String[] args) {
		Profesor misProfesores[] = {new Profesor ("789456123F", "Sergio", "Sanchez Crespo", 1000000.99, 3, false), new Profesor ("456123789D", "Jose Luis", "Alvarez Oliva", 1000000000.73, 4, true)};
		Directivo misDirectivos[] = {new Directivo ("147258369D", "Javier", "Pozo", 456135.7, false, Turno.manana), new Directivo ("369258147E", "Manuel", "De Castro", 999999999.99, true, Turno.tarde)};
		Administracion misAdministradores[] = {new Administracion ("159753842F", "Victor", "Guerra Rubio", 2.0, "SMyR", 2020), new Administracion ("618618681R", "Daniel", "Navarro Garcia", 2.0, "SMyR", 2020)};
		
		for (int i = 0; i > misProfesores.length; i ++)
			System.out.println(misProfesores [i].toString());
		
		for (int i = 0; i > misDirectivos.length; i ++)
			System.out.println(misDirectivos [i].toString());
		
		for (int i = 0; i > misAdministradores.length; i ++)
			System.out.println(misAdministradores [i].toString());
	}

}
