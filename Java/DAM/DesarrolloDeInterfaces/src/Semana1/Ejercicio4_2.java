package Semana1;

import java.util.Scanner;


public class Ejercicio4_2 {

	public static void main(String[] args) {
		Scanner teclado = new Scanner (System.in);
		boolean repetir = false;
		int filas = 0, columnas = 0, resultado = 0;
		
		System.out.println("Voy a sumar los n�meros de la matriz que me digas.");
		 // pido las filas
		do {
			try {
				System.out.print ("Dime el numero de filas que tiene la matriz: ");
				String aux = teclado.nextLine ();
				
				filas = Integer.parseInt (aux);
				repetir = true;
			} catch (NumberFormatException err) { // De java.lang
				System.out.println ("Debes escribir un numero entero.");
			}
		} while (!repetir);
		repetir = false;
		
		//Pido las columnas
		do {
			try {
				System.out.print ("Dime el numero de columnas que tiene la matriz: ");
				String aux = teclado.nextLine ();
				
				columnas = Integer.parseInt (aux);
				repetir = true;
			} catch (NumberFormatException err) { // De java.lang
				System.out.println ("Debes escribir un numero entero.");
			}
		} while (!repetir);
		repetir = false;
		
		// System.out.println("Filas: " + filas + "\nColumnas: " + columnas);
		int miMatriz[][] = new int[filas][columnas];
		
		// Relleno la matriz con datos de usuario
		for (int i = 0; i < filas; i ++)
			for (int j = 0; j < columnas; j ++) {
				do {
					try {
						System.out.print ("Introduce el valor de Matriz [" + i + "][" + j + "] (fila: " + (i + 1) + ", columna: " + (j + 1) + "): ");
						String aux = teclado.nextLine ();
						
						miMatriz [i][j] = Integer.parseInt (aux);
						repetir = true;
					} catch (NumberFormatException err) { // De java.lang
						System.out.println ("Debes escribir un numero entero.");
					}
				} while (!repetir);
				repetir = false;
			}
		
		// Sumamos numeros
		for (int i = 0; i < filas; i ++)
			for (int j = 0; j < columnas; j ++)
				resultado += miMatriz [i][j];
		
		System.out.println("La suma de los n�meros es: " + resultado);
		teclado.close ();
	}

}
