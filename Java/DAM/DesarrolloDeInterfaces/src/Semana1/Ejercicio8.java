package Semana1;

import java.util.Scanner;
import java.util.Date;
import Semana1.Directivo.Turno;
import Semana1.Persona.Sexo;

public class Ejercicio8 {

	public static void main(String[] args) {
		Profesor misProfesores[] = {new Profesor ("789456123F", "Sergio", "Sanchez Crespo", 1000000.99, 3, false), new Profesor ("456123789D", "Jose Luis", "Alvarez Oliva", 1000000000.73, 4, true)};
		Directivo misDirectivos[] = {new Directivo ("147258369D", "Javier", "Pozo", 456135.7, false, Turno.manana), new Directivo ("369258147E", "Manuel", "De Castro", 999999999.99, true, Turno.tarde)};
		Administracion misAdministradores[] = {new Administracion ("159753842F", "Victor", "Guerra Rubio", 2.0, "SMyR", 2020), new Administracion ("618618681R", "Daniel", "Navarro Garcia", 2.0, "SMyR", 2020)};
		// -------------- Modulos ------------------
		Modulo modulosDam[] = {new Modulo ("Programacion multimedia", 3, misProfesores [1], false), new Modulo ("Desarrollo de interfaces", 7, misProfesores [0], false)};
		Modulo modulosTseas[] = {new Modulo ("Educacion fisica", 6, misProfesores [0], false), new Modulo ("Anatomia", 5, misProfesores [0], false)};
		// -----------------------------------------
		
		@SuppressWarnings ("deprecation")
		Alumno misAlumnos[] = {new Alumno ("789456123F", "Daniel", "Navarro", 0.0, new Date (1999, 12, 4), Sexo.varon, false, modulosDam), new Alumno ("321654987T", "Victor", "Guerra", 0.0, new Date (2000, 9, 11), Sexo.varon, false, modulosDam), new Alumno ("741852963R", "Victor", "Guerra", 0.0, new Date (2000, 9, 11), Sexo.varon, false, modulosTseas)};
		boolean repetir = true, salir = false;
		int eleccion = 0;
		Scanner teclado = new Scanner (System.in);
		
		do {
			System.out.println("+-------------------------------------------------+");
			System.out.println("| 1. Listado de alumnos.                          |");
			System.out.println("| 2. Listado de profesores.                       |");
			System.out.println("| 3. Listado de directivos.                       |");
			System.out.println("| 4. Listado de administradores.                  |");
			System.out.println("| 5. Salir.                                       |");
			System.out.println("+-------------------------------------------------+");
			do {
				try {
					System.out.print ("| Que listado quieres imprimir?: ");
					String aux = teclado.nextLine ();
					
					eleccion = Integer.parseInt (aux);
					repetir = true;
				} catch (NumberFormatException err) { // De java.lang
					System.out.println ("| Debes escribir un numero entero.");
				}
			} while (!repetir);
			repetir = false;
			
			switch (eleccion) {
				case 1:
					for (int i = 0; i < misAlumnos.length; i++)
						System.out.println(misAlumnos [i].toString ());
					
					break;
				case 2:
					for (int i = 0; i < misProfesores.length; i++)
						System.out.println(misProfesores [i].toString ());
					
					break;
				case 3:
					for (int i = 0; i < misDirectivos.length; i++)
						System.out.println(misDirectivos [i].toString ());
					
					break;
				case 4:
					for (int i = 0; i < misAdministradores.length; i++)
						System.out.println(misAdministradores [i].toString ());
					
					break;
				case 5:
					salir = true;
					break;
				default:
					System.out.println("La opcion debe estar entre 1 y 4");
			}
		} while (!salir);
		teclado.close ();
		
	}

}
