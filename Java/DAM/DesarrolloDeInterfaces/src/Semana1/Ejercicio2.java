package Semana1;

public class Ejercicio2 {

	public static void main(String[] args) {
		String miString = "Jose Luis";
		int miInt = 73;
		char miChar = 'S';
		double miDouble = 73.0;
		boolean miBoolean = true;
		
		System.out.println("-- VARIABLES --\n"
				+ "miString = " + miString + "\n"
				+ "miInt = " + miInt + "\n"
				+ "miChar = " + miChar + "\n"
				+ "miDouble = " + miDouble + "\n"
				+ "miBoolean = " + miBoolean);
	}

}
