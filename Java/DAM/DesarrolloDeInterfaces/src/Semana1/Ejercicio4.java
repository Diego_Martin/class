package Semana1;

import java.util.Scanner;


public class Ejercicio4 {

	public static void main(String[] args) {
		Scanner teclado = new Scanner (System.in);
		boolean repetir = false;
		int dim = 0, resultado = 0;
		
		System.out.println("Voy a sumar los n�meros de la matriz que me digas.");
		//Pido las columnas
		do {
			try {
				System.out.print ("Dime el numero de columnas que tiene la matriz: ");
				String aux = teclado.nextLine ();
				
				dim = Integer.parseInt (aux);
				repetir = true;
			} catch (NumberFormatException err) { // De java.lang
				System.out.println ("Debes escribir un numero entero.");
			}
		} while (!repetir);
		repetir = false;
		
		// System.out.println("Filas: " + filas + "\nColumnas: " + columnas);
		int miMatriz[][] = new int[dim][dim];
		
		// Relleno la matriz con datos aleatorios
		for (int i = 0; i < dim; i ++)
			for (int j = 0; j < dim; j ++) {
				miMatriz [i][j] = (int) Math.floor (Math.random () * 10);
				// Sumamos numeros
				resultado += miMatriz [i][j];
			}
		
		System.out.println("La suma de los n�meros es: " + resultado);
		teclado.close ();
	}

}
