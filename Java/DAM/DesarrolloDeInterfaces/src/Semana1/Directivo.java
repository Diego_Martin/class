package Semana1;

public class Directivo extends Persona {
	public boolean salesiano;
	public enum Turno {manana, tarde};
	public Turno turno = Turno.manana;
	
	public Directivo(String DNI, String nombre, String apellidos, double salario, boolean salesiano, Turno turno) {
		super (DNI, nombre, apellidos, salario);
		this.salesiano = salesiano;
		this.turno = turno;
	}

	public boolean isSalesiano() {
		return salesiano;
	}

	public void setSalesiano(boolean salesiano) {
		this.salesiano = salesiano;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	@Override
	public String toString() {
		return "Directivo [DNI=" + super.getDNI () + ", nombre=" + super.getNombre () + ", apellidos=" + super.getApellidos () + ", salario=" + super.getSalario ()
				+ ", salesiano=" + salesiano + ", turno=" + turno + "]";
	}
	
}
