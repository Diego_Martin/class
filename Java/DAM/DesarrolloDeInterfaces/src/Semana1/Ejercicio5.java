package Semana1;

public class Ejercicio5 {
	
	static int factorial (int numero) {
		if (numero == 0)
			return 1;
		else 
			return numero * factorial (numero - 1);
	}

	public static void main(String[] args) {
		for (int i = 1; i < 6; i++)
			System.out.println("El factorial de " + i +" es: " + factorial (i));
	}
}
