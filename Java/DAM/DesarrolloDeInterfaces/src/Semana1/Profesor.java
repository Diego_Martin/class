package Semana1;

public class Profesor extends Persona {
	public int asignaturas;
	public boolean tutor;
	
	public Profesor(String DNI, String nombre, String apellidos, double salario, int asignaturas, boolean tutor) {
		super (DNI, nombre, apellidos, salario);
		this.asignaturas = asignaturas;
		this.tutor = tutor;
	}
	
	public void setSalario(double salario) {
		this.salario = salario;
	}
	
	public int getAsignaturas() {
		return asignaturas;
	}
	
	public void setAsignaturas(int asignaturas) {
		this.asignaturas = asignaturas;
	}
	
	public boolean isTutor() {
		return tutor;
	}
	
	public void setTutor(boolean tutor) {
		this.tutor = tutor;
	}
	
	@Override
	public String toString() {
		return "Profesor [DNI=" + super.getDNI () + ", nombre=" + super.getNombre () + ", apellidos=" + super.getApellidos () + ", salario=" + super.getSalario () + ", asignaturas=" + asignaturas + ", tutor=" + tutor + "]";
	}
	
}
