package Semana1;

import java.util.Date;
//import Semana1.Persona.Sexo;

public class Alumno extends Persona {
	public Sexo sexo;
	public boolean repetidor;
	public Date nacimiento;
	public Modulo modulos[];
	
	public Alumno(String dNI, String nombre, String apellidos, double salario, Date nacimiento, Sexo sexo, boolean repetidor, Modulo modulos[]) {
		super(dNI, nombre, apellidos, salario);
		this.sexo = sexo;
		this.repetidor = repetidor;
		this.nacimiento = nacimiento;
		this.modulos = modulos;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public boolean isRepetidor() {
		return repetidor;
	}

	public void setRepetidor(boolean repetidor) {
		this.repetidor = repetidor;
	}

	public Date getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}

	public Modulo[] getModulos() {
		return modulos;
	}

	public void setModulos(Modulo[] modulos) {
		this.modulos = modulos;
	}

	@Override
	public String toString() {
		String resultado = "";
		resultado += "Alumno [nacimiento=" + nacimiento.toString () +", sexo=" + sexo + ", repetidor=" + repetidor + ", modulos=[";
		for (int i = 0; i < this.modulos.length; i++)
			if (i != this.modulos.length - 1)
				resultado += modulos [i].getNombre () + ", ";
			else
				resultado += modulos [i].getNombre ();
		resultado += "]]";
		return resultado;
	}

}
