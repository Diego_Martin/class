package Semana1;

public class Administracion extends Persona {
	public String estudios;
	public int antiguedad;
	
	public Administracion(String DNI, String nombre, String apellidos, double salario, String estudios, int antiguedad) {
		super (DNI, nombre, apellidos, salario);
		this.estudios = estudios;
		this.antiguedad = antiguedad;
	}

	public String getEstudios() {
		return estudios;
	}

	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}

	public int getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}

	@Override
	public String toString() {
		return "Administracion [DNI=" + super.getDNI () + ", nombre=" + super.getNombre () + ", apellidos=" + super.getApellidos () + ", salario=" + super.getSalario () + ", estudios=" + estudios + ", antiguedad=" + antiguedad + "]";
	}
	
}
