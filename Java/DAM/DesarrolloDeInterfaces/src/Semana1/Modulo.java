package Semana1;

public class Modulo {
	String nombre;
	int horas;
	Profesor profesor;
	boolean convalidado;
	
	public Modulo(String nombre, int horas, Profesor profesor, boolean convalidado) {
		this.nombre = nombre;
		this.horas = horas;
		this.profesor = profesor;
		this.convalidado = convalidado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getHoras() {
		return horas;
	}

	public void setHoras(int horas) {
		this.horas = horas;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	public boolean isConvalidado() {
		return convalidado;
	}

	public void setConvalidado(boolean convalidado) {
		this.convalidado = convalidado;
	}

	@Override
	public String toString() {
		return "Modulo [nombre=" + nombre + ", horas=" + horas + ", profesor=" + profesor.toString () + ", convalidado="
				+ convalidado + "]";
	}
	
}
